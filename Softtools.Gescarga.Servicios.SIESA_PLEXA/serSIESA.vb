﻿Option Explicit On

Imports System.Configuration
Imports System.Diagnostics.Eventing.Reader
Imports System.Net
Imports System.Threading
Imports System.Xml
' Servicio GENERICTRANSFER
'Imports Softtools.Gescarga.Servicios.SW_GESCARGA50_SIESA.wsSIESA_GENERICTRANSFER
' Servicio SELENE
Imports Softtools.Gescarga.Servicios.SW_GESCARGA50_SIESA.wsSIESA_SELENE

Public Class serSIESA

#Region "Variables Generales"

    ' Variables
    Private strSQL As String
    Private strPlano As String
    Private lonNumeRegi As Long
    Private objGeneral As clsGeneral

    Private intCodigoEmpresa As Integer
    Private lonIntervaloMilisegundosEjecucionProceso As Long
    Private strRutaArchivoLog As String
    Private strRutaArchivoXML As String

    ' Hilos Procesos
    Private HilProcesoTerceros As Thread
    Private HilProcesoComprobantesEgreso As Thread
    Private HilProcesoLegalizacionGastos As Thread
    Private HilProcesoFacturaVentas As Thread

#End Region

#Region "Constantes GESTRANS"
    ' GESTRANS
    Const CODIGO_USUARIO_SYSTEM As Integer = 0 ' Usuario del sistema

    ' TIPOS DOCUMENTOS GESTRANS
    Const TIDO_COMPROBANTE_EGRESO As Integer = 30
    Const TIDO_CUENTA_X_COBRAR As Integer = 80

    ' CATALOGOS GESTRANS
    Const TIPO_NATURALEZA_NATURAL_GESTRANS As Long = 2299
    Const TIPO_NATURALEZA_JURIDICA_GESTRANS As Long = 2298

    Const TIPO_IDENTIFICACION_CEDULA_GESTRANS As Long = 2292
    Const TIPO_IDENTIFICACION_EXTRANJERIA_GESTRANS As Long = 2293
    Const TIPO_IDENTIFICACION_NIT_GESTRANS As Long = 2291

    Const ESDO_DEFINTTIVO As Long = 8656
    Const ESDO_ANULADO As Long = 9444

    Const CONCEPTO_CUENTA_X_PAGAR_SALDO_FAVOR_CONDUCTOR As Long = 10001
    Const CONCEPTO_CUENTA_X_COBRAR_SALDO_FAVOR_EMPRESA As Long = 10002

#End Region

#Region "Constantes SIESA"
    ' SIESA TABLAS
    Const TIPO_NATURALEZA_NATURAL_SIESA As Integer = 1
    Const TIPO_NATURALEZA_JURIDICA_SIESA As Integer = 2

    Const TIPO_IDENTIFICACION_CEDULA_SIESA As String = "C"
    Const TIPO_IDENTIFICACION_EXTRANJERIA_SIESA As String = "E"
    Const TIPO_IDENTIFICACION_NIT_SIESA As String = "N"

    Const ACTIVIDAD_ECONOMICA_PASAJEROS_SIESA As String = "4921"
    Const PAIS_COLOMBIA_SIESA As String = "169"
    Const CORREO_POR_DEFECTO As String = "sistemas@plexa.com"

    ' Movimientos Contables PLEXA
    Const CENTRO_OPERACION_PLEXA_SIESA As String = "201"
    Const CENTRO_OPERACION_PLEXA_SIESA_INVENTARIO As String = "202"
    Const TIPO_DOCUMENTO_EGRESO_PLEXA_SIESA As String = "A1"
    Const UNIDAD_NEGOCIO_PLEXA_SIESA As String = "02"
    Const SUCURSAL_PLEXA_SIESA As String = "001"

    'Legalizacion Gastos PLEXA
    Const TIPO_DOCUMENTO_LEGALIZACION_PLEXA_SIESA As String = "P35" ' P33

    ' Factura Ventas
    Const ID_CODIGO_FACTURA As String = "202"
    Const ID_CODIGO_FACTURA_GLP As String = "202"
    Const ID_CODIGO_FACTURA_PGR As String = "201"
    Const ID_CONDICION_PAGO As String = "10D"
    Const ID_BODEGA_PLEXA_SIESA As String = "TGLP"
    Const ID_BODEGA_PLEXA_SIESA_GLP As String = "TGLP"
    Const ID_BODEGA_PLEXA_SIESA_PGR As String = "TPGR"
    Const ID_MOTIVO_PLEXA_SIESA As String = "SP"
    Const ID_MOTIVO_PLEXA_SIESA_GLP As String = "SP"
    Const ID_MOTIVO_PLEXA_SIESA_PGR As String = "ST"
    Const ID_LISTA_PRECIOS_PLEXA_SIESA As String = "C01"
    Const ID_UNIDAD_MEDIDA_KILOGRAMO_SIESA As String = "KGM"
    Const ID_UNIDAD_MEDIDA_GALON_SIESA As String = "GLS"
    Const ID_UNIDAD_MEDIDA_TONELADA_SIESA As String = "TON"

    'Unidad Medida GESCARGA
    Const UNIDAD_MEDIDA_KILOGRAMO = "KG"
    Const UNIDAD_MEDIDA_GALON = "GA"
    Const UNIDAD_MEDIDA_METRO_CUBICO = "M3"
    Const UNIDAD_MEDIDA_TONELADA = "TO"

    'Items SIESA
    Const ITEM_ID_INVENTARIO_GLP = "40"
    Const ITEM_ID_INVENTARIO_PGR = "39"

#End Region

#Region "GENERICTRANSFER"

    'Dim objGenericTransfer As wsGenerarPlanoSoapClient

#End Region

#Region "SELENE"

    Dim objSelene As WebServicePlexaSoapClient

#End Region

    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            ' Comprobanntes de Egreso (Anticipos)
            Me.HilProcesoComprobantesEgreso = New Thread(AddressOf Proceso_Comprobantes_Egreso_SELENE)
            Me.HilProcesoComprobantesEgreso.Start()

            ' Legalizacion Gastos
            Me.HilProcesoLegalizacionGastos = New Thread(AddressOf Proceso_Legalizacion_Gastos_SELENE)
            Me.HilProcesoLegalizacionGastos.Start()

            ' Facturas Venta
            Me.HilProcesoFacturaVentas = New Thread(AddressOf Proceso_Factura_Ventas_SELENE)
            Me.HilProcesoFacturaVentas.Start()

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error OnStart: " & ex.Message)
        End Try
    End Sub

    Protected Overrides Sub OnStop()
        Try
            Me.HilProcesoComprobantesEgreso.Abort()

            Me.HilProcesoLegalizacionGastos.Abort()

            Me.HilProcesoFacturaVentas.Abort()

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error OnStop: " & ex.Message)
        End Try
    End Sub

    Public Sub New()
        Try
            InitializeComponent()

            Me.objGeneral = New clsGeneral
            Call Cargar_Datos_App_Config()
            Me.objGeneral.Guardar_Mensaje_Log("Inició servicio")

            ' COMENTARIAR CUANDO SE GENERE EL INSTALADOR DEL SERVICIO, SOLO PARA PRUEBAS

            'Me.HilProcesoComprobantesEgreso = New Thread(AddressOf Proceso_Comprobantes_Egreso_SELENE)
            'Me.HilProcesoComprobantesEgreso.Start()

            'Me.HilProcesoLegalizacionGastos = New Thread(AddressOf Proceso_Legalizacion_Gastos_SELENE)
            'Me.HilProcesoLegalizacionGastos.Start()

            'Me.HilProcesoFacturaVentas = New Thread(AddressOf Proceso_Factura_Ventas_SELENE)
            'Me.HilProcesoFacturaVentas.Start()

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error New: " & ex.Message)
        End Try

    End Sub


#Region "Rutinas Integración WS SELENE"

    ' Autor: Jesus Cuadros 
    ' Fecha Creación: 11-JUN-2021
    ' Observaciones: Se consulta el Encabezado y Detalle de Comprobantes Contables, el cual debe traer 2 registros por comprobante

    Private Sub Proceso_Comprobantes_Egreso_SELENE()
        Try
            Dim dtsCompCont As DataSet

            While (True)
                Me.objGeneral.Guardar_Mensaje_Log("Inició Proceso Interfaz Anticipos(Comprobantes Egreso)")
                strSQL = "gsp_Consultar_Comprobantes_Egreso_SIESA_PLEXA " & intCodigoEmpresa.ToString
                dtsCompCont = objGeneral.Retorna_Dataset(strSQL)

                If dtsCompCont.Tables.Count > 0 Then

                    If dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        Me.objGeneral.Guardar_Mensaje_Log("Número Comprobantes Egreso a Procesar: " & dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows.Count)

                        Call Crear_XML_Comprobantes_Egreso_SELENE(dtsCompCont)

                    End If

                End If

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)

            End While

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Proceso_Terceros: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros 
    ' Fecha Creación: 11-JUN-2021
    ' Observaciones: El XML se arma con una sección <Documento> que corresponde a un encabezado y dos secciones de <Movimiento> con las cuentas PUC y valores Crédito y Débito

    Private Sub Crear_XML_Comprobantes_Egreso_SELENE(ByVal dtsCompCont As DataSet)
        Try
            Dim strXML As String, strRespuestaXML As String, strValorCampoDefec As String = "", strValorCampo As String = "", strErrorDetalleSIESA As String
            Dim lonNumeComp As Long, lonNumeCompEgre As Long, strFechComp As String, strIdenTerc As String, dblValoDebi As Double, dblValoCred As Double
            Dim strCentOper As String, strObserva As String, strCuentaPUC As String, lonNumeCompAnte As Long

            Dim xmlDocumento As XmlDocument
            Dim xmlDeclaracion As XmlDeclaration
            Dim xmlElementoRaiz As XmlElement
            Dim xmlElementoRoot As XmlElement
            Dim xmlElemento As XmlElement

            For intCon = 0 To dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                lonNumeComp = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero")
                lonNumeCompEgre = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString()
                strFechComp = Retorna_Fecha_Formato_SIESA(dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString())
                strIdenTerc = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Tercero").ToString()
                dblValoCred = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Credito").ToString()
                dblValoDebi = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Debito").ToString()
                strCentOper = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Centro_Operacion_Movimiento").ToString()
                strObserva = Mid$(dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Observaciones").ToString(), 1, 250)

                If dblValoDebi > 0 Then
                    strCuentaPUC = "14201101" ' TEMPORAL
                Else
                    strCuentaPUC = "24559001" ' TEMPORAL
                End If

                If lonNumeComp <> lonNumeCompAnte Then

                    xmlDocumento = New XmlDocument

                    'xmlDeclaracion = xmlDocumento.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
                    'xmlDocumento.AppendChild(xmlDeclaracion)

                    xmlElementoRaiz = xmlDocumento.CreateElement("MyDataSet")
                    xmlDocumento.AppendChild(xmlElementoRaiz)

                    ' DOCUMENTO 
                    xmlElementoRoot = xmlDocumento.DocumentElement
                    xmlElemento = xmlDocumento.CreateElement("Documento_contable_V2")
                    xmlElementoRoot.AppendChild(xmlElemento)
                    xmlElementoRoot = xmlDocumento.DocumentElement

                    ' F350_ID_CO (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_PLEXA_SIESA)
                    ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_EGRESO_PLEXA_SIESA)
                    ' F350_CONSEC_DOCTO (Entero(8))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeCompEgre)
                    ' F350_FECHA (Alfanumerico(YYYYMMDD))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_FECHA", strFechComp)
                    ' F350_ID_TERCERO (Alfanumerico(15))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TERCERO", strIdenTerc)
                    ' F350_NOTAS (Alfanumerico(255))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_NOTAS", strObserva)

                    ' MOVIMIENTO 
                    xmlElementoRoot = xmlDocumento.DocumentElement
                    xmlElemento = xmlDocumento.CreateElement("Movimiento_contable_V4")
                    xmlElementoRoot.AppendChild(xmlElemento)
                    xmlElementoRoot = xmlDocumento.DocumentElement

                    ' F350_ID_CO (Alfanumerico (3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_PLEXA_SIESA)
                    ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_EGRESO_PLEXA_SIESA)
                    ' F350_CONSEC_DOCTO (Entero(8))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeCompEgre)
                    ' F351_ID_AUXILIAR (Alfanumerico(20))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strCuentaPUC) ' TEMPORAL
                    ' F351_ID_TERCERO (Alfanumerico (15))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenTerc)
                    ' F351_ID_CO_MOV (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", strCentOper)
                    ' F351_ID_UN (Alfanumerico(20) 05=PASAJEROS)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_PLEXA_SIESA)
                    ' F351_VALOR_DB (Decimal)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblValoDebi)
                    ' F351_VALOR_CR (Decimal)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValoCred)
                    ' F351_VALOR_DB2 (Decimal)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB2", dblValoDebi)
                    ' F351_VALOR_CR2 (Decimal)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR2", dblValoCred)
                    ' F353_ID_SUCURSAL (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", SUCURSAL_PLEXA_SIESA)
                    ' F353_PREFIJO_CRUCE (Alfanumerico(20))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", lonNumeCompEgre)
                    ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", 0)
                    ' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechComp)
                    ' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechComp)
                    ' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strFechComp)

                    ' F354_NOTAS (Alfanumerico(255))
                    strValorCampo = Mid$(dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Observaciones").ToString(), 1, 250)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strObserva)

                Else
                    ' MOVIMIENTO 
                    xmlElementoRoot = xmlDocumento.DocumentElement
                    xmlElemento = xmlDocumento.CreateElement("Movimiento_contable_V4")
                    xmlElementoRoot.AppendChild(xmlElemento)
                    xmlElementoRoot = xmlDocumento.DocumentElement

                    ' F350_ID_CO (Alfanumerico (3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_PLEXA_SIESA)
                    ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_EGRESO_PLEXA_SIESA)
                    ' F350_CONSEC_DOCTO (Entero(8))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeCompEgre)
                    ' F351_ID_AUXILIAR (Alfanumerico(20))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strCuentaPUC) ' TEMPORAL 
                    ' F351_ID_TERCERO (Alfanumerico (15))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenTerc)
                    ' F351_ID_CO_MOV (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", strCentOper)
                    ' F351_ID_UN (Alfanumerico(20) 05=PASAJEROS)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_PLEXA_SIESA)
                    ' F351_VALOR_DB (Decimal)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblValoDebi)
                    ' F351_VALOR_CR (Decimal)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValoCred)
                    ' F351_VALOR_DB2 (Decimal)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB2", dblValoDebi)
                    ' F351_VALOR_CR2 (Decimal)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR2", dblValoCred)
                    ' F353_ID_SUCURSAL (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", SUCURSAL_PLEXA_SIESA)
                    ' F353_PREFIJO_CRUCE (Alfanumerico(20))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", lonNumeCompEgre)
                    ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", 0)
                    ' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechComp)
                    ' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechComp)
                    ' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strFechComp)
                    ' F354_NOTAS (Alfanumerico(255))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strObserva)

                    strXML = xmlDocumento.InnerXml

                    ' Invocar WEB SERVICE SELENE método Anticipo
                    Me.objSelene = New WebServicePlexaSoapClient
                    strRespuestaXML = objSelene.Anticipo(strXML)

                    If Trim$(strRespuestaXML) = "Resultado Siesa: Importacion Exitosa" Then

                        ' Actualizar el Comprobante Contable con Interfase_Contable = 1 y Fecha_Interfase_Contable = getDate()
                        strSQL = "UPDATE Encabezado_Comprobante_Contables SET Interfase_Contable = 1, Fecha_Interfase_Contable = getDate()"
                        strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                        strSQL += " AND Numero = " & lonNumeComp

                        ' Ejecutar UPDATE
                        lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                    Else
                        strErrorDetalleSIESA = Retorna_Error_Detalle_SIESA(strRespuestaXML)

                        ' Actualizar el Comprobante Contable con Interfase_Contable = 0, mensaje de error retornado por Generic Transfer y NUMERO INTENTOS
                        strSQL = "UPDATE Encabezado_Comprobante_Contables SET Interfase_Contable = 0, Mensaje_Error = '" & strErrorDetalleSIESA & "', Fecha_Interfase_Contable = getDate(), Intentos_Interfase_Contable = ISNULL(Intentos_Interfase_Contable,0) + 1"
                        strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                        strSQL += " AND Numero = " & lonNumeComp

                        ' Ejecutar UPDATE
                        lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                    End If

                End If

                lonNumeCompAnte = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero")

            Next

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Crear_XML_Comprobantes_Egreso: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros 
    ' Fecha Creación: 11-JUN-2021
    ' Observaciones: Se consultan las Legalizacion Gastos del Conductor

    Private Sub Proceso_Legalizacion_Gastos_SELENE()
        Try
            Dim dtsLegaGast As DataSet

            While (True)
                Me.objGeneral.Guardar_Mensaje_Log("Inició Proceso Interfaz Legalizacion Gastos Conductor")

                strSQL = "gsp_Consultar_Encabezado_Legalizacion_Gastos_SIESA_PLEXA " & intCodigoEmpresa.ToString ' & ", 136" ' TEMPORAL PRUEBAS
                dtsLegaGast = objGeneral.Retorna_Dataset(strSQL)

                If dtsLegaGast.Tables.Count > 0 Then

                    If dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        Me.objGeneral.Guardar_Mensaje_Log("Número Legalización Gastos a Procesar: " & dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows.Count)

                        Call Crear_XML_Legalizacion_Gastos_SELENE(dtsLegaGast)

                    End If

                End If

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)

            End While

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Proceso_Legalizacion_Gastos: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros  
    ' Fecha Creación: 11-JUN-2021
    ' Observaciones: El XML se arma con una sección <Documento> que corresponde a un encabezado y cuantos conceptos <Movimiento> tenga esa legalización

    Private Sub Crear_XML_Legalizacion_Gastos_SELENE(ByVal dtsLegaGast As DataSet)
        Try
            Dim strXML As String, strRespuestaXML As String, strFecha As String, strValorCampoDefec As String = "", strValorCampo As String = ""
            Dim strErrorDetalleSIESA As String, intCont1 As Integer, intCont2 As Integer, intCont3 As Integer
            Dim lonNumeLega As Long, lonNumeDocuLega As Long, lonCodiCond As Long = 0, strIdenCond As String = "", dblSaldFavoCond As Double
            Dim strIdenProv As String = "", dblSaldFavoEmpr As Double = 0, lonNumPlaDes As Long = 0, bolProcesoValido As Boolean, strNombConc As String
            Dim intCodiConc As Integer, lonNumConCxC As Long = 0, dblValConCxC As Double = 0, lonCodCxCSalFavEmp As Long, dblValConCxP As Long, lonCodCxPSalFavCon As Long
            ' Variables SIESA
            Dim strTipoOper As String, intCodiCentOper As Integer, strUnidadNegocio As String, strTipoDocu As String

            Dim xmlDocumento As XmlDocument
            'Dim xmlDeclaracion As XmlDeclaration
            Dim xmlElementoRaiz As XmlElement
            Dim xmlElementoRoot As XmlElement
            Dim xmlElemento As XmlElement

            For intCon = 0 To dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                Dim dtsCondLegaGast As DataSet
                Dim intConsecutivo As Integer = 1

                lonNumeLega = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Numero")
                lonNumeDocuLega = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Numero_Documento")
                lonNumPlaDes = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont1).Item("ENPD_Numero")

                ' En el catalogo Lineas Productos Transportados(CATA_LIPT_Codigo) relacionado al Producto Transportado que se encuentra en la Remesa que hace parte de la Planilla
                ' se parametrizaron los Centros de Operación que maneja PLEXA
                ' 201-PGR, 202-GLP, 209-SECA, 210-ENFENCO SAN ROQUE, 211-ENFENCO TISQUIRAMA
                strTipoOper = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont1).Item("Tipo_Operacion")
                ' Centro Operacion SIESA
                intCodiCentOper = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont1).Item("Codigo_Centro_Operacion")
                ' Unidad Negocio SIESA
                If intCodiCentOper = 210 Or intCodiCentOper = 211 Then
                    strUnidadNegocio = "10" ' Si es igual 210-ENFENCO SAN ROQUE
                Else
                    strUnidadNegocio = "02" ' Si es diferente 210-ENFENCO SAN ROQUE
                End If
                ' Tipo Documento SIESA
                If intCodiCentOper = 201 Then
                    strTipoDocu = "P35"
                ElseIf intCodiCentOper = 202 Then
                    strTipoDocu = "P34"
                ElseIf intCodiCentOper = 209 Or intCodiCentOper = 210 Or intCodiCentOper = 211 Then
                    strTipoDocu = "P36"
                End If

                strSQL = "gsp_Consultar_Conductores_Legalizacion_Gastos_SIESA_PLEXA " & intCodigoEmpresa.ToString & "," & lonNumeLega.ToString
                dtsCondLegaGast = objGeneral.Retorna_Dataset(strSQL)

                If dtsCondLegaGast.Tables.Count > 0 Then

                    If dtsCondLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        ' Recorrer los conductores que pertenecen a la legalización
                        For intCon1 = 0 To dtsCondLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                            strFecha = Retorna_Fecha_Formato_SIESA(dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont1).Item("Fecha").ToString())
                            lonCodiCond = dtsCondLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Codigo")
                            strIdenCond = dtsCondLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Identificacion_Tercero").ToString()
                            dblSaldFavoEmpr = dtsCondLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Saldo_Favor_Empresa")
                            dblSaldFavoCond = dtsCondLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Saldo_Favor_Conductor")

                            xmlDocumento = New XmlDocument

                            xmlElementoRaiz = xmlDocumento.CreateElement("MyDataSet")
                            xmlDocumento.AppendChild(xmlElementoRaiz)

                            ' DOCUMENTO CONTABLE
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("Documento_contable_V02")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            'F350_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", intCodiCentOper)
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", strTipoDocu)
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", intConsecutivo.ToString) ' En SIESA se genera un nuevo consecutivo
                            ' F350_FECHA (Alfanumerico(YYYYMMDD))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_FECHA", strFecha)
                            ' F350_ID_TERCERO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TERCERO", strIdenCond)
                            ' F350_NOTAS (Alfanumerico(2000))
                            strValorCampo = "No.LEGALIZACION:" & lonNumeDocuLega.ToString
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_NOTAS", strValorCampo)

                            ' Consultar Detalle Legalizacion Gastos
                            Dim dtsDetaLegaGast As DataSet

                            strSQL = "gsp_Consultar_Detalle_Legalizacion_Gastos_SIESA_PLEXA " & intCodigoEmpresa.ToString() & ", " & lonNumeLega.ToString() & ", " & lonCodiCond.ToString()
                            dtsDetaLegaGast = objGeneral.Retorna_Dataset(strSQL)

                            If dtsDetaLegaGast.Tables.Count > 0 Then
                                If dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                                    ' Recorrer los Conceptos de Gasto de la Planilla Legalización
                                    For intCont2 = 0 To dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                                        strIdenProv = dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont2).Item("Identificacion_Tercero").ToString()
                                        intCodiConc = dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont2).Item("Codigo_Concepto").ToString()
                                        strNombConc = dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont2).Item("Nombre_Concepto").ToString()
                                        strNombConc = Eliminar_Caracteres_Especiales_SIESA(strNombConc)

                                        If intCodiConc = CONCEPTO_CUENTA_X_COBRAR_SALDO_FAVOR_EMPRESA Then
                                            ' Este Concepto CxC se generó en la anterior Legalización porque Saldo Favor Empresa > 0

                                            dblValConCxC = CDbl(dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont2).Item("Valor_Credito").ToString())
                                            lonCodCxCSalFavEmp = CDbl(dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont2).Item("ENDC_Codigo").ToString()) ' Código CxC Saldo Favor Empresa

                                            ' MOVIMIENTO CxP
                                            xmlElementoRoot = xmlDocumento.DocumentElement
                                            xmlElemento = xmlDocumento.CreateElement("Movimiento_CxP_V06")
                                            xmlElementoRoot.AppendChild(xmlElemento)
                                            xmlElementoRoot = xmlDocumento.DocumentElement

                                            'F350_ID_CO (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", intCodiCentOper)
                                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", strTipoDocu)
                                            ' F350_CONSEC_DOCTO (Entero(8))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", intConsecutivo.ToString)
                                            ' F351_ID_AUXILIAR (Entero (10))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "14201101") ' PARAMETRIZAR
                                            ' F350_ID_TERCERO (Alfanumerico(15))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenCond)
                                            ' F351_ID_CO_MOV (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", intCodiCentOper)
                                            ' F351_ID_UN (Alfanumerico(20))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", strUnidadNegocio)
                                            ' F351_ID_CCOSTO (Alfanumerico(15)) 
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                                            ' F351_VALOR_DB (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", "0")
                                            ' F351_VALOR_CR (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValConCxC)
                                            ' F351_VALOR_DB2 (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB2", "0")
                                            ' F351_VALOR_CR2 (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR2", dblValConCxC)
                                            ' F351_NOTAS (Alfanumerico(255))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", "")
                                            ' F353_ID_SUCURSAL (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", SUCURSAL_PLEXA_SIESA)
                                            ' F353_PREFIJO_CRUCE (Alfanumerico(20))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", "")
                                            ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", lonCodCxCSalFavEmp) ' Codigo de la CxC generada en la anterior legalizacion Saldo Favor Empresa
                                            ' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFecha)
                                            ' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFecha)
                                            ' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strFecha)
                                            ' F354_NOTAS (Alfanumerico(255))
                                            strValorCampo = "CxP RELACIONADA con CxC GENERADA POR SALDO FAVOR EMPRESA"
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strValorCampo)

                                        ElseIf intCodiConc = CONCEPTO_CUENTA_X_PAGAR_SALDO_FAVOR_CONDUCTOR Then
                                            ' Este Concepto CxP se generó en la anterior Legalización porque Saldo Favor Conductor > 0

                                            dblValConCxP = CDbl(dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont2).Item("Valor_Debito").ToString())
                                            lonCodCxPSalFavCon = CDbl(dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont2).Item("ENDC_Codigo").ToString()) ' Código CxP Saldo Favor Conductor

                                            ' MOVIMIENTO CxP
                                            xmlElementoRoot = xmlDocumento.DocumentElement
                                            xmlElemento = xmlDocumento.CreateElement("Movimiento_CxP_V06")
                                            xmlElementoRoot.AppendChild(xmlElemento)
                                            xmlElementoRoot = xmlDocumento.DocumentElement

                                            'F350_ID_CO (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", intCodiCentOper)
                                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", strTipoDocu)
                                            ' F350_CONSEC_DOCTO (Entero(8))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", intConsecutivo.ToString)
                                            ' F351_ID_AUXILIAR (Entero (10))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "24250801") ' PARAMETRIZAR
                                            ' F350_ID_TERCERO (Alfanumerico(15))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenCond)
                                            ' F351_ID_CO_MOV (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", intCodiCentOper)
                                            ' F351_ID_UN (Alfanumerico(20))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", strUnidadNegocio)
                                            ' F351_ID_CCOSTO (Alfanumerico(15)) 
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                                            ' F351_VALOR_DB (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblValConCxP)
                                            ' F351_VALOR_CR (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", "0")
                                            ' F351_VALOR_DB2 (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB2", dblValConCxP)
                                            ' F351_VALOR_CR2 (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR2", "0")
                                            ' F351_NOTAS (Alfanumerico(255))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", "")
                                            ' F353_ID_SUCURSAL (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", SUCURSAL_PLEXA_SIESA)
                                            ' F353_PREFIJO_CRUCE (Alfanumerico(20))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", "")
                                            ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", lonCodCxPSalFavCon) ' Codigo de la CxP generada en la anterior legalizacion Saldo Favor Conductor
                                            ' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFecha)
                                            ' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFecha)
                                            ' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strFecha)
                                            ' F354_NOTAS (Alfanumerico(255))
                                            strValorCampo = "CxP RELACIONADA con CxP GENERADA POR SALDO FAVOR CONDUCTOR"
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strValorCampo)

                                        Else
                                            ' CONCEPTOS DE GASTO LEGALIZACION NORMALES

                                            ' MOVIMIENTO CONTABLE (Conceptos de Gasto) 
                                            xmlElementoRoot = xmlDocumento.DocumentElement
                                            xmlElemento = xmlDocumento.CreateElement("Movimiento_contable_V04")
                                            xmlElementoRoot.AppendChild(xmlElemento)
                                            xmlElementoRoot = xmlDocumento.DocumentElement

                                            ' F350_ID_CO (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f350_id_co", intCodiCentOper)
                                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f350_id_tipo_docto", strTipoDocu)
                                            ' F350_CONSEC_DOCTO (Entero(8))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f350_consec_docto", intConsecutivo.ToString)
                                            ' F351_ID_AUXILIAR (Entero (10))
                                            strValorCampo = dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont2).Item("Codigo_Cuenta").ToString()
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f351_id_auxiliar", strValorCampo)
                                            ' F351_ID_TERCERO (Alfanumerico(15))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f351_id_tercero", strIdenProv)
                                            ' F351_ID_CO_MOV (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f351_id_co_mov", intCodiCentOper)
                                            ' F351_ID_UN (Alfanumerico(20))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f351_id_un", strUnidadNegocio)
                                            ' F351_ID_CCOSTO (Alfanumerico(15)) 
                                            strValorCampo = "2-" & dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont1).Item("Placa").ToString()
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f351_id_ccosto", strValorCampo)
                                            ' F351_VALOR_DB (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f351_valor_db", CDbl(dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont2).Item("Valor_Debito").ToString()))
                                            ' F351_VALOR_CR (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f351_valor_cr", CDbl(dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont2).Item("Valor_Credito").ToString()))
                                            ' F351_VALOR_DB2 (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f351_valor_db2", CDbl(dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont2).Item("Valor_Debito").ToString()))
                                            ' F351_VALOR_CR2 (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f351_valor_cr2", CDbl(dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont2).Item("Valor_Credito").ToString()))
                                            ' F351_VALOR_DB3 (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f351_valor_db3", CDbl(dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont2).Item("Valor_Debito").ToString()))
                                            ' F351_VALOR_CR3 (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f351_valor_cr3", CDbl(dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont2).Item("Valor_Credito").ToString()))
                                            ' F351_NOTAS (Alfanumerico(255))
                                            strNombConc = Eliminar_Caracteres_Especiales_SIESA(strNombConc)

                                            strValorCampo = "No.LEGALIZACION:" & lonNumeDocuLega.ToString & ", CONCEPTO GASTO:" & strNombConc
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f351_notas", strValorCampo)

                                        End If

                                    Next

                                    ' PENDIENTE crear SP Consulta
                                    ' Se consultan los Comprobante Egreso Anticipo relacionado con la Legalizacion Gastos y el Conductor para crear un Movimiento_CxP_V06 por cada una
                                    Dim dtsCompEgreAnti As DataSet, lonNumeCompEgreAnti As Long, dblValoCompEgreAnti As Double, strFechCompEgre As String

                                    strSQL = "SELECT ENDC.Numero, ENDC.Fecha, ENDC.Valor_Pago_Recaudo_Total, TERC.Numero_Identificacion"
                                    strSQL += " FROM Encabezado_Documento_Comprobantes ENDC, Terceros As TERC"
                                    strSQL += " WHERE ENDC.EMPR_Codigo = TERC.EMPR_Codigo"
                                    strSQL += " AND ENDC.TERC_Codigo_Titular = TERC.Codigo"
                                    strSQL += " AND ENDC.EMPR_Codigo = " & intCodigoEmpresa.ToString
                                    strSQL += " AND ENDC.TIDO_Codigo = " & TIDO_COMPROBANTE_EGRESO
                                    strSQL += " AND ENDC.ELGC_Numero = " & lonNumeLega.ToString()
                                    strSQL += " AND ENDC.TERC_Codigo_Titular = " & lonCodiCond.ToString
                                    strSQL += " AND ENDC.Estado = 1"
                                    strSQL += " AND ENDC.Anulado = 0"

                                    dtsCompEgreAnti = objGeneral.Retorna_Dataset(strSQL)

                                    If dtsCompEgreAnti.Tables.Count > 0 Then

                                        If dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                                            For intCont3 = 0 To dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                                                lonNumeCompEgreAnti = dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont3).Item("Numero").ToString()
                                                dblValoCompEgreAnti = dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont3).Item("Valor_Pago_Recaudo_Total").ToString()
                                                strIdenCond = dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont3).Item("Numero_Identificacion").ToString()
                                                strFechCompEgre = Retorna_Fecha_Formato_SIESA(dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont3).Item("Fecha").ToString())

                                                ' MOVIMIENTO CxP
                                                xmlElementoRoot = xmlDocumento.DocumentElement
                                                xmlElemento = xmlDocumento.CreateElement("Movimiento_CxP_V06")
                                                xmlElementoRoot.AppendChild(xmlElemento)
                                                xmlElementoRoot = xmlDocumento.DocumentElement

                                                'F350_ID_CO (Alfanumerico(3))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", intCodiCentOper)
                                                ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", strTipoDocu)
                                                ' F350_CONSEC_DOCTO (Entero(8))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", intConsecutivo.ToString)
                                                ' F351_ID_AUXILIAR (Entero (10))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "14201101") ' PARAMETRIZAR
                                                ' F350_ID_TERCERO (Alfanumerico(15))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenCond)
                                                ' F351_ID_CO_MOV (Alfanumerico(3))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", intCodiCentOper)
                                                ' F351_ID_UN (Alfanumerico(20))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", strUnidadNegocio)
                                                ' F351_ID_CCOSTO (Alfanumerico(15)) 
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                                                ' F351_VALOR_DB (Decimal)
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", "0")
                                                ' F351_VALOR_CR (Decimal)
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValoCompEgreAnti.ToString) ' Valor Comprobante Egreso Anticipo
                                                ' F351_VALOR_DB2 (Decimal)
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB2", "0")
                                                ' F351_VALOR_CR2 (Decimal)
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR2", dblValoCompEgreAnti.ToString) ' Valor Comprobante Egreso Anticipo
                                                ' F351_NOTAS (Alfanumerico(255))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", "")
                                                ' F353_ID_SUCURSAL (Alfanumerico(3))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", SUCURSAL_PLEXA_SIESA)
                                                ' F353_PREFIJO_CRUCE (Alfanumerico(20))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", lonNumeCompEgreAnti.ToString) ' Número Comprobante Egreso Anticipo
                                                ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", "")
                                                ' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFecha)
                                                ' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFecha)
                                                ' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strFecha)
                                                ' F354_NOTAS (Alfanumerico(255))
                                                strValorCampo = "No.LEGALIZACION:" & lonNumeDocuLega.ToString & ", No. EGRESO:" & lonNumeCompEgreAnti.ToString & ", No.PLANILLA:" & lonNumPlaDes.ToString
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strValorCampo)

                                            Next

                                        End If
                                    End If

                                    If dblSaldFavoEmpr > 0 Then
                                        ' Se crea una CxP si Saldo_Favor_Empresa > 0

                                        lonCodCxCSalFavEmp = Retorna_Cuenta_X_Cobrar_Saldo_Favor_Empresa(lonNumeDocuLega, lonCodiCond)

                                        ' MOVIMIENTO CxP, Nuevo Anticipo CxC
                                        xmlElementoRoot = xmlDocumento.DocumentElement
                                        xmlElemento = xmlDocumento.CreateElement("Movimiento_CxP_V06")
                                        xmlElementoRoot.AppendChild(xmlElemento)
                                        xmlElementoRoot = xmlDocumento.DocumentElement

                                        'F350_ID_CO (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", intCodiCentOper)
                                        ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", strTipoDocu)
                                        ' F350_CONSEC_DOCTO (Entero(8))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", intConsecutivo.ToString)
                                        ' F351_ID_AUXILIAR (Entero (10))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "14201101") ' Cuenta CxC Saldo Favor Empresa
                                        ' F350_ID_TERCERO (Alfanumerico(15))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenCond) ' Identificacion Conductor
                                        ' F351_ID_CO_MOV (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", intCodiCentOper)
                                        ' F351_ID_UN (Alfanumerico(20))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", strUnidadNegocio)
                                        ' F351_ID_CCOSTO (Alfanumerico(15)) 
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                                        ' F351_VALOR_DB (Decimal)
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblSaldFavoEmpr.ToString) ' Valor Saldo_Favor_Empresa
                                        ' F351_VALOR_CR (Decimal)
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", "0")
                                        ' F351_VALOR_DB2 (Decimal)
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB2", dblSaldFavoEmpr.ToString) ' Valor Saldo_Favor_Empresa
                                        ' F351_VALOR_CR2 (Decimal)
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR2", "0")
                                        ' F351_NOTAS (Alfanumerico(255))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", "")
                                        ' F353_ID_SUCURSAL (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", SUCURSAL_PLEXA_SIESA)
                                        ' F353_PREFIJO_CRUCE (Alfanumerico(20))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", "")
                                        ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", lonCodCxCSalFavEmp.ToString) ' Código CxC Saldo Favor Emrpresa
                                        ' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFecha)
                                        ' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFecha)
                                        ' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strFecha)
                                        ' F354_NOTAS (Alfanumerico(255))
                                        strValorCampo = "No.LEGALIZACION:" & lonNumeDocuLega.ToString & ", SALDO FAVOR EMPRESA CxC No." & lonCodCxCSalFavEmp.ToString & " "
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strValorCampo)


                                    ElseIf dblSaldFavoCond > 0 Then
                                        ' Se crea una CxP si Saldo Favor Conductor > 0

                                        lonCodCxPSalFavCon = Retorna_Cuenta_X_Pagar_Saldo_Favor_Conductor(lonNumeDocuLega, lonCodiCond)

                                        ' MOVIMIENTO CxP, Nuevo Anticipo CxC
                                        xmlElementoRoot = xmlDocumento.DocumentElement
                                        xmlElemento = xmlDocumento.CreateElement("Movimiento_CxP_V06")
                                        xmlElementoRoot.AppendChild(xmlElemento)
                                        xmlElementoRoot = xmlDocumento.DocumentElement

                                        'F350_ID_CO (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", intCodiCentOper)
                                        ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", strTipoDocu)
                                        ' F350_CONSEC_DOCTO (Entero(8))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", intConsecutivo.ToString)
                                        ' F351_ID_AUXILIAR (Entero (10))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "24250801") ' Cuenta CxP Conductor
                                        ' F350_ID_TERCERO (Alfanumerico(15))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenCond) ' Identificacion Conductor
                                        ' F351_ID_CO_MOV (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", intCodiCentOper)
                                        ' F351_ID_UN (Alfanumerico(20))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", strUnidadNegocio)
                                        ' F351_ID_CCOSTO (Alfanumerico(15)) 
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                                        ' F351_VALOR_DB (Decimal)
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", "0")
                                        ' F351_VALOR_CR (Decimal)
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblSaldFavoCond.ToString) ' Valor Saldo_Favor_Conductor
                                        ' F351_VALOR_DB2 (Decimal)
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB2", "0")
                                        ' F351_VALOR_CR2 (Decimal)
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR2", dblSaldFavoCond.ToString) ' Valor Saldo_Favor_Conductor
                                        ' F351_NOTAS (Alfanumerico(255))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", "")
                                        ' F353_ID_SUCURSAL (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", SUCURSAL_PLEXA_SIESA)
                                        ' F353_PREFIJO_CRUCE (Alfanumerico(20))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", "")
                                        ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", lonCodCxPSalFavCon.ToString) ' Código CxP Saldo Favor Conductor
                                        ' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFecha)
                                        ' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFecha)
                                        ' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strFecha)
                                        ' F354_NOTAS (Alfanumerico(255))
                                        strValorCampo = "No.LEGALIZACION:" & lonNumeDocuLega.ToString & ", SALDO FAVOR CONDUCTOR CxP No." & lonCodCxPSalFavCon.ToString & " "
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strValorCampo)

                                    End If

                                    strXML = xmlDocumento.InnerXml

                                    ' Invocar WEB SERVICE SELENE método Legalizar
                                    Me.objSelene = New WebServicePlexaSoapClient
                                    strRespuestaXML = objSelene.Legalizar(strXML)

                                    If Trim$(strRespuestaXML) = "Resultado Siesa: Importacion Exitosa" Then

                                        bolProcesoValido = True

                                    Else
                                        strErrorDetalleSIESA = Retorna_Error_Detalle_SIESA(strRespuestaXML)

                                        ' Actualizar el Comprobante Contable con Interfase_Contable = 0, mensaje de error retornado por Generic Transfer y NUMERO INTENTOS
                                        strSQL = "UPDATE Encabezado_Legalizacion_Gastos_Conductor SET Interfaz_Contable_Legalizacion = 0, Mensaje_Error = '" & strErrorDetalleSIESA & "', Fecha_Interfase_Contable = getDate(), Intentos_Interfase_Contable = ISNULL(Intentos_Interfase_Contable,0) + 1"
                                        strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                                        strSQL += " AND Numero = " & lonNumeLega

                                        ' Ejecutar UPDATE
                                        lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                                    End If
                                End If
                            End If

                            intConsecutivo = intConsecutivo + 1

                        Next

                        If bolProcesoValido Then

                            ' Actualizar Encabezado_Legalizacion_Gastos_Conductor con Interfaz_Contable_Factura = 1 y Fecha_Interfase_Contable = getDate()
                            strSQL = "UPDATE Encabezado_Legalizacion_Gastos_Conductor SET Interfaz_Contable_Legalizacion = 1, Fecha_Interfase_Contable = getDate()"
                            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                            strSQL += " AND Numero = " & lonNumeLega

                            ' Ejecutar UPDATE
                            lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                        End If

                    End If

                End If
            Next

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Crear_XML_Legalizacion_Gastos: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros  
    ' Fecha Creación: 11-JUN-2021
    ' Observaciones: 

    Private Sub Proceso_Factura_Ventas_SELENE()
        Try
            Dim dtsFactVent As DataSet

            While (True)
                Me.objGeneral.Guardar_Mensaje_Log("Inició Proceso Interfaz Factura Ventas")
                strSQL = "gsp_Consultar_Encabezado_Facturas_SIESA_PLEXA " & intCodigoEmpresa.ToString '& ", 120425" ' TEMPORAL PRUEBAS
                dtsFactVent = objGeneral.Retorna_Dataset(strSQL)

                If dtsFactVent.Tables.Count > 0 Then

                    If dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        Me.objGeneral.Guardar_Mensaje_Log("Número Facturas a Procesar: " & dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows.Count)

                        Call Crear_XML_Factura_Ventas_SELENE(dtsFactVent)

                    End If

                End If

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)

            End While

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Proceso_Factura_Ventas: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros  
    ' Fecha Creación: 11-JUN-2021
    ' Observaciones: 

    Private Sub Crear_XML_Factura_Ventas_SELENE(ByVal dtsFactVent As DataSet)
        Try
            Dim strXML As String, strRespuestaXML As String, strValorCampoDefec As String = "", strValorCampo As String = "", strErrorDetalleSIESA As String
            Dim lonNumeFact As Long, lonNumeDocuFact As Long, strFechFact As Long, strIdenClie As String, strObseFact As String, strUnidNego As String
            Dim lonNumRegFac As Long, dblCantReme As Double, dblValoFletReme As Double, strPlacaRemesa As String
            Dim dtsDetalleFactVent As DataSet
            ' Variables SIESA
            Dim strTipoOper As String, intCodiCentOper As Integer, strUnidadNegocio As String, strBodega As String, strMotivo As String
            Dim strUnidMediProd As String, strListaPrecio As String, strItem As String

            Dim xmlDocumento As XmlDocument
            Dim xmlDeclaracion As XmlDeclaration
            Dim xmlElementoRaiz As XmlElement
            Dim xmlElementoRoot As XmlElement
            Dim xmlElemento As XmlElement

            For intCon = 0 To dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                lonNumeFact = dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero")
                lonNumeDocuFact = dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString()
                strFechFact = Retorna_Fecha_Formato_SIESA(dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString())
                strIdenClie = dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Tercero").ToString()
                strObseFact = Mid$(dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Observaciones").ToString(), 1, 250)

                strListaPrecio = Consultar_Lista_Precios_Cliente_SELENE(strIdenClie)

                strSQL = "gsp_Consultar_Detalle_Factura_SIESA_PLEXA " & intCodigoEmpresa.ToString & ", " & lonNumeFact
                dtsDetalleFactVent = objGeneral.Retorna_Dataset(strSQL)

                If dtsDetalleFactVent.Tables.Count > 0 Then
                    If dtsDetalleFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        ' Se toma la información de Centro Operación de la primera Remesa
                        ' En el catalogo Lineas Productos Transportados(CATA_LIPT_Codigo) relacionado al Producto Transportado que se encuentra en la Remesa 
                        ' se parametrizaron los Centros de Operación que maneja PLEXA
                        ' 201-PGR, 202-GLP, 209-SECA, 210-ENFENCO SAN ROQUE, 211-ENFENCO TISQUIRAMA
                        strTipoOper = dtsDetalleFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Tipo_Operacion")
                        ' Centro Operacion SIESA
                        intCodiCentOper = dtsDetalleFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Codigo_Centro_Operacion")
                        ' Unidad Negocio SIESA
                        If intCodiCentOper = 210 Or intCodiCentOper = 211 Then
                            strUnidadNegocio = "10" ' Si es igual 210-ENFENCO SAN ROQUE
                        Else
                            strUnidadNegocio = "02" ' Si es diferente 210-ENFENCO SAN ROQUE
                        End If

                        If intCodiCentOper = 201 Then ' PGR
                            strBodega = "TPGR"
                            strMotivo = "ST"
                            strUnidMediProd = "GLS"
                            strItem = "39"

                        ElseIf intCodiCentOper = 202 Then ' GLP
                            strBodega = "TGLP"
                            strMotivo = "SP"
                            strUnidMediProd = "KGM"
                            strItem = "40"

                        ElseIf intCodiCentOper = 209 Then ' SECA
                            strBodega = "CARGA"
                            strMotivo = "SC"
                            strUnidMediProd = "TON"
                            strItem = "130"
                        Else
                            strBodega = "CARGA"
                            strMotivo = "SC"
                            strUnidMediProd = "TON"
                            strItem = "130"

                        End If

                        ' DOCUMENTO 
                        xmlDocumento = New XmlDocument
                        xmlElementoRaiz = xmlDocumento.CreateElement("MyDataSet")
                        xmlDocumento.AppendChild(xmlElementoRaiz)

                        xmlElementoRoot = xmlDocumento.DocumentElement
                        xmlElemento = xmlDocumento.CreateElement("Documento_V02")
                        xmlElementoRoot.AppendChild(xmlElemento)
                        xmlElementoRoot = xmlDocumento.DocumentElement

                        'F350_ID_CO (Alfanumerico(3))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", intCodiCentOper)
                        'F350_CONSEC_DOCTO (Entero(8))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeDocuFact)
                        ' F350_FECHA (Alfanumerico(YYYYMMDD))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_FECHA", strFechFact)
                        ' F350_ID_TERCERO (Alfanumerico(15))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TERCERO", strIdenClie)
                        ' f461_id_sucursal_fact (Alfanumerico(3))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f461_id_sucursal_fact", SUCURSAL_PLEXA_SIESA)
                        ' f461_id_co_fact (Alfanumerico(3))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f461_id_co_fact", intCodiCentOper)
                        ' f461_id_tercero_rem (Alfanumerico(15))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f461_id_tercero_rem", strIdenClie)
                        ' f461_id_sucursal_rem (Alfanumerico(3))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f461_id_sucursal_rem", SUCURSAL_PLEXA_SIESA)
                        ' f461_id_cond_pago (Alfanumerico(3))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f461_id_cond_pago", ID_CONDICION_PAGO)
                        ' f461_notas (Alfanumerico(2000))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f461_notas", "Observaciones:" & strObseFact)

                        For intdet = 0 To dtsDetalleFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                            lonNumRegFac = dtsDetalleFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intdet).Item("NumeroRegistro").ToString()
                            dblCantReme = dtsDetalleFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intdet).Item("Cantidad").ToString()
                            dblValoFletReme = dtsDetalleFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intdet).Item("TotalFlete").ToString()
                            strPlacaRemesa = "2-" & dtsDetalleFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intdet).Item("Placa").ToString()

                            ' MOVIMIENTO 
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("Movimiento_V11")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' f470_id_co (Alfanumerico (3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_co", intCodiCentOper)
                            ' f470_consec_docto (Entero (8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_consec_docto", lonNumeDocuFact)
                            ' f470_nro_registro (Entero (10))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_nro_registro", lonNumRegFac)
                            ' f470_id_bodega (Alfanumerico (5))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_bodega", strBodega)
                            ' f470_id_motivo (Alfanumerico (2))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_motivo", strMotivo)
                            ' f470_id_co_movto (Alfanumerico (3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_co_movto", intCodiCentOper)
                            ' f470_id_ccosto_movto (Alfanumerico (15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_ccosto_movto", strPlacaRemesa)
                            ' f470_id_lista_precio (Alfanumerico (3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_lista_precio", strListaPrecio)
                            ' f470_id_unidad_medida (Alfanumerico (4))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_unidad_medida", strUnidMediProd)
                            ' f470_cant_base (Decimal (20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_cant_base", dblCantReme)
                            ' f470_vlr_bruto (Decimal (20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_vlr_bruto", dblValoFletReme)
                            ' f470_id_item (Entero (7))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_item", strItem)
                            ' f470_id_un_movto (Alfanumerico (20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_un_movto", strUnidadNegocio)
                        Next

                        ' Cuota CXC 
                        xmlElementoRoot = xmlDocumento.DocumentElement
                        xmlElemento = xmlDocumento.CreateElement("CxC_V01")
                        xmlElementoRoot.AppendChild(xmlElemento)
                        xmlElementoRoot = xmlDocumento.DocumentElement

                        ' F350_ID_CO (Alfanumerico(3))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", intCodiCentOper) ' PENDIENTE Definir Centro Operación
                        ' F350_CONSEC_DOCTO (Entero(8))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeDocuFact)
                        ' F353_FECHA_VCTO (Alfanumerico(8))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechFact)
                        ' F353_FECHA_VCTO_DSCTO_PP (Alfanumerico(8))
                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechFact)

                        strXML = xmlDocumento.InnerXml

                        ' Invocar WEB SERVICE SELENE método Facturar
                        Me.objSelene = New WebServicePlexaSoapClient
                        strRespuestaXML = objSelene.Facturar(strXML)

                        If Trim$(strRespuestaXML) = "Resultado Siesa: Importacion Exitosa" Then

                            ' Actualizar Encabezado_Facturas con Interfaz_Contable_Factura = 1 y Fecha_Interfase_Contable = getDate()
                            strSQL = "UPDATE Encabezado_Facturas SET Interfaz_Contable_Factura = 1, Fecha_Interfase_Contable = getDate()"
                            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                            strSQL += " AND Numero = " & lonNumeFact

                            ' Ejecutar UPDATE
                            lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                        Else
                            strErrorDetalleSIESA = Retorna_Error_Detalle_SIESA(strRespuestaXML)

                            ' Actualizar el Comprobante Contable con Interfase_Contable = 0, mensaje de error retornado por Generic Transfer y NUMERO INTENTOS
                            strSQL = "UPDATE Encabezado_Facturas SET Interfaz_Contable_Factura = 0, Mensaje_Error = '" & strErrorDetalleSIESA & "', Fecha_Interfase_Contable = getDate(), Intentos_Interfase_Contable = Intentos_Interfase_Contable + 1"
                            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                            strSQL += " AND Numero = " & lonNumeFact

                            ' Ejecutar UPDATE
                            lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                        End If

                    End If
                End If

            Next

        Catch ex As Exception

            Me.objGeneral.Guardar_Mensaje_Log("Error Crear_XML_Factura_Ventas_SELENE: " & ex.Message)

        End Try
    End Sub

    Private Function Consultar_Lista_Precios_Cliente_SELENE(strIdentClie As String) As String
        Try
            Dim strRespuestaXML As String

            ' Invocar WEB SERVICE CONSULTA SELENE método ListaPrecios
            Me.objSelene = New WebServicePlexaSoapClient
            strRespuestaXML = objSelene.ListaPrecio(strIdentClie)

            Consultar_Lista_Precios_Cliente_SELENE = "G97" ' Retorna tambien 99

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Consultar_Lista_Precios_Cliente_SELENE: " & ex.Message)
        End Try

    End Function

#End Region

#Region "Funciones Generales"

    Public Sub AdicionarNodoXml(ByRef xmlDocumento As XmlDocument, ByRef xmlElementoRoot As XmlElement, strElemento As String, strElementoNodo As String)
        Try
            xmlElementoRoot.LastChild.AppendChild(xmlDocumento.CreateElement(strElemento))
            xmlElementoRoot.LastChild.LastChild.AppendChild(xmlDocumento.CreateTextNode(strElementoNodo))

        Catch ex As Exception

        End Try
    End Sub

    Private Sub Cargar_Datos_App_Config()
        Try
            ' Generales
            intCodigoEmpresa = ConfigurationSettings.AppSettings.Get("Empresa")
            lonIntervaloMilisegundosEjecucionProceso = ConfigurationSettings.AppSettings.Get("IntervaloMilisegundosEjecucionProceso")
            strRutaArchivoLog = ConfigurationSettings.AppSettings("RutaArchivoLog").ToString()
            strRutaArchivoXML = ConfigurationSettings.AppSettings("RutaArchivoXML").ToString()

        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Error Cargar_Datos_App_Config: " & ex.Message)
        End Try
    End Sub

    Private Function Retorna_Valor_Tabla_SIESA(strTabla As String, strValoGest As String) As String
        Try
            Dim strRetorna As String = ""

            If strTabla = "TINA" Then

                Select Case strValoGest
                    Case serSIESA.TIPO_NATURALEZA_NATURAL_GESTRANS : strRetorna = serSIESA.TIPO_NATURALEZA_NATURAL_SIESA.ToString
                    Case serSIESA.TIPO_NATURALEZA_JURIDICA_GESTRANS : strRetorna = serSIESA.TIPO_NATURALEZA_JURIDICA_SIESA.ToString
                End Select

            ElseIf strTabla = "TIID" Then

                Select Case strValoGest
                    Case serSIESA.TIPO_IDENTIFICACION_CEDULA_GESTRANS : strRetorna = serSIESA.TIPO_IDENTIFICACION_CEDULA_SIESA.ToString
                    Case serSIESA.TIPO_IDENTIFICACION_EXTRANJERIA_GESTRANS : strRetorna = serSIESA.TIPO_IDENTIFICACION_EXTRANJERIA_SIESA.ToString
                    Case serSIESA.TIPO_IDENTIFICACION_NIT_GESTRANS : strRetorna = serSIESA.TIPO_IDENTIFICACION_NIT_SIESA.ToString
                End Select
            End If

            Return strRetorna
        Catch ex As Exception


        End Try

    End Function

    Private Function Retorna_Fecha_Formato_SIESA(ByVal datFecha As Date) As String
        Dim strFecha As String = ""

        Try
            strFecha = datFecha.Year.ToString + String.Format("{0:MM}", datFecha) + String.Format("{0:dd}", datFecha)
        Catch ex As Exception

        End Try
        Return strFecha

    End Function

    Private Function Eliminar_Caracteres_Especiales_SIESA(ByVal strCadena As String) As String
        Try

            strCadena = strCadena.Replace("(", " ")
            strCadena = strCadena.Replace(")", " ")
            strCadena = strCadena.Replace("Á", " ")
            strCadena = strCadena.Replace("É", " ")
            strCadena = strCadena.Replace("Í", " ")
            strCadena = strCadena.Replace("Ó", " ")
            strCadena = strCadena.Replace("Ú", " ")
            strCadena = strCadena.Replace("á", " ")
            strCadena = strCadena.Replace("é", " ")
            strCadena = strCadena.Replace("í", " ")
            strCadena = strCadena.Replace("ó", " ")
            strCadena = strCadena.Replace("ú", " ")
            strCadena = strCadena.Replace("ñ", " ")
            strCadena = strCadena.Replace("*", " ")
            strCadena = strCadena.Replace("/", " ")
            strCadena = strCadena.Replace("\", " ")

        Catch ex As Exception

        End Try
        Return strCadena

    End Function

    Private Function Retorna_Error_Detalle_SIESA(strMensajeSIESA As String) As String

        ' Ejemplo Mensaje Retorna SIESA
        'strMensajeSIESA = "Error al importar el plano<NewDataSet>"
        'strMensajeSIESA += "<Table>"
        'strMensajeSIESA += "<f_nro_linea>2</f_nro_linea>"
        'strMensajeSIESA += "<f_tipo_reg>0350</f_tipo_reg>"
        'strMensajeSIESA += "<f_subtipo_reg>00</f_subtipo_reg>"
        'strMensajeSIESA += "<f_version>02</f_version>"
        'strMensajeSIESA += "<f_nivel>00</f_nivel>"
        'strMensajeSIESA += "<f_valor>1-201-A1-01000278</f_valor>"
        'strMensajeSIESA += "<f_detalle>La fecha del documento debe estar abierta para el modulo Contabilidad. Documento: 201-A1-1000278.</f_detalle>"
        'strMensajeSIESA += "</Table>"
        'strMensajeSIESA += "</NewDataSet>"
        'strMensajeSIESA += "Se genero el plano correctamente"

        Dim intPosiInicDeta As Integer, intPosiFinaDeta As Integer, strDetalle As String = ""

        Try

            intPosiInicDeta = InStr(strMensajeSIESA, "<f_detalle>")
            intPosiFinaDeta = InStr(strMensajeSIESA, "</f_detalle>")

            If intPosiInicDeta > 0 Then
                strDetalle = strMensajeSIESA.Substring(intPosiInicDeta + 10, intPosiFinaDeta - intPosiInicDeta - 12)
            Else
                strDetalle = Mid$(Trim$(strMensajeSIESA), 1, 250)
            End If

        Catch ex As Exception

        End Try
        Retorna_Error_Detalle_SIESA = strDetalle

    End Function

    Private Function Retorna_Cuenta_X_Cobrar_Saldo_Favor_Empresa(ByVal lonNumeLega As Long, ByVal lonCodiCond As Long) As Long
        Dim strSQL As String = "", dtsCxC As DataSet, lonCodCxC As Long = 0

        Try
            strSQL = "SELECT Codigo FROM Encabezado_Documento_Cuentas"
            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " AND TIDO_Codigo = 80" ' Cuenta x Cobrar
            strSQL += " AND TERC_Codigo = " & lonCodiCond.ToString()
            strSQL += " AND CATA_DOOR_Codigo = 2616" ' Legalización Planilla Masivo
            strSQL += " AND Documento_Origen = " & lonNumeLega.ToString()  ' No Legalizacion

            dtsCxC = objGeneral.Retorna_Dataset(strSQL)

            If dtsCxC.Tables.Count > 0 Then
                If dtsCxC.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                    lonCodCxC = dtsCxC.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Codigo").ToString()
                End If
            End If
        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Retorna_Cuenta_X_Cobrar_Saldo_Favor_Empresa: " & ex.Message)
        End Try

        Return lonCodCxC

    End Function

    Private Function Retorna_Cuenta_X_Pagar_Saldo_Favor_Conductor(ByVal lonNumeLega As Long, ByVal lonCodiCond As Long) As Long
        Dim strSQL As String = "", dtsCxP As DataSet, lonCodCxP As Long = 0

        Try
            strSQL = "SELECT Codigo FROM Encabezado_Documento_Cuentas"
            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
            strSQL += " AND TIDO_Codigo = 85" ' Cuenta x Pagar
            strSQL += " AND TERC_Codigo = " & lonCodiCond.ToString()
            strSQL += " AND CATA_DOOR_Codigo = 2616" ' Legalización Planilla Masivo
            strSQL += " AND Documento_Origen = " & lonNumeLega.ToString()  ' No Legalizacion

            dtsCxP = objGeneral.Retorna_Dataset(strSQL)

            If dtsCxP.Tables.Count > 0 Then
                If dtsCxP.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                    lonCodCxP = dtsCxP.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Codigo").ToString()
                End If
            End If
        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Retorna_Cuenta_X_Pagar_Saldo_Favor_Conductor: " & ex.Message)
        End Try

        Return lonCodCxP

    End Function

#End Region

#Region "Rutinas Integración WS GENERICTRANSFER"

    ' Autor: Jesus Cuadros 
    ' Fecha Creación: 25-JUN-2020
    ' Observaciones Creación: Se consulta el Encabezado y Detalle de Comprobantes Contables, el cual debe traer 2 registros por comprobante
    ' Fecha Modificación: 
    ' Observaciones Modificación: 

    Private Sub Proceso_Comprobantes_Egreso()
        Try
            Dim dtsCompCont As DataSet

            While (True)
                Me.objGeneral.Guardar_Mensaje_Log("Inició Proceso Interfaz Anticipos(Comprobantes Egreso)")

                strSQL = "gps_Obtenter_Comprobantes_Egresos_SIESA " & intCodigoEmpresa.ToString
                dtsCompCont = objGeneral.Retorna_Dataset(strSQL)

                If dtsCompCont.Tables.Count > 0 Then

                    If dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        Call Crear_XML_Comprobantes_Egreso(dtsCompCont)

                    End If

                End If

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)

            End While

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Proceso_Terceros: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros 
    ' Fecha Creación: 25-JUN-2020
    ' Observaciones Creación: Se consulta el Encabezado y Detalle de Legalizacion de Gastos
    ' Fecha Modificación: 
    ' Observaciones Modificación: 

    Private Sub Proceso_Legalizacion_Gastos()
        Try
            Dim dtsLegaGast As DataSet

            While (True)
                Me.objGeneral.Guardar_Mensaje_Log("Inició Proceso Interfaz Legalizacion Gastos Conductor")

                strSQL = "gps_Obtenter_Encabezado_Legalizacion_Gastos_SIESA " & intCodigoEmpresa.ToString '& ", 142" ' TEMPORAL PRUEBA

                dtsLegaGast = objGeneral.Retorna_Dataset(strSQL)

                If dtsLegaGast.Tables.Count > 0 Then

                    If dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        Call Crear_XML_Legalizacion_Gastos(dtsLegaGast)

                    End If

                End If

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)

            End While

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Proceso_Legalizacion_Gastos: " & ex.Message)
        End Try
    End Sub

    Private Sub Proceso_Factura_Ventas()
        Try
            Dim dtsFactVent As DataSet

            While (True)
                Me.objGeneral.Guardar_Mensaje_Log("Inició Proceso Interfaz Factura Ventas")
                strSQL = "gps_Obtenter_Encabezado_Facturas_SIESA " & intCodigoEmpresa.ToString
                dtsFactVent = objGeneral.Retorna_Dataset(strSQL)

                If dtsFactVent.Tables.Count > 0 Then

                    If dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        Call Crear_XML_Factura_Ventas(dtsFactVent)

                    End If

                End If

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)

            End While

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Proceso_Factura_Ventas: " & ex.Message)
        End Try
    End Sub

    Private Sub Proceso_Factura_Provision()
        Try
            Dim dtsFactProv As DataSet

            While (True)
                Me.objGeneral.Guardar_Mensaje_Log("Inició Proceso Interfaz Factura Provision")
                strSQL = "gps_Obtenter_Encabezado_Provision_SIESA " & intCodigoEmpresa.ToString
                dtsFactProv = objGeneral.Retorna_Dataset(strSQL)

                If dtsFactProv.Tables.Count > 0 Then

                    If dtsFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        Call Crear_XML_Factura_Provision(dtsFactProv)

                    End If

                End If

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)

            End While

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Proceso_Factura_Provision: " & ex.Message)
        End Try
    End Sub

    Private Sub Proceso_Liquidacion_Despachos()
        Try
            Dim dtsLiquDesp As DataSet

            While (True)
                Me.objGeneral.Guardar_Mensaje_Log("Inició Proceso Liquidacion Despachos")
                strSQL = "gps_Obtenter_Encabezado_Liquidacion_Despachos_SIESA " & intCodigoEmpresa.ToString
                dtsLiquDesp = objGeneral.Retorna_Dataset(strSQL)

                If dtsLiquDesp.Tables.Count > 0 Then

                    If dtsLiquDesp.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        Call Crear_XML_Liquidacion_Despachos(dtsLiquDesp)

                    End If

                End If

            End While

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Proceso_Liquidacion_Despachos: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros 
    ' Fecha Creación: 25-JUN-2020
    ' Observaciones El XML se arma con una sección <Documento> que corresponde a un encabezado y dos secciones de <Movimiento> con las cuentas PUC y valores Crédito y Débito
    ' Fecha Modificación: 
    ' Observaciones Modificación: 

    Private Sub Crear_XML_Comprobantes_Egreso(ByVal dtsCompCont As DataSet)
        Try
            Dim strXML As String, strPlanoXML As String, strValorCampoDefec As String = "", strValorCampo As String = "", strErrorDetalleSIESA As String
            Dim lonNumeComp As Long, lonNumeCompAnte As Long

            Dim xmlDocumento As XmlDocument
            Dim xmlDeclaracion As XmlDeclaration
            Dim xmlElementoRaiz As XmlElement
            Dim xmlElementoRoot As XmlElement
            Dim xmlElemento As XmlElement

            For intCon = 0 To dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                lonNumeComp = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero")

                If lonNumeComp <> lonNumeCompAnte Then

                    xmlDocumento = New XmlDocument

                    'xmlDeclaracion = xmlDocumento.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
                    'xmlDocumento.AppendChild(xmlDeclaracion)

                    xmlElementoRaiz = xmlDocumento.CreateElement("Conector")
                    xmlDocumento.AppendChild(xmlElementoRaiz)

                    ' DOCUMENTO 
                    xmlElementoRoot = xmlDocumento.DocumentElement
                    xmlElemento = xmlDocumento.CreateElement("Documento")
                    xmlElementoRoot.AppendChild(xmlElemento)
                    xmlElementoRoot = xmlDocumento.DocumentElement

                    ' F350_ID_CO (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_PLEXA_SIESA)
                    ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_EGRESO_PLEXA_SIESA)
                    ' F350_CONSEC_DOCTO (Entero(8))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString())
                    ' F350_FECHA (Alfanumerico(YYYYMMDD))
                    strValorCampo = Retorna_Fecha_Formato_SIESA(dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString())
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_FECHA", strValorCampo)
                    ' F350_ID_TERCERO (Alfanumerico(15))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TERCERO", dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Tercero").ToString())
                    ' F350_NOTAS (Alfanumerico(255))
                    strValorCampo = Mid$(dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Observaciones").ToString(), 1, 250)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_NOTAS", strValorCampo)

                    ' MOVIMIENTO 
                    xmlElementoRoot = xmlDocumento.DocumentElement
                    xmlElemento = xmlDocumento.CreateElement("Movimiento")
                    xmlElementoRoot.AppendChild(xmlElemento)
                    xmlElementoRoot = xmlDocumento.DocumentElement

                    ' F350_ID_CO (Alfanumerico (3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_PLEXA_SIESA)
                    ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_EGRESO_PLEXA_SIESA)
                    ' F350_CONSEC_DOCTO (Entero(8))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString())

                    ' F351_ID_AUXILIAR (Alfanumerico(20))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "24559001")

                    ' F351_ID_TERCERO (Alfanumerico (15))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Tercero").ToString())
                    ' F351_ID_CO_MOV (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Centro_Operacion_Movimiento").ToString())
                    ' F351_ID_UN (Alfanumerico(20) 05=PASAJEROS)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_PLEXA_SIESA)
                    ' F351_VALOR_DB (Decimal)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", Convert.ToDouble(dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Debito").ToString()))
                    ' F351_VALOR_CR (Decimal)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", Convert.ToDouble(dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Credito").ToString()))
                    ' F351_VALOR_DB2 (Decimal)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB2", Convert.ToDouble(dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Debito").ToString()))
                    ' F351_VALOR_CR2 (Decimal)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR2", Convert.ToDouble(dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Credito").ToString()))
                    ' F353_ID_SUCURSAL (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", SUCURSAL_PLEXA_SIESA)

                    ' F353_PREFIJO_CRUCE (Alfanumerico(20))
                    ' CAMBIO SOLICITADO POR GIOVANNY BARRERO 14-OCT-2020, ENVIAR NUMERO COMPROBANTE EGRESO EN LUGAR DE FECHA
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString())

                    strValorCampo = Retorna_Fecha_Formato_SIESA(dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString())

                    ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", 0)

                    ' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strValorCampo)
                    ' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strValorCampo)
                    ' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strValorCampo)

                    ' F354_NOTAS (Alfanumerico(255))
                    strValorCampo = Mid$(dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Observaciones").ToString(), 1, 250)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strValorCampo)

                Else
                    ' MOVIMIENTO 
                    xmlElementoRoot = xmlDocumento.DocumentElement
                    xmlElemento = xmlDocumento.CreateElement("Movimiento")
                    xmlElementoRoot.AppendChild(xmlElemento)
                    xmlElementoRoot = xmlDocumento.DocumentElement

                    ' F350_ID_CO (Alfanumerico (3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_PLEXA_SIESA)
                    ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_EGRESO_PLEXA_SIESA)
                    ' F350_CONSEC_DOCTO (Entero(8))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString())

                    ' F351_ID_AUXILIAR (Alfanumerico(20))
                    'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Codigo_Cuenta").ToString())
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "14201101") ' TEMPORAL POR PARAMETRIZACION - Santiago Correa

                    ' F351_ID_TERCERO (Alfanumerico (15))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Tercero").ToString())
                    ' F351_ID_CO_MOV (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Centro_Operacion_Movimiento").ToString())
                    ' F351_ID_UN (Alfanumerico(20) 05=PASAJEROS)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_PLEXA_SIESA)
                    ' F351_VALOR_DB (Decimal)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", Convert.ToDouble(dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Debito").ToString()))
                    ' F351_VALOR_CR (Decimal)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", Convert.ToDouble(dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Credito").ToString()))
                    ' F351_VALOR_DB2 (Decimal)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB2", Convert.ToDouble(dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Debito").ToString()))
                    ' F351_VALOR_CR2 (Decimal)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR2", Convert.ToDouble(dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Credito").ToString()))
                    ' F353_ID_SUCURSAL (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", SUCURSAL_PLEXA_SIESA)

                    ' F353_PREFIJO_CRUCE (Alfanumerico(20))
                    ' CAMBIO SOLICITADO POR GIOVANNY BARRERO 14-OCT-2020, ENVIAR NUMERO COMPROBANTE EGRESO EN LUGAR DE FECHA
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString())

                    strValorCampo = Retorna_Fecha_Formato_SIESA(dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString())

                    ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", 0)
                    ' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strValorCampo)
                    ' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strValorCampo)
                    ' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strValorCampo)
                    ' F354_NOTAS (Alfanumerico(255))
                    strValorCampo = Mid$(dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Observaciones").ToString(), 1, 250)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strValorCampo)

                    strXML = xmlDocumento.InnerXml
                    Dim TMPStrRutaArchivoXML As String = strRutaArchivoXML

                    ' Invocar servicio GENERIC TRANSFER método ImportarDatosXML
                    'Me.objGenericTransfer = New wsGenerarPlanoSoapClient
                    'strPlanoXML = objGenericTransfer.ImportarDatosXML(93908, "ANTICIPOS", 2, "1", "gt", "gt", strXML, TMPStrRutaArchivoXML)

                    If strPlanoXML = "Importacion exitosa" Then

                        ' Actualizar el Comprobante Contable con Interfase_Contable = 1 y Fecha_Interfase_Contable = getDate()
                        strSQL = "UPDATE Encabezado_Comprobante_Contables SET Interfase_Contable = 1, Fecha_Interfase_Contable = getDate()"
                        strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                        strSQL += " AND Numero = " & lonNumeComp

                        ' Ejecutar UPDATE
                        lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                    Else
                        strErrorDetalleSIESA = Retorna_Error_Detalle_SIESA(strPlanoXML)

                        ' Actualizar el Comprobante Contable con Interfase_Contable = 0, mensaje de error retornado por Generic Transfer y NUMERO INTENTOS
                        strSQL = "UPDATE Encabezado_Comprobante_Contables SET Interfase_Contable = 0, Mensaje_Error = '" & strErrorDetalleSIESA & "', Fecha_Interfase_Contable = getDate(), Intentos_Interfase_Contable = ISNULL(Intentos_Interfase_Contable,0) + 1"
                        strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                        strSQL += " AND Numero = " & lonNumeComp

                        ' Ejecutar UPDATE
                        lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                    End If

                End If

                lonNumeCompAnte = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero")

            Next

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Crear_XML_Comprobantes_Egreso: " & ex.Message)
        End Try
    End Sub

    ' Autor: Santiago Correa  
    ' Fecha Creación: 18-AGO-2020
    ' Observaciones El XML se arma con una sección <Documento> que corresponde a un encabezado y cuantos conceptos <Movimiento> tenga esa legalización
    ' Fecha Modificación: 
    ' Observaciones Modificación: 

    Private Sub Crear_XML_Legalizacion_Gastos(ByVal dtsLegaGast As DataSet)
        Try
            Dim strXML As String, strPlanoXML As String, strFecha As String, strValorCampoDefec As String = "", strValorCampo As String = "", strErrorDetalleSIESA As String
            Dim intCont1 As Integer, intCont2 As Integer, intCont3 As Integer
            Dim lonNumeLega As Long, lonNumeDocuLega As Long, lonCodiCond As Long = 0, strIdenCond As String = ""
            Dim strIdenProv As String = "", dblSaldFavoEmpr As Double = 0, lonNumPlaDes As Long = 0, bolProcesoValido As Boolean
            Dim intCodiConc As Integer, lonNumConCxC As Long = 0, dblValConCxC As Double = 0, lonCodCxCSalFavEmp As Long

            Dim xmlDocumento As XmlDocument
            'Dim xmlDeclaracion As XmlDeclaration
            Dim xmlElementoRaiz As XmlElement
            Dim xmlElementoRoot As XmlElement
            Dim xmlElemento As XmlElement

            For intCon = 0 To dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                Dim dtsCondLegaGast As DataSet
                Dim intConsecutivo As Integer = 1

                lonNumeLega = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Numero")
                lonNumeDocuLega = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(0).Item("Numero_Documento")

                strSQL = "gps_Obtenter_Detalle_Conductores_Legalizacion_Gastos_SIESA " & intCodigoEmpresa.ToString & ", " & lonNumeLega.ToString
                dtsCondLegaGast = objGeneral.Retorna_Dataset(strSQL)

                If dtsCondLegaGast.Tables.Count > 0 Then
                    If dtsCondLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        ' Recorrer los conductores que pertenecen a la legalización
                        For intCon1 = 0 To dtsCondLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                            strFecha = Retorna_Fecha_Formato_SIESA(dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont1).Item("Fecha").ToString())
                            lonNumPlaDes = dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont1).Item("ENPD_Numero").ToString()

                            lonCodiCond = dtsCondLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Codigo")
                            strIdenCond = dtsCondLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Identificacion_Tercero").ToString()
                            dblSaldFavoEmpr = dtsCondLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Saldo_Favor_Empresa")

                            xmlDocumento = New XmlDocument

                            xmlElementoRaiz = xmlDocumento.CreateElement("Conector")
                            xmlDocumento.AppendChild(xmlElementoRaiz)

                            ' DOCUMENTO CONTABLE
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("Documento")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            'F350_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_PLEXA_SIESA)
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZACION_PLEXA_SIESA)
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", intConsecutivo.ToString) ' PREGUNTA Que consecutivo ?????
                            ' F350_FECHA (Alfanumerico(YYYYMMDD))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_FECHA", strFecha)
                            ' F350_ID_TERCERO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TERCERO", strIdenCond)
                            ' F350_ID_TERCERO (Alfanumerico(1)) 
                            ' 0 = EN ELABORACION
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_IND_ESTADO", "0")
                            ' F350_NOTAS (Alfanumerico(2000))
                            strValorCampo = "No LEGALIZACION GASTOS:" & lonNumeDocuLega.ToString
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_NOTAS", strValorCampo)

                            ' Obtener Detalle Legalizacion Gastos
                            Dim dtsDetaLegaGast As DataSet

                            strSQL = "gps_Obtenter_Detalle_Legalizacion_Gastos_SIESA " & intCodigoEmpresa.ToString() & ", " & lonNumeLega.ToString() & ", " & lonCodiCond.ToString()
                            dtsDetaLegaGast = objGeneral.Retorna_Dataset(strSQL)

                            If dtsDetaLegaGast.Tables.Count > 0 Then
                                If dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                                    ' Recorrer los Conceptos de Gasto de la Planilla Legalización
                                    For intCont2 = 0 To dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                                        strIdenProv = dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont2).Item("Identificacion_Tercero").ToString()
                                        intCodiConc = dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont2).Item("Codigo_Concepto").ToString()

                                        If intCodiConc = CONCEPTO_CUENTA_X_COBRAR_SALDO_FAVOR_EMPRESA Then

                                            ' Autor: Jesús Cuadros Barrero
                                            ' Fecha: 20-ENE-2021
                                            ' Observaciones: El concepto de CxC que se generó en la anterior Legalización se debe registrar como un Movimiento de CxP en esta nueva Legalización, utilizando como documento cruce el número de la CxC que generó GESTRANS

                                            dblValConCxC = CDbl(dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont2).Item("Valor_Credito").ToString())
                                            lonCodCxCSalFavEmp = CDbl(dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont2).Item("ENDC_Codigo").ToString()) ' Código CxC Saldo Favor Empresa

                                            ' MOVIMIENTO CxP
                                            xmlElementoRoot = xmlDocumento.DocumentElement
                                            xmlElemento = xmlDocumento.CreateElement("MovimientoCxP")
                                            xmlElementoRoot.AppendChild(xmlElemento)
                                            xmlElementoRoot = xmlDocumento.DocumentElement

                                            'F350_ID_CO (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_PLEXA_SIESA)
                                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZACION_PLEXA_SIESA)
                                            ' F350_CONSEC_DOCTO (Entero(8))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", intConsecutivo.ToString)
                                            ' F351_ID_AUXILIAR (Entero (10))
                                            strValorCampo = "14201101" ' PENDIENTE
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strValorCampo)
                                            ' F350_ID_TERCERO (Alfanumerico(15))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenCond)
                                            ' F351_ID_CO_MOV (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", 209) ' GLP=202, PGR=201, CARGA SECA=209 CENTRO_OPERACION_PLEXA_SIESA Preguntar (En el Anticipo esta Parametrizado)
                                            ' F351_ID_UN (Alfanumerico(20))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_PLEXA_SIESA)
                                            ' F351_VALOR_DB (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", "0")
                                            ' F351_VALOR_CR (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValConCxC)
                                            ' F351_VALOR_DB2 (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB2", "0")
                                            ' F351_VALOR_CR2 (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR2", dblValConCxC)
                                            ' F353_ID_SUCURSAL (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", SUCURSAL_PLEXA_SIESA)
                                            ' F353_PREFIJO_CRUCE (Alfanumerico(20))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", lonCodCxCSalFavEmp) ' Codigo de la CxC generada en la anterior legalizacion
                                            ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", "0")
                                            ' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFecha)
                                            ' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFecha)
                                            ' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strFecha)
                                            ' F354_NOTAS (Alfanumerico(255))
                                            strValorCampo = "CxP RELACIONADA con CxC GENERADA POR SALDO FAVOR EMPRESA"
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strValorCampo)

                                        Else
                                            ' CONCEPTOS DE GASTO LEGALIZACION

                                            ' MOVIMIENTO CONTABLE (Conceptos de Gasto) 
                                            xmlElementoRoot = xmlDocumento.DocumentElement
                                            xmlElemento = xmlDocumento.CreateElement("Movimiento")
                                            xmlElementoRoot.AppendChild(xmlElemento)
                                            xmlElementoRoot = xmlDocumento.DocumentElement

                                            ' F350_ID_CO (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_PLEXA_SIESA)
                                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZACION_PLEXA_SIESA)
                                            ' F350_CONSEC_DOCTO (Entero(8))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", intConsecutivo.ToString)
                                            ' F351_ID_AUXILIAR (Entero (10))
                                            strValorCampo = dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont2).Item("Codigo_Cuenta").ToString()
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strValorCampo)
                                            ' F351_ID_TERCERO (Alfanumerico(15))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenProv)
                                            ' F351_ID_CO_MOV (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", 209) ' GLP=202, PGR=201, CARGA SECA=209 CENTRO_OPERACION_PLEXA_SIESA Preguntar (En el Anticipo esta Parametrizado)
                                            ' F351_ID_UN (Alfanumerico(20))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_PLEXA_SIESA)
                                            ' F351_ID_CCOSTO (Alfanumerico(15))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont1).Item("Placa").ToString())
                                            ' F351_VALOR_DB (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", CDbl(dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont2).Item("Valor_Debito").ToString()))
                                            ' F351_VALOR_CR (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", CDbl(dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont2).Item("Valor_Credito").ToString()))
                                            'f351_base_gravable (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_BASE_GRAVABLE", "0")
                                            ' F351_VALOR_DB2 (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB2", CDbl(dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont2).Item("Valor_Debito").ToString()))
                                            ' F351_VALOR_CR2 (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR2", "0")
                                            ' F351_BASE_GRAVABLE2 (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_BASE_GRAVABLE2", "0")
                                            ' F351_VALOR_DB3 (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB3", CDbl(dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont2).Item("Valor_Debito").ToString()))
                                            ' F351_VALOR_CR3 (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR3", "0")
                                            ' F351_BASE_GRAVABLE3 (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_BASE_GRAVABLE3", "0")
                                            ' F351_NOTAS (Alfanumerico(255))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", dtsLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont1).Item("ENPD_Numero").ToString())

                                        End If

                                    Next

                                    ' Autor: Jesús Cuadros Barrero
                                    ' Fecha: 18-OCT-2020
                                    ' Observaciones: Crear una CxP por cada Comprobante Egreso Anticipo relacionado con la Legalizacion Gastos y el Conductor
                                    Dim dtsCompEgreAnti As DataSet, lonNumeCompEgreAnti As Long, dblValoCompEgreAnti As Double

                                    strSQL = "SELECT ENDC.Numero, ENDC.Fecha, ENDC.Valor_Pago_Recaudo_Total, TERC.Numero_Identificacion"
                                    strSQL += " FROM Encabezado_Documento_Comprobantes ENDC, Terceros As TERC"
                                    strSQL += " WHERE ENDC.EMPR_Codigo = TERC.EMPR_Codigo"
                                    strSQL += " AND ENDC.TERC_Codigo_Titular = TERC.Codigo"
                                    strSQL += " AND ENDC.EMPR_Codigo = " & intCodigoEmpresa.ToString & " And ENDC.TIDO_Codigo = " & TIDO_COMPROBANTE_EGRESO & " AND ENDC.ELGC_Numero = " & lonNumeLega.ToString() & " And ENDC.Estado = 1 And ENDC.Anulado = 0"
                                    strSQL += " AND ENDC.TERC_Codigo_Titular = " & lonCodiCond.ToString
                                    dtsCompEgreAnti = objGeneral.Retorna_Dataset(strSQL)

                                    If dtsCompEgreAnti.Tables.Count > 0 Then

                                        If dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                                            For intCont3 = 0 To dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                                                lonNumeCompEgreAnti = dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont3).Item("Numero").ToString()
                                                dblValoCompEgreAnti = dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont3).Item("Valor_Pago_Recaudo_Total").ToString()
                                                strIdenCond = dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont3).Item("Numero_Identificacion").ToString()

                                                ' MOVIMIENTO CxP
                                                xmlElementoRoot = xmlDocumento.DocumentElement
                                                xmlElemento = xmlDocumento.CreateElement("MovimientoCxP")
                                                xmlElementoRoot.AppendChild(xmlElemento)
                                                xmlElementoRoot = xmlDocumento.DocumentElement

                                                'F350_ID_CO (Alfanumerico(3))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_PLEXA_SIESA)
                                                ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZACION_PLEXA_SIESA)
                                                ' F350_CONSEC_DOCTO (Entero(8))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", intConsecutivo.ToString)
                                                ' F351_ID_AUXILIAR (Entero (10))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "14201101") ' PREGUNTAR: Por esta cuenta
                                                ' F350_ID_TERCERO (Alfanumerico(15))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenCond) ' Identificacion Conductor
                                                ' F351_ID_CO_MOV (Alfanumerico(3))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", 209) ' GLP=202, PGR=201, CARGA SECA=209 CENTRO_OPERACION_PLEXA_SIESA Preguntar (En el Anticipo esta Parametrizado)
                                                ' F351_ID_UN (Alfanumerico(20))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_PLEXA_SIESA)
                                                ' F351_VALOR_DB (Decimal)
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", "0")
                                                ' F351_VALOR_CR (Decimal)
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValoCompEgreAnti.ToString) ' Valor Comprobante Egreso Anticipo
                                                ' F351_VALOR_DB2 (Decimal)
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB2", "0")
                                                ' F351_VALOR_CR2 (Decimal)
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR2", dblValoCompEgreAnti.ToString) ' Valor Comprobante Egreso Anticipo
                                                ' F353_ID_SUCURSAL (Alfanumerico(3))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", SUCURSAL_PLEXA_SIESA)
                                                ' F353_PREFIJO_CRUCE (Alfanumerico(20))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", lonNumeCompEgreAnti.ToString) ' Número Comprobante Egreso Anticipo
                                                ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", "0")
                                                ' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFecha)
                                                ' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFecha)
                                                ' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strFecha)
                                                ' F354_NOTAS (Alfanumerico(255))
                                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", lonNumPlaDes)

                                            Next

                                        End If
                                    End If

                                    ' Autor: Jesús Cuadros Barrero
                                    ' Fecha: 30-DIC-2020
                                    ' Observaciones: Crear una CxC si Saldo_Favor_Empresa > 0
                                    If dblSaldFavoEmpr > 0 Then

                                        lonCodCxCSalFavEmp = Retorna_Cuenta_X_Cobrar_Saldo_Favor_Empresa(lonNumeDocuLega, lonCodiCond)

                                        ' MOVIMIENTO CxP, Nuevo Anticipo CxC
                                        xmlElementoRoot = xmlDocumento.DocumentElement
                                        xmlElemento = xmlDocumento.CreateElement("MovimientoCxP")
                                        xmlElementoRoot.AppendChild(xmlElemento)
                                        xmlElementoRoot = xmlDocumento.DocumentElement

                                        'F350_ID_CO (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_PLEXA_SIESA)
                                        ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZACION_PLEXA_SIESA)
                                        ' F350_CONSEC_DOCTO (Entero(8))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", intConsecutivo.ToString)
                                        ' F351_ID_AUXILIAR (Entero (10))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "14201101") ' PREGUNTAR POR ESTA CUENTA
                                        ' F350_ID_TERCERO (Alfanumerico(15))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenCond) ' Identificacion Conductor
                                        ' F351_ID_CO_MOV (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", 209) ' GLP=202, PGR=201, CARGA SECA=209 CENTRO_OPERACION_PLEXA_SIESA Preguntar (En el Anticipo esta Parametrizado)
                                        ' F351_ID_UN (Alfanumerico(20))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_PLEXA_SIESA)
                                        ' F351_VALOR_DB (Decimal)
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblSaldFavoEmpr.ToString) ' Valor Saldo_Favor_Empresa
                                        ' F351_VALOR_CR (Decimal)
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", "0")
                                        ' F351_VALOR_DB2 (Decimal)
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB2", dblSaldFavoEmpr.ToString) ' Valor Saldo_Favor_Empresa
                                        ' F351_VALOR_CR2 (Decimal)
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR2", "0")
                                        ' F353_ID_SUCURSAL (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", SUCURSAL_PLEXA_SIESA)
                                        ' F353_PREFIJO_CRUCE (Alfanumerico(20))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", lonCodCxCSalFavEmp.ToString) ' Código CxC Saldo Favor Emrpresa
                                        ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", "0")
                                        ' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFecha)
                                        ' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFecha)
                                        ' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strFecha)
                                        ' F354_NOTAS (Alfanumerico(255))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", "NUEVO ANTICIPO CxC")

                                    End If

                                    ' ENVIAR XML LEGALIZACION GASTOS
                                    strXML = xmlDocumento.InnerXml
                                    Dim TMPStrRutaArchivoXML As String = strRutaArchivoXML

                                    'Me.objGenericTransfer = New wsGenerarPlanoSoapClient
                                    'strPlanoXML = objGenericTransfer.ImportarDatosXML(92025, "LEGALIZACION_ANTICIPOS", 2, "1", "gt", "gt", strXML, TMPStrRutaArchivoXML)

                                    If strPlanoXML = "Importacion exitosa" Then

                                        bolProcesoValido = True

                                    Else
                                        strErrorDetalleSIESA = Retorna_Error_Detalle_SIESA(strPlanoXML)

                                        ' Actualizar el Comprobante Contable con Interfase_Contable = 0, mensaje de error retornado por Generic Transfer y NUMERO INTENTOS
                                        strSQL = "UPDATE Encabezado_Legalizacion_Gastos_Conductor SET Interfaz_Contable_Legalizacion = 0, Mensaje_Error = '" & strErrorDetalleSIESA & "', Fecha_Interfase_Contable = getDate(), Intentos_Interfase_Contable = ISNULL(Intentos_Interfase_Contable,0) + 1"
                                        strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                                        strSQL += " AND Numero = " & lonNumeLega

                                        ' Ejecutar UPDATE
                                        lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                                    End If

                                End If

                            End If

                            intConsecutivo = intConsecutivo + 1

                        Next

                        If bolProcesoValido Then

                            ' Actualizar Encabezado_Facturas con Interfaz_Contable_Factura = 1 y Fecha_Interfase_Contable = getDate()
                            strSQL = "UPDATE Encabezado_Legalizacion_Gastos_Conductor SET Interfaz_Contable_Legalizacion = 1, Fecha_Interfase_Contable = getDate()"
                            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                            strSQL += " AND Numero = " & lonNumeLega

                            ' Ejecutar UPDATE
                            lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                        End If

                    End If

                End If
            Next

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Crear_XML_Legalizacion_Gastos: " & ex.Message)
        End Try
    End Sub

    Private Sub Crear_XML_Factura_Ventas(ByVal dtsFactVent As DataSet)
        Try
            Dim strXML As String, strPlanoXML As String, strValorCampoDefec As String = "", strValorCampo As String = ""
            Dim strErrorDetalleSIESA As String, lonNumeComp As Long
            Dim strXMLEntrInve As String ' Variable con la respuesta de la Entrada Inventario

            Dim xmlDocumento As XmlDocument
            Dim xmlDeclaracion As XmlDeclaration
            Dim xmlElementoRaiz As XmlElement
            Dim xmlElementoRoot As XmlElement
            Dim xmlElemento As XmlElement

            For intCon = 0 To dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                ' Crea una adición del item XXXX en Inventario 
                strXMLEntrInve = Crear_XML_Entrada_Inventario(dtsFactVent, intCon)
                lonNumeComp = dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero")

                If strXMLEntrInve = "Importacion exitosa" Then

                    xmlDocumento = New XmlDocument

                    xmlElementoRaiz = xmlDocumento.CreateElement("Conector")
                    xmlDocumento.AppendChild(xmlElementoRaiz)

                    ' DOCUMENTO 
                    xmlElementoRoot = xmlDocumento.DocumentElement
                    xmlElemento = xmlDocumento.CreateElement("Documento")
                    xmlElementoRoot.AppendChild(xmlElemento)
                    xmlElementoRoot = xmlDocumento.DocumentElement

                    'F350_ID_CO (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", "201") ' PENDIENTE Centro Operacion dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Unidad_Negocio").ToString()
                    ' f470_id_tipo_docto (Alfanumerico (3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f350_id_tipo_docto", "FE1")
                    'F350_CONSEC_DOCTO (Entero(8))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString())
                    ' F350_FECHA (Alfanumerico(YYYYMMDD))
                    strValorCampo = Retorna_Fecha_Formato_SIESA(dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString())
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_FECHA", strValorCampo)
                    ' F350_ID_TERCERO (Alfanumerico(15))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TERCERO", dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Tercero").ToString())
                    ' f461_id_sucursal_fact (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f461_id_sucursal_fact", SUCURSAL_PLEXA_SIESA)
                    ' f461_id_co_fact (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f461_id_co_fact", ID_CODIGO_FACTURA)
                    ' f461_id_tercero_rem (Alfanumerico(15))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f461_id_tercero_rem", dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Tercero").ToString())
                    ' f461_id_sucursal_rem (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f461_id_sucursal_rem", SUCURSAL_PLEXA_SIESA)
                    ' f461_id_cond_pago (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f461_id_cond_pago", ID_CONDICION_PAGO)
                    ' f461_notas (Alfanumerico(2000))
                    'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f461_notas", dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Observaciones").ToString())
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f461_notas", "PENDIENTE DETALLE") ' PENDIENTE Definir Detalle dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString()

                    'Obtiene Detalle 
                    Dim dtsDetalleFactVent As DataSet

                    strSQL = "gps_Obtenter_Detalle_Facturas_SIESA " & intCodigoEmpresa.ToString & ", " & dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero").ToString()
                    dtsDetalleFactVent = objGeneral.Retorna_Dataset(strSQL)

                    If dtsDetalleFactVent.Tables.Count > 0 Then
                        If dtsDetalleFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                            For intdet = 0 To dtsDetalleFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1
                                ' MOVIMIENTO 
                                xmlElementoRoot = xmlDocumento.DocumentElement
                                xmlElemento = xmlDocumento.CreateElement("Movimiento")
                                xmlElementoRoot.AppendChild(xmlElemento)
                                xmlElementoRoot = xmlDocumento.DocumentElement

                                ' f470_id_co (Alfanumerico (3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_co", "201") ' PENDIENTE Definir Centro Operación
                                ' f470_id_tipo_docto (Alfanumerico (3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_tipo_docto", "FE1")
                                ' f470_consec_docto (Entero (8))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_consec_docto", dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString())
                                ' f470_nro_registro (Entero (10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_nro_registro", dtsDetalleFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intdet).Item("NumeroRegistro").ToString())
                                ' f470_id_bodega (Alfanumerico (5))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_bodega", "TPGR") ' PENDIENTE Bodega dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Bodega").ToString()
                                ' f470_id_motivo (Alfanumerico (2))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_motivo", "SP") ' PENDIENTE Motivo dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Bodega").ToString()
                                ' f470_id_co_movto (Alfanumerico (3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_co_movto", "201") ' PENDIENTE Definir Unidad Negocio dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Unidad_Negocio").ToString()
                                ' f470_id_ccosto_movto (Alfanumerico (15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_ccosto_movto", dtsDetalleFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intdet).Item("Placa").ToString())
                                ' f470_id_lista_precio (Alfanumerico (3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_lista_precio", ID_LISTA_PRECIOS_PLEXA_SIESA)
                                ' f470_id_unidad_medida (Alfanumerico (4))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_unidad_medida", "GLS") ' PENDIENTE Definir Unidad Medida

                                'Select Case dtsDetalleFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intdet).Item("UMPT_NombreCorto").ToString()
                                '    Case UNIDAD_MEDIDA_KILOGRAMO
                                '        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_unidad_medida", ID_UNIDAD_MEDIDA_KILOGRAMO_SIESA)
                                '    Case UNIDAD_MEDIDA_TONELADA
                                '        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_unidad_medida", ID_UNIDAD_MEDIDA_TONELADA_SIESA)
                                '    Case UNIDAD_MEDIDA_GALON
                                '        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_unidad_medida", ID_UNIDAD_MEDIDA_GALON_SIESA)
                                'End Select

                                ' f470_cant_base (Decimal (20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_cant_base", CLng(dtsDetalleFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intdet).Item("Cantidad").ToString()))
                                ' f470_vlr_bruto (Decimal (20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_vlr_bruto", CLng(dtsDetalleFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intdet).Item("TotalFlete").ToString()))
                                ' f470_id_item (Entero (7))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_item", "39") ' PENDIENTE: Definir ITEM AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_unidad_medida", ID_UNIDAD_MEDIDA_KILOGRAMO_SIESA)
                                ' f470_id_un_movto (Alfanumerico (20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_un_movto", UNIDAD_NEGOCIO_PLEXA_SIESA)
                            Next

                            ' Cuota CXC 
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("CuotaCxC")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F350_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_PLEXA_SIESA)
                            ' f470_id_tipo_docto (Alfanumerico (3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f350_id_tipo_docto", "FE1")
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString())
                            ' F353_FECHA_VCTO (Alfanumerico(8))
                            strValorCampo = Retorna_Fecha_Formato_SIESA(dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString())
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strValorCampo)
                            ' F353_FECHA_VCTO_DSCTO_PP (Alfanumerico(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO_DSCTO_PP", strValorCampo)

                            strXML = xmlDocumento.InnerXml
                            Dim TMPStrRutaArchivoXML As String = strRutaArchivoXML

                            'Me.objGenericTransfer = New wsGenerarPlanoSoapClient
                            'strPlanoXML = objGenericTransfer.ImportarDatosXML(94626, "FACTURA_VENTA", 2, "1", "gt", "gt", strXML, TMPStrRutaArchivoXML)

                            If strPlanoXML = "Importacion exitosa" Then

                                ' Actualizar Encabezado_Facturas con Interfaz_Contable_Factura = 1 y Fecha_Interfase_Contable = getDate()
                                strSQL = "UPDATE Encabezado_Facturas SET Interfaz_Contable_Factura = 1, Fecha_Interfase_Contable = getDate()"
                                strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                                strSQL += " AND Numero = " & lonNumeComp

                                ' Ejecutar UPDATE
                                lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                            Else
                                strErrorDetalleSIESA = Retorna_Error_Detalle_SIESA(strPlanoXML)

                                ' Actualizar el Comprobante Contable con Interfase_Contable = 0, mensaje de error retornado por Generic Transfer y NUMERO INTENTOS
                                strSQL = "UPDATE Encabezado_Facturas SET Interfaz_Contable_Factura = 0, Mensaje_Error = '" & strErrorDetalleSIESA & "', Fecha_Interfase_Contable = getDate(), Intentos_Interfase_Contable = Intentos_Interfase_Contable + 1"
                                strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                                strSQL += " AND Numero = " & lonNumeComp

                                ' Ejecutar UPDATE
                                lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                            End If

                        End If
                    End If
                Else
                    strErrorDetalleSIESA = Retorna_Error_Detalle_SIESA(strXMLEntrInve)

                    ' Actualizar el Comprobante Contable con Interfase_Contable = 0, mensaje de error retornado por Generic Transfer y NUMERO INTENTOS
                    strSQL = "UPDATE Encabezado_Facturas SET Interfaz_Contable_Factura = 0, Mensaje_Error = '" & strErrorDetalleSIESA & "', Fecha_Interfase_Contable = getDate(), Intentos_Interfase_Contable = Intentos_Interfase_Contable + 1"
                    strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                    strSQL += " AND Numero = " & lonNumeComp

                    ' Ejecutar UPDATE
                    lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)
                End If
            Next

        Catch ex As Exception

        End Try
    End Sub

    Private Sub Crear_XML_Factura_Provision(ByVal dtsFactProv As DataSet)
        Try
            Dim strXML As String, strPlanoXML As String, strValorCampoDefec As String = "", strValorCampo As String = ""
            Dim strErrorDetalleSIESA As String, lonNumeComp As Long
            Dim strXMLEntrInve As String ' Variable con la respuesta de la Entrada Inventario 

            Dim xmlDocumento As XmlDocument
            Dim xmlDeclaracion As XmlDeclaration
            Dim xmlElementoRaiz As XmlElement
            Dim xmlElementoRoot As XmlElement
            Dim xmlElemento As XmlElement

            For intCon = 0 To dtsFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                strXMLEntrInve = Crear_XML_Entrada_Inventario(dtsFactProv, intCon)
                lonNumeComp = dtsFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero")
                If strXMLEntrInve = "Importacion exitosa" Then

                    xmlDocumento = New XmlDocument

                    xmlElementoRaiz = xmlDocumento.CreateElement("Conector")
                    xmlDocumento.AppendChild(xmlElementoRaiz)

                    ' DOCUMENTO 
                    xmlElementoRoot = xmlDocumento.DocumentElement
                    xmlElemento = xmlDocumento.CreateElement("Documento")
                    xmlElementoRoot.AppendChild(xmlElemento)
                    xmlElementoRoot = xmlDocumento.DocumentElement

                    'F350_ID_CO (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", dtsFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Unidad_Negocio").ToString())
                    'F350_ID_TIPO_DOCTO
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", "PFC")
                    'F350_CONSEC_DOCTO (Entero(8))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", dtsFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString())
                    ' F350_FECHA (Alfanumerico(YYYYMMDD))
                    strValorCampo = Retorna_Fecha_Formato_SIESA(dtsFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString())
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_FECHA", strValorCampo)
                    ' F350_ID_TERCERO (Alfanumerico(15))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TERCERO", dtsFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Tercero").ToString())
                    ' f461_id_sucursal_fact (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f461_id_sucursal_fact", SUCURSAL_PLEXA_SIESA)
                    ' f461_id_co_fact (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f461_id_co_fact", ID_CODIGO_FACTURA)
                    ' f461_id_tercero_rem (Alfanumerico(15))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f461_id_tercero_rem", dtsFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Tercero").ToString())
                    ' f461_id_sucursal_rem (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f461_id_sucursal_rem", SUCURSAL_PLEXA_SIESA)
                    ' f461_id_cond_pago (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f461_id_cond_pago", ID_CONDICION_PAGO)
                    ' f461_notas (Alfanumerico(2000))
                    'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f461_notas", dtsFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Observaciones").ToString())
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f461_notas", dtsFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString())

                    'Obtiene Detalle 
                    Dim dtsDetalleFactProv As DataSet

                    strSQL = "gps_Obtenter_Detalle_Provision_SIESA " & intCodigoEmpresa.ToString & ", " & dtsFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero").ToString()
                    dtsDetalleFactProv = objGeneral.Retorna_Dataset(strSQL)

                    If dtsDetalleFactProv.Tables.Count > 0 Then
                        If dtsDetalleFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                            For intdet = 0 To dtsDetalleFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1
                                ' MOVIMIENTO 
                                xmlElementoRoot = xmlDocumento.DocumentElement
                                xmlElemento = xmlDocumento.CreateElement("Movimiento")
                                xmlElementoRoot.AppendChild(xmlElemento)
                                xmlElementoRoot = xmlDocumento.DocumentElement

                                ' f470_id_co (Alfanumerico (3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_co", dtsFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Unidad_Negocio").ToString())
                                ' f470_id_tipo_docto (Alfanumerico (3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_tipo_docto", "PFC")
                                ' f470_consec_docto (Entero (8))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_consec_docto", dtsFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString())
                                ' f470_nro_registro (Entero (10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_nro_registro", dtsDetalleFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows(intdet).Item("NumeroRegistro").ToString())
                                ' f470_id_bodega (Alfanumerico (5))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_bodega", dtsFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Bodega").ToString())
                                ' f470_id_motivo (Alfanumerico (2))
                                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_motivo", ID_MOTIVO_PLEXA_SIESA)
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_motivo", dtsFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Motivo").ToString())
                                ' f470_id_co_movto (Alfanumerico (3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_co_movto", dtsFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Unidad_Negocio").ToString())
                                ' f470_id_ccosto_movto (Alfanumerico (15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_ccosto_movto", dtsDetalleFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows(intdet).Item("Placa").ToString())
                                ' f470_id_lista_precio (Alfanumerico (3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_lista_precio", ID_LISTA_PRECIOS_PLEXA_SIESA)
                                ' f470_id_unidad_medida (Alfanumerico (4))
                                Select Case dtsDetalleFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows(intdet).Item("UMPT_NombreCorto").ToString()
                                    Case UNIDAD_MEDIDA_KILOGRAMO
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_unidad_medida", ID_UNIDAD_MEDIDA_KILOGRAMO_SIESA)
                                    Case UNIDAD_MEDIDA_TONELADA
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_unidad_medida", ID_UNIDAD_MEDIDA_TONELADA_SIESA)
                                    Case UNIDAD_MEDIDA_GALON
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_unidad_medida", ID_UNIDAD_MEDIDA_GALON_SIESA)
                                End Select
                                ' f470_cant_base (Decimal (20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_cant_base", CLng(dtsDetalleFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows(intdet).Item("Cantidad").ToString()))
                                ' f470_vlr_bruto (Decimal (20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_vlr_bruto", CLng(dtsDetalleFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows(intdet).Item("TotalFlete").ToString()))
                                ' f470_id_item (Entero (7))
                                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_item", dtsDetalleFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows(intdet).Item("Referencia_Producto").ToString())
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_item", dtsFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Item").ToString())
                                ' f470_id_un_movto (Alfanumerico (20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_un_movto", UNIDAD_NEGOCIO_PLEXA_SIESA)
                            Next

                            'Cuota CXC 
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("CuotaCxC")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F350_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_PLEXA_SIESA)
                            ' F350_ID_TIPO_DOCTO (Alfanumerico (3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", "PFC")
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", dtsFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString())
                            ' F353_FECHA_VCTO (Alfanumerico(8))
                            strValorCampo = Retorna_Fecha_Formato_SIESA(dtsFactProv.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString())
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strValorCampo)
                            ' F353_FECHA_VCTO_DSCTO_PP (Alfanumerico(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO_DSCTO_PP", strValorCampo)

                            strXML = xmlDocumento.InnerXml
                            Dim TMPStrRutaArchivoXML As String = strRutaArchivoXML

                            'Me.objGenericTransfer = New wsGenerarPlanoSoapClient
                            'strPlanoXML = objGenericTransfer.ImportarDatosXML(94626, "FACTURA_VENTA", 2, "1", "gt", "gt", strXML, TMPStrRutaArchivoXML)

                            If strPlanoXML = "Importacion exitosa" Then

                                ' Actualizar Encabezado_Facturas con Interfaz_Contable_Factura = 1 y Fecha_Interfase_Contable = getDate()
                                strSQL = "UPDATE Encabezado_Facturas SET Interfaz_Contable_Pre_Factura = 1, Fecha_Interfase_Contable_Pre_Factura = getDate()"
                                strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                                strSQL += " AND Numero = " & lonNumeComp

                                ' Ejecutar UPDATE
                                lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                            Else
                                strErrorDetalleSIESA = Retorna_Error_Detalle_SIESA(strPlanoXML)

                                ' Actualizar el Comprobante Contable con Interfase_Contable = 0, mensaje de error retornado por Generic Transfer y NUMERO INTENTOS
                                strSQL = "UPDATE Encabezado_Facturas SET Interfaz_Contable_Pre_Factura = 0, Mensaje_Error_Pre_Factura = '" & strErrorDetalleSIESA & "', Fecha_Interfase_Contable_Pre_Factura = getDate(), Intentos_Interfase_Contable_Pre_Factura = Intentos_Interfase_Contable_Pre_Factura + 1"
                                strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                                strSQL += " AND Numero = " & lonNumeComp

                                ' Ejecutar UPDATE
                                lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                            End If

                        End If
                    End If
                Else
                    strErrorDetalleSIESA = Retorna_Error_Detalle_SIESA(strXMLEntrInve)

                    ' Actualizar el Comprobante Contable con Interfase_Contable = 0, mensaje de error retornado por Generic Transfer y NUMERO INTENTOS
                    strSQL = "UPDATE Encabezado_Facturas SET Interfaz_Contable_Factura = 0, Mensaje_Error = '" & strErrorDetalleSIESA & "', Fecha_Interfase_Contable = getDate(), Intentos_Interfase_Contable = Intentos_Interfase_Contable + 1"
                    strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                    strSQL += " AND Numero = " & lonNumeComp

                    ' Ejecutar UPDATE
                    lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)
                End If
            Next

        Catch ex As Exception

        End Try
    End Sub

    Private Function Crear_XML_Entrada_Inventario(ByVal dtsFactVent As DataSet, ByVal intCon As Integer) As String
        Dim strXML As String, strPlanoXML As String, strValorCampoDefec As String = "", strValorCampo As String = ""
        Dim strErrorDetalleSIESA As String

        Dim xmlDocumento As XmlDocument
        Dim xmlDeclaracion As XmlDeclaration
        Dim xmlElementoRaiz As XmlElement
        Dim xmlElementoRoot As XmlElement
        Dim xmlElemento As XmlElement

        xmlDocumento = New XmlDocument
        xmlElementoRaiz = xmlDocumento.CreateElement("Conector")
        xmlDocumento.AppendChild(xmlElementoRaiz)

        ' DOCUMENTO 
        xmlElementoRoot = xmlDocumento.DocumentElement
        xmlElemento = xmlDocumento.CreateElement("Documento")
        xmlElementoRoot.AppendChild(xmlElemento)
        xmlElementoRoot = xmlDocumento.DocumentElement

        'F350_ID_CO (Alfanumerico(3))
        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", "201")  ' PENDIENTE: Definir Centro Operación Documento
        'F350_CONSEC_DOCTO (Entero(8))
        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString())
        ' F350_FECHA (Alfanumerico(YYYYMMDD))
        strValorCampo = Retorna_Fecha_Formato_SIESA(dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString())
        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_FECHA", strValorCampo)
        ' F350_ID_TERCERO (Alfanumerico(15))
        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TERCERO", dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Tercero").ToString())
        ' f350_notas (Alfanumerico(2000))
        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f350_notas", dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Observaciones").ToString())

        'Obtiene Detalle 
        Dim dtsDetalleInv As DataSet

        strSQL = "gps_Obtenter_Detalle_Inventario_SIESA " & intCodigoEmpresa.ToString & ", " & dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero").ToString()
        dtsDetalleInv = objGeneral.Retorna_Dataset(strSQL)

        If dtsDetalleInv.Tables.Count > 0 Then
            If dtsDetalleInv.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                For intdet = 0 To dtsDetalleInv.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1
                    xmlElementoRoot = xmlDocumento.DocumentElement
                    xmlElemento = xmlDocumento.CreateElement("Movimiento")
                    xmlElementoRoot.AppendChild(xmlElemento)
                    xmlElementoRoot = xmlDocumento.DocumentElement

                    ' f470_id_co (Alfanumerico (3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_co", "201") ' PENDIENTE: Definir Centro Operación Documento
                    ' f470_consec_docto (Entero (8))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_consec_docto", dtsFactVent.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString())
                    ' f470_nro_registro (Entero (10))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_nro_registro", "1")
                    ' f470_id_bodega (Alfanumerico (5))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_bodega", "TPGR") ' PENDIENTE: Definir manejo Bodega
                    ' f470_id_co_movto (Alfanumerico (3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_co_movto", "201") ' PENDIENTE: Definir Centro Operacion Movimiento
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_unidad_medida", "GLS") ' PENDIENTE: Homologar UM de Productos GESTRANS - SIESA

                    'Select Case dtsDetalleInv.Tables(clsGeneral.PRIMER_TABLA).Rows(intdet).Item("UMPT_NombreCorto").ToString()
                    '    Case UNIDAD_MEDIDA_KILOGRAMO
                    '        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_unidad_medida", ID_UNIDAD_MEDIDA_KILOGRAMO_SIESA)
                    '    Case UNIDAD_MEDIDA_TONELADA
                    '        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_unidad_medida", ID_UNIDAD_MEDIDA_TONELADA_SIESA)
                    '    Case UNIDAD_MEDIDA_GALON
                    '        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_unidad_medida", ID_UNIDAD_MEDIDA_GALON_SIESA)
                    'End Select
                    ' f470_cant_base (Decimal (20))

                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_cant_base", CLng(dtsDetalleInv.Tables(clsGeneral.PRIMER_TABLA).Rows(intdet).Item("Cantidad").ToString()))
                    ' f470_costo_prom_uni (Decimal (20))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_costo_prom_uni", "0")
                    ' f470_notas (Alfanumerico(2000))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_notas", "NOTAS PRUEBAS") ' PENDIENTE: Definir el contenido
                    ' f470_id_item (Entero (7))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_item", "39") ' PENDIENTE: Definir el código del producto 39 es PGR
                    ' f470_id_un_movto (Alfanumerico (20))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f470_id_un_movto", UNIDAD_NEGOCIO_PLEXA_SIESA)

                Next
            End If
        End If

        strXML = xmlDocumento.InnerXml
        Dim TMPStrRutaArchivoXML As String = strRutaArchivoXML

        'Me.objGenericTransfer = New wsGenerarPlanoSoapClient
        'strPlanoXML = objGenericTransfer.ImportarDatosXML(94625, "ENTRADA_INVENTARIO", 2, "1", "gt", "gt", strXML, TMPStrRutaArchivoXML)

        Return strPlanoXML

    End Function

    Private Sub Crear_XML_Liquidacion_Despachos(ByVal dtsLiquDesp As DataSet)
        Try
            Dim strXML As String, strPlanoXML As String, strValorCampoDefec As String = "", strValorCampo As String = ""
            Dim strErrorDetalleSIESA As String, lonNumeComp As Long

            Dim xmlDocumento As XmlDocument
            Dim xmlDeclaracion As XmlDeclaration
            Dim xmlElementoRaiz As XmlElement
            Dim xmlElementoRoot As XmlElement
            Dim xmlElemento As XmlElement

            For intCon = 0 To dtsLiquDesp.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                lonNumeComp = dtsLiquDesp.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero")

                xmlDocumento = New XmlDocument

                xmlElementoRaiz = xmlDocumento.CreateElement("Conector")
                xmlDocumento.AppendChild(xmlElementoRaiz)

                ' DOCUMENTO 
                xmlElementoRoot = xmlDocumento.DocumentElement
                xmlElemento = xmlDocumento.CreateElement("Documento")
                xmlElementoRoot.AppendChild(xmlElemento)
                xmlElementoRoot = xmlDocumento.DocumentElement

                'F350_ID_CO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_PLEXA_SIESA)
                'F350_CONSEC_DOCTO (Entero(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", dtsLiquDesp.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString())
                ' F350_FECHA (Alfanumerico(YYYYMMDD))
                strValorCampo = Retorna_Fecha_Formato_SIESA(dtsLiquDesp.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString())
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_FECHA", strValorCampo)
                ' F350_ID_TERCERO (Alfanumerico(15))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TERCERO", dtsLiquDesp.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Tercero").ToString())
                ' F350_NOTAS (Alfanumerico(255))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_NOTAS", dtsLiquDesp.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Observaciones").ToString())

                'Obtiene Detalle 
                Dim dtsDetalleLiquDesp As DataSet
                strSQL = "gps_Obtenter_Detalle_Liquidacion_Despachos_SIESA " & intCodigoEmpresa.ToString & ", " & dtsLiquDesp.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero").ToString()
                dtsDetalleLiquDesp = objGeneral.Retorna_Dataset(strSQL)

                If dtsDetalleLiquDesp.Tables.Count > 0 Then
                    If dtsDetalleLiquDesp.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                        For intdet = 0 To dtsDetalleLiquDesp.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1
                            ' MOVIMIENTO 
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("Movimiento")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            'F350_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_PLEXA_SIESA)
                            'F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", dtsLiquDesp.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString())
                            ' F351_ID_AUXILIAR (Alfanumerico(20))
                            'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "23201501") ' TEMPORAL POR PARAMETRIZACION
                            REM : SC AJUSTAR CUENTAS LIQUIDACION 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "27909018") ' TEMPORAL POR PARAMETRIZACION CREDITO
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "75170701") ' TEMPORAL POR PARAMETRIZACION DEBITO
                            ' F351_ID_TERCERO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", dtsLiquDesp.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Tercero").ToString())
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", CENTRO_OPERACION_PLEXA_SIESA)
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_PLEXA_SIESA)
                            ' F351_ID_CCOSTO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", dtsLiquDesp.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Placa").ToString())
                            ' F351_VALOR_DB (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", CLng(dtsDetalleLiquDesp.Tables(clsGeneral.PRIMER_TABLA).Rows(intdet).Item("Valor_Debito").ToString()))
                            ' F351_VALOR_CR (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", CLng(dtsDetalleLiquDesp.Tables(clsGeneral.PRIMER_TABLA).Rows(intdet).Item("Valor_Credito").ToString()))
                            ' F351_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", dtsDetalleLiquDesp.Tables(clsGeneral.PRIMER_TABLA).Rows(intdet).Item("Observaciones").ToString())
                        Next

                        strXML = xmlDocumento.InnerXml
                        Dim TMPStrRutaArchivoXML As String = strRutaArchivoXML

                        'Me.objGenericTransfer = New wsGenerarPlanoSoapClient
                        'strPlanoXML = objGenericTransfer.ImportarDatosXML(94624, "LIQUIDACION_TERCEROS", 2, "1", "gt", "gt", strXML, TMPStrRutaArchivoXML)

                        If strPlanoXML = "Importacion exitosa" Then

                            ' Actualizar el Comprobante Contable con Interfase_Contable = 1 y Fecha_Interfase_Contable = getDate()
                            strSQL = "UPDATE Encabezado_Comprobante_Contables SET Interfase_Contable = 1, Fecha_Interfase_Contable = getDate()"
                            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                            strSQL += " AND Numero = " & lonNumeComp

                            ' Ejecutar UPDATE
                            lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                        Else
                            strErrorDetalleSIESA = Retorna_Error_Detalle_SIESA(strPlanoXML)

                            ' Actualizar el Comprobante Contable con Interfase_Contable = 0, mensaje de error retornado por Generic Transfer y NUMERO INTENTOS
                            strSQL = "UPDATE Encabezado_Comprobante_Contables SET Interfase_Contable = 0, Mensaje_Error = '" & strErrorDetalleSIESA & "', Fecha_Interfase_Contable = getDate(), Intentos_Interfase_Contable = Intentos_Interfase_Contable + 1"
                            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                            strSQL += " AND Numero = " & lonNumeComp

                            ' Ejecutar UPDATE
                            lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                        End If
                    End If
                End If

            Next

        Catch ex As Exception

        End Try
    End Sub

    ' Autor: Jesus Cuadros 
    ' Fecha Creación: 10-DIC-2019
    ' Observaciones Creación: 
    ' Fecha Modificación: 
    ' Observaciones Modificación: 

    Private Sub Proceso_Terceros()
        Try
            Dim dtsTerceros As DataSet

            While (True)

                Me.objGeneral.Guardar_Mensaje_Log("Inició Proceso Terceros")

                strSQL = "SELECT TOP 5 TERC.Codigo, TERC.Numero_Identificacion, TERC.CATA_TIID_Codigo, TERC.CATA_TINA_Codigo, TERC.Razon_Social,"
                strSQL += " TERC.Nombre, TERC.Apellido1, TERC.Apellido2, TERC.CIUD_Codigo_Direccion, TERC.Direccion, TERC.Telefono,"
                strSQL += " TERC.Email, TERC.Celular, CIUD.Codigo_Alterno AS CIUD_Codigo_DANE,"
                strSQL += " TERC.Reportado_Aplicativo_Contable, TERC.Mensaje_Aplicativo_Contable"
                strSQL += " FROM V_Tenedores AS TERC, Ciudades AS CIUD"
                strSQL += " WHERE TERC.EMPR_Codigo = " & intCodigoEmpresa.ToString
                strSQL += " AND TERC.Codigo > 1000000"
                strSQL += " AND TERC.EMPR_Codigo = CIUD.EMPR_Codigo"
                strSQL += " AND TERC.CIUD_Codigo_Direccion = CIUD.Codigo"

                dtsTerceros = objGeneral.Retorna_Dataset(strSQL)

                If dtsTerceros.Tables.Count > 0 Then

                    Crear_XML_Terceros(dtsTerceros)

                End If

            End While
        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Proceso_Terceros: " & ex.Message)
        End Try
    End Sub

    Private Sub Crear_XML_Terceros(ByVal dtsTerceros As DataSet)
        Try
            Dim strXML As String, strPlanoXML As String, strRutaPlanoXML As String = "", strValorTablaSIESA As String, strValorCampoDefec As String = ""

            Dim xmlDocumento As XmlDocument
            Dim xmlDeclaracion As XmlDeclaration
            Dim xmlElementoRaiz As XmlElement
            Dim xmlElementoRoot As XmlElement
            Dim xmlElemento As XmlElement

            For intCon = 0 To dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                xmlDocumento = New XmlDocument
                xmlDeclaracion = xmlDocumento.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
                xmlDocumento.AppendChild(xmlDeclaracion)

                xmlElementoRaiz = xmlDocumento.CreateElement("myDataset")
                xmlDocumento.AppendChild(xmlElementoRaiz)

                xmlElementoRoot = xmlDocumento.DocumentElement
                xmlElemento = xmlDocumento.CreateElement("Terceros")
                xmlElementoRoot.AppendChild(xmlElemento)
                xmlElementoRoot = xmlDocumento.DocumentElement

                ' F200_ID_NIT (Lon=15)
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F200_ID_NIT", dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Codigo").ToString())
                ' F200_NIT_CC (Lon=25)
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F200_NIT_CC", dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Identificacion").ToString())
                ' F200_ID_TIPO_IDENT(C=Cedula E=Extranjeria N=Nit)
                strValorTablaSIESA = Retorna_Valor_Tabla_SIESA("TIID", dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("CATA_TIID_Codigo").ToString())
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F200_ID_TIPO_IDENT", strValorTablaSIESA)
                ' F200_IND_TIPO_TERCERO(1=Natural, 2=Juridica)
                strValorTablaSIESA = Retorna_Valor_Tabla_SIESA("TINA", dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("CATA_TINA_Codigo").ToString())
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F200_IND_TIPO_TERCERO", strValorTablaSIESA)
                ' F200_RAZON_SOCIAL
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F200_RAZON_SOCIAL", dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Razon_Social").ToString())
                ' F200_APELLIDO1
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F200_APELLIDO1", dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Apellido1").ToString())
                ' F200_APELLIDO2
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F200_APELLIDO2", dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Apellido2").ToString())
                ' F200_NOMBRES 
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F200_NOMBRES", dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Nombre").ToString())
                ' F200_NOMBRE_EST (Nombre Establecimiento)
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F200_NOMBRE_EST", "")
                ' F015_CONTACTO
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_CONTACTO", "")
                ' F015_DIRECCION1 (Lon=40)
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_DIRECCION1", Mid$(dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Direccion").ToString(), 1, 40))
                ' F015_ID_PAIS (169=Colombia)
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_ID_PAIS", PAIS_COLOMBIA_SIESA)
                ' F015_ID_DEPTO (11=Bogota, 05=Antioquia)
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_ID_DEPTO", Mid$(dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("CIUD_Codigo_DANE").ToString(), 1, 2))
                ' F015_ID_CIUDAD (001=Medellín)
                strValorTablaSIESA = Mid$(dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("CIUD_Codigo_DANE").ToString(), 3, 3)
                strValorTablaSIESA = String.Format("{0:000}", strValorTablaSIESA)
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_ID_CIUDAD", strValorTablaSIESA)
                ' F015_TELEFONO (Lon=20)
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_TELEFONO", Mid$(dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Telefono").ToString(), 1, 20))
                ' F015_EMAIL (Lon=255)
                If dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Email").ToString() = "" Then strValorCampoDefec = CORREO_POR_DEFECTO
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_EMAIL", strValorCampoDefec)
                ' F200_FECHA_NACIMIENTO (AAAAMMDD) ' Este campo es Obligatorio y se envia la fecha del proceso
                strValorTablaSIESA = Date.Now.Year.ToString + String.Format("{0:MM}", DateTime.Now) + String.Format("{0:dd}", DateTime.Now)
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F200_FECHA_NACIMIENTO", strValorTablaSIESA)
                ' F200_ID_CIIU (Actividad Económica 4921=Transporte Pasajeros)
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F200_ID_CIIU", ACTIVIDAD_ECONOMICA_PASAJEROS_SIESA)
                ' F015_CELULAR (Lon=50)
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F015_CELULAR", Mid$(dtsTerceros.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Celular").ToString(), 1, 50))

                strXML = xmlDocumento.InnerXml

                ' Invocar servicio GENERIC TRANSFER método ImportarDatosXML
                'Me.objGenericTransfer = New wsGenerarPlanoSoapClient
                'strPlanoXML = objGenericTransfer.ImportarDatosXML(82961, "COOTRANSTAME_TERCEROS_INT", 2, "1", "gt", "gt", strXML, strRutaPlanoXML)

                If strPlanoXML = "Importacion Exitosa" Then
                    ' Actualizar Terceros

                End If

            Next

        Catch ex As Exception

        End Try
    End Sub

#End Region


End Class
