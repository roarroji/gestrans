# General
The copyright and all intellectual property rights in this software ("the Software") are owned by WebSupergoo ("the Owner").

The term "you" refers to one legal entity. In the case of a purchased license the legal entity is the licensee named at the point of purchase. If the named licensee is not one legal entity then the license is void.

If you have purchased a Group License, the term "you" also refers to your subsidiaries. For the purposes of clarification, if you have not purchased a Group License then your subsidiaries are not covered by your license.

You may not load the Software into any computer or copy it without the license of the Owner. The Owner offers you a non-exclusive, non-transferable license on the terms of this Agreement. If you do not accept these terms you may within 30 days of purchase return the Software, its packaging and documentation unused and intact to your supplier together with proof of purchase for a full refund.

For the purposes of this agreement, a computer is regarded as a single instance of an Operating System installed on a physical machine. So multiple instances of Operating Systems (e.g. Virtual Machines) installed on a physical machine are regarded as multiple computers.

For the purposes of this agreement, your subsidiaries are corporations in which more than fifty percent of the voting power is owned directly or indirectly by you. In addition, partners within a Limited Liability Partnership (LLP) are regarded as subsidiaries of the LLP.


You are permitted to:

1) load the Software and use it only on a single computer under your control, unless a multiple user license has been purchased in which case the software may be used on any number of computers, up to a maximum total equal to the number of purchased licenses;

2) transfer the Software from one computer to another provided it is used on only one computer at any one time, unless a multiple user license has been purchased in which case the software may be used on any number of computers, up to a maximum total equal to the number of purchased licenses; 

3) provided that you have purchased an Enterprise License or Group License you may load the Software and use it on any computer which is owned by you;

4) provided that you have purchased a Redistribution License and provided that your application is not a software development system as detailed in clause (b) below, you may distribute the Software (without royalty) as part of a single distinct application, unless multiple Redistribution Licenses have been purchased in which case the software may be distributed as part of a number of distinct applications, up to a maximum total equal to the number of purchased licenses;


You are not permitted: 

a) to rent, lease, sub-license, loan, copy (except as expressly provided in this Agreement), modify, adapt, merge, translate, reverse engineer, decompile, disassemble, create derivative works based on the whole or any part of the Software or its associated documentation or otherwise attempt to discover the source code of the Software. 

b) to distribute any part of the Software as part of any software development system (compiler or interpreter), or to expose any functionality of the Software via an application programming interface (API), or to present any functionality of the Software in a way that allows it to be used by another computer program that is not covered by this agreement.

c) except as expressly provided in this Agreement, to use, reproduce or deal in the Software in any way. 

d) to use the Software in any way which might violate the conditions of use for PDF set down by Adobe and detailed in the Adobe PDF Specification or in a way which is not in accordance with the accompanying documentation or does not take heed of advice given in the accompanying documentation.

e) to reveal or make public any license key that may have been provided to you.

f) to use the software in any way which might result in the Owner being legally obligated, either directly or indirectly, to a third party.

# Acceptance
You shall be deemed to have accepted the terms of this Agreement by loading the Software into any computer. 

# Term 
This license is effective until you terminate it by destroying all copies of the Software and any associated license keys. It will also terminate if you fail to abide by this Agreement and in this case you agree to immediately destroy all copies of the Software and any associated license keys. The Termination Date is the date on which the Owner is notified of such termination in writing.

# Ownership 
The Owner shall at all times retain ownership of the Software and all subsequent copies thereof regardless of form. This Agreement applies to the grant of the license only and not to the contract of sale of the Software. You may not assign or transfer this Agreement or license, in whole or in part, without the Owners prior written consent. 

This product may incorporate intellectual property owned by Microsoft Corporation. The terms and conditions upon which Microsoft is licensing such intellectual property may be found at http://go.microsoft.com/fwlink/?LinkId=52369.

You agree that your usage of the Software is a matter of public knowledge and that the Owner may use this fact, in combination with your trademarks and your logos, for marketing purposes. The Owner agrees to waive or desist from such use if requested in writing.

# Warranties 
1) The Owner warrants that the Software will perform substantially in accordance with its accompanying documentation (provided that the Software is properly used on the computer and with the operating system for which it was designed) and that the documentation correctly describes the operation of the Software in all material respects. If the Owner is notified of significant errors within 30 days of purchase (the "Warranty Period") it will take reasonable steps to correct such errors. If you do not feel that these steps are sufficient you may, at any point within the Warranty Period, terminate the Agreement in the manner detailed in the Term clause of this Agreement. Provided the Termination Date is within the Warranty Period the Owner will authorize a refund of the price of the Software. Such refund claims may not be made after the Warranty Period whether or not any error may have been previously reported. 

3) THE ABOVE REPRESENT YOUR SOLE REMEDIES FOR ANY BREACH OF THE OWNER'S WARRANTIES, WHICH ARE GIVEN ONLY TO THE ORIGINAL REGISTERED USER. 

4) THE EXPRESS TERMS OF THIS AGREEMENT ARE IN LIEU OF ALL WARRANTIES, CONDITIONS, UNDERTAKINGS, TERMS AND OBLIGATIONS IMPLIED BY STATUTE, COMMON LAW, TRADE USAGE, COURSE OF DEALING OR OTHERWISE ALL OF WHICH ARE HEREBY EXCLUDED TO THE FULLEST EXTENT PERMITTED BY LAW. 

5) THE OWNER DOES NOT WARRANT THAT THE SOFTWARE WILL MEET YOUR REQUIREMENTS OR THAT THE OPERATION OF THE SOFTWARE WILL BE UNINTERRUPTED OR ERROR-FREE OR THAT DEFECTS IN THE SOFTWARE WILL BE CORRECTED. YOU SHALL LOAD AND USE THE SOFTWARE AT YOUR OWN RISK AND IN NO EVENT WILL THE OWNER BE LIABLE TO YOU FOR ANY LOSS OR DAMAGE OF ANY KIND (EXCEPT PERSONAL INJURY OR DEATH RESULTING FROM THE OWNER'S NEGLIGENCE) INCLUDING LOST PROFITS OR OTHER CONSEQUENTIAL LOSS ARISING FROM YOUR USE OF OR INABILITY TO USE THE SOFTWARE OR FROM ERRORS OR DEFICIENCIES IN IT WHETHER CAUSED BY NEGLIGENCE OR OTHERWISE EXCEPT AS EXPRESSLY PROVIDED HEREIN.

6) ALL OTHER WARRANTIES ARE DISCLAIMED, TO THE EXTENT PERMITTED BY APPLICABLE LAW, BY THE OWNER AND ALL OTHER PARTIES.

Some states or jurisdictions do not allow the exclusion or limitation of incidental, consequential or special damages, so the above limitation or exclusion may not apply to you. Also some states or jurisdictions do not allow the exclusion of implied warranties or limitations on how long an implied warranty may last, so the above limitations may not apply to you. To the extent permissible, any implied warranties are limited to ninety (90) days. This warranty gives you specific legal rights. You may have other rights which vary from state to state or jurisdiction to jurisdiction. For further warranty information, please contact the Owner. 


# Law 
This Agreement shall be governed by the law of the State of California.

In relation to any legal action or proceedings arising out of or in connection with this Agreement ("Proceedings"), each of the parties irrevocably submits to the exclusive jurisdiction of the English courts and waives any objection to Proceedings in such courts on the grounds of venue or on the grounds that Proceedings have been brought in an inappropriate forum.

WebSupergoo and WebSupergoo Software are trading names of Zandent Ltd. Registered Address: The Business Design Centre, 52 Upper Street, Islington, London, N1 0QH, UK. Company number 07742414, incorporated in England and Wales.

This Agreement will not be governed by the United Nations Convention on Contracts for the International Sale of Goods, the Uniform Law on the Formation of Agreements for the International Sale of Goods, or the Uniform Law on the International Sale of Goods, or any law, rule or regulation or any jurisdiction based on any of the foregoing, and the application of all the foregoing is expressly excluded. 

If any part of this Agreement is found void and unenforceable, it will not affect the validity of the balance of the Agreement, which shall remain valid and enforceable according to its terms. 

You agree that the Software will not be shipped, transferred or exported into any country or used in any manner prohibited by the United States Export Administration Act or any other export laws, restrictions or regulations. This Agreement may only be modified in writing signed by authorized officers of the Owner.
