﻿SofttoolsApp.controller("GestionarLiquidacionCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'LiquidacionesFactory', 'PlanillaDespachosFactory', 'ValorCatalogosFactory', 'TercerosFactory', 'OficinasFactory', 'DetalleNovedadesDespachosFactory', 'CierreContableDocumentosFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, LiquidacionesFactory, PlanillaDespachosFactory, ValorCatalogosFactory, TercerosFactory, OficinasFactory, DetalleNovedadesDespachosFactory, CierreContableDocumentosFactory) {
        console.clear()
        $scope.Titulo = 'GESTIONAR LIQUIDACIÓN DESPACHOS';
        $scope.MapaSitio = $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Documentos' }, { Nombre: 'Liquidación' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_LIQUIDACIONES);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.ListadoFacturacion = [];
        var TerceroLiquidar = 0;
        var PlanillaValidada = false;
        var TotalBaseImpuestos;
        var TotalValorImpuestos;
        var TotalConceptosSuma = 0;
        var TotalConceptosResta = 0;
        $scope.ListadoTipoDocumentos = [];

        $scope.ModeloNumeroPlanilla = '';
        $scope.ModeloValorFlete = '';

        var OficinaUsuario = {
            Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
            Nombre: $scope.Sesion.UsuarioAutenticado.Oficinas.Nombre
        };
        $scope.TotalFleteCliente = 0;

        $scope.ModeloFecha = new Date();
        $scope.DeshabilitarControlPlanilla = false;
        $scope.DeshabilitarGuardar = false;
        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/

        if ($routeParams.Numero > 0) {
            $scope.ModeloNumero = $routeParams.Numero;
            Obtener();
        } else {
            $scope.ModeloNumero = 0;
            $scope.ModeloOficina = OficinaUsuario.Nombre;
            Find();
        }

        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'BORRADOR', Codigo: CERO },
            { Nombre: 'DEFINITIVO', Codigo: 1 }
        ];
        $scope.ModeloEstado = $scope.ListadoEstados[2];

        function Find() {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumentos: { Codigo: 160 }
            };
            CierreContableDocumentosFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoDocumentos = response.data.Datos;
                            $scope.ValidarCierre();
                        }
                    }
                }, function (response) {
                });
        }

        $scope.ValidarCierre = function () {
            if ($scope.ListadoTipoDocumentos.length > 0) {
                var anoFecha = $scope.ModeloFecha.getFullYear();
                var mesFecha = $scope.ModeloFecha.getMonth();
                for (var k = 0; k < $scope.ListadoTipoDocumentos.length; k++) {
                    var item = $scope.ListadoTipoDocumentos[k];
                    var mes = MascaraNumero(item.Mes.CampoAuxiliar2);
                    if (item.Ano === anoFecha && mes === (mesFecha + 1) && item.EstadoCierre.Codigo === 18002) {
                        ShowError('La fecha se encuentra en un mes y año con cierre contable');
                        $scope.ModeloFecha = '';
                        break;
                    }
                }
            }
        };

        function Obtener() {
            blockUI.start('Cargando Liquidación Despachos...');

            $timeout(function () {
                blockUI.message('Cargando Liquidación Despachos...');
            }, 200);

            TotalBaseImpuestos = 0;
            TotalValorImpuestos = 0;

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.ModeloNumero,
                Obtener: 1
            };
            blockUI.delay = 1000;
            LiquidacionesFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ModeloNumero = response.data.Datos.Numero;
                        $scope.ModeloNumeroDocumento = response.data.Datos.NumeroDocumento;
                        $scope.ModeloFecha = new Date(response.data.Datos.Fecha);
                        $scope.ModeloNumeroPlanilla = response.data.Datos.NumeroPlanilla;
                        $scope.ModeloNumeroManifiesto = response.data.Datos.NumeroManifiesto;
                        $scope.ModeloNumeroCumplido = response.data.Datos.NumeroCumplido;
                        $scope.ModeloPlacaVehiculo = response.data.Datos.PlacaVehiculo;
                        $scope.ModeloSemirremolque = response.data.Datos.PlacaSemirremolque;
                        TerceroLiquidar = response.data.Datos.Tenedor.Codigo;
                        $scope.ModeloTenedor = response.data.Datos.Tenedor.NombreCompleto;
                        $scope.ModeloConductor = response.data.Datos.Conductor.NombreCompleto;
                        $scope.ModeloObservaciones = response.data.Datos.Observaciones;
                        $scope.ModeloValorFlete = $scope.MaskValoresGrid(response.data.Datos.ValorFleteTranportador);
                        $scope.ModeloValorConceptos = $scope.MaskValoresGrid(response.data.Datos.ValorConceptosLiquidacion);
                        $scope.ModeloValorBase = $scope.MaskValoresGrid(response.data.Datos.ValorBaseImpuestos);
                        $scope.ModeloValorImpuestos = $scope.MaskValoresGrid(response.data.Datos.ValorImpuestos);
                        $scope.ModeloValorPagar = $scope.MaskValoresGrid(response.data.Datos.ValorPagar);
                        $scope.ModeloListadoConceptos = response.data.Datos.ListaDetalleLiquidacion;

                        //for (i = 0; i < $scope.ModeloListadoConceptos.length; i++) {
                        //    if ($scope.ModeloListadoConceptos[i].ConceptoSistema == 1) {
                        //        $scope.DeshabilitarCampo = true;
                        //    } else {
                        //        $scope.DeshabilitarCampo = true;
                        //    }
                        //}

                        for (i = 0; i < $scope.ModeloListadoConceptos.length; i++) {
                            $scope.ModeloListadoConceptos[i].Valor = $scope.MaskValoresGrid($scope.ModeloListadoConceptos[i].Valor);
                        }

                        $scope.ModeloListadoImpuestos = response.data.Datos.ListaImpuestosConceptosLiquidacion;

                        //for (i = 0; i < $scope.ModeloListadoImpuestos.length; i++) {
                        //    $scope.ModeloListadoImpuestos[i].ValorImpuesto = parseFloat($scope.ModeloListadoImpuestos[i].Valor_base) * parseFloat($scope.ModeloListadoImpuestos[i].Valor_tarifa);
                        //    $scope.ModeloListadoImpuestos[i].Valor_base = $scope.ModeloListadoImpuestos[i].Valor_base;
                        //    $scope.ModeloListadoImpuestos[i].ValorImpuesto = $scope.ModeloListadoImpuestos[i].ValorImpuesto;
                        //}
                        $scope.ModeloTipoDespacho = response.data.Datos.TipoDespacho;
                        $scope.ModeloCodDespacho = response.data.Datos.NumeroTarifario;
                        $scope.ModeloUsuarioCumplido = response.data.Datos.UsuarioCumplido;
                        $scope.ModeloFechaCumplido = response.data.Datos.FechaCumplido;
                        $scope.ModeloCiudadOrigen = response.data.Datos.CiudadOrigen;
                        $scope.ModeloCiudadDestino = response.data.Datos.CiudadDestino;
                        $scope.ModeloRuta = response.data.Datos.NombreRuta;
                        $scope.ModeloPesoCargue = MascaraValores(response.data.Datos.PesoCargue)
                        $scope.ModeloCantidad = response.data.Datos.Cantidad
                        $scope.ModeloPesoCumplido = response.data.Datos.PesoCumplido;


                        $scope.ModeloOficina = response.data.Datos.Oficina.Nombre;
                        $scope.ModeloEstado = response.data.Datos.Estado;

                        if ($scope.ListadoEstados !== undefined && $scope.ListadoEstados !== null) {
                            $scope.ModeloEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + $scope.ModeloEstado);
                        }
                        $scope.DeshabilitarControlPlanilla = true;
                        if ($scope.ModeloEstado.Codigo === ESTADO_DEFINITIVO) {
                            $scope.DeshabilitarCampo = true;
                            $scope.DeshabilitarGuardar = true;
                        }

                        PlanillaValidada = true;
                        var filtros = {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO },
                            NumeroDocumento: response.data.Datos.NumeroPlanilla,
                            Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                            CodigoUsuario: $scope.Sesion.UsuarioAutenticado.Codigo,
                            HaciaTemporal: 1,
                            Obtener: 1
                        }
                        $scope.ListadoGuiaGuardadas = []
                        LiquidacionesFactory.ObtenerPlanilla(filtros).
                            then(function (response) {
                                if (response.data.ProcesoExitoso === true && response.data.Datos.Numero > 0) {
                                    $scope.ListadoGuiaGuardadas = response.data.Datos.Detalles
                                    if ($scope.ListadoGuiaGuardadas.length > 0) {
                                        $scope.CondicionesPeso = [];
                                        $scope.ListadoFacturacion = [];
                                        for (var i = 0; i < $scope.ListadoGuiaGuardadas.length; i++) {
                                            filtros = {
                                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                Codigo: $scope.ListadoGuiaGuardadas[i].Remesa.Cliente.Codigo,
                                                Sync: true,
                                                SoloCondicionesPesoCumplido: 1
                                            };
                                            //--TotalFletecliente
                                            $scope.TotalFleteCliente += $scope.ListadoGuiaGuardadas[i].Remesa.TotalFleteCliente;
                                            //--TotalFletecliente
                                            var Condicion = TercerosFactory.Obtener(filtros)
                                            if (Condicion.ProcesoExitoso === true) {
                                                for (var j = 0; j < Condicion.Datos.CondicionesPeso.length; j++) {
                                                    if (Condicion.Datos.CondicionesPeso[j].Producto.Codigo == $scope.ListadoGuiaGuardadas[i].Remesa.ProductoTransportado.Codigo) {
                                                        if ($scope.CondicionesPeso.length > 0) {
                                                            var cont = 0
                                                            for (var k = 0; k < $scope.CondicionesPeso.length; k++) {
                                                                if ($scope.CondicionesPeso[k].Producto.Codigo == $scope.ListadoGuiaGuardadas[i].Remesa.ProductoTransportado.Codigo) {
                                                                    cont++
                                                                }
                                                            }
                                                            if (cont == 0) {
                                                                $scope.CondicionesPeso.push(Condicion.Datos.CondicionesPeso[j])
                                                            }
                                                        } else {
                                                            $scope.CondicionesPeso.push(Condicion.Datos.CondicionesPeso[j])
                                                        }
                                                    }
                                                }
                                            }

                                            if ($scope.ListadoGuiaGuardadas[i].Remesa.Factura.Estado == 1) {
                                                $scope.ListadoGuiaGuardadas[i].Remesa.Factura.NumeroRemesa = $scope.ListadoGuiaGuardadas[i].Remesa.NumeroDocumento;
                                                $scope.ListadoGuiaGuardadas[i].Remesa.Factura.CodigoRemesa = $scope.ListadoGuiaGuardadas[i].Remesa.Numero;
                                                $scope.ListadoGuiaGuardadas[i].Remesa.Factura.PesoFacturado = $scope.ListadoGuiaGuardadas[i].Remesa.PesoCliente;
                                                $scope.ListadoGuiaGuardadas[i].Remesa.Factura.FleteFacturado = $scope.ListadoGuiaGuardadas[i].Remesa.ValorFleteCliente;
                                                $scope.ListadoFacturacion.push($scope.ListadoGuiaGuardadas[i].Remesa.Factura);
                                            } else {
                                                $scope.ListadoGuiaGuardadas[i].Remesa.Factura.NumeroRemesa = $scope.ListadoGuiaGuardadas[i].Remesa.NumeroDocumento;
                                                $scope.ListadoGuiaGuardadas[i].Remesa.Factura.CodigoRemesa = $scope.ListadoGuiaGuardadas[i].Remesa.Numero;
                                                $scope.ListadoGuiaGuardadas[i].Remesa.Factura.PesoFacturado = '';
                                                $scope.ListadoGuiaGuardadas[i].Remesa.Factura.FleteFacturado = '';
                                                $scope.ListadoGuiaGuardadas[i].Remesa.Factura.NumeroDocumento = '';
                                                $scope.ListadoFacturacion.push($scope.ListadoGuiaGuardadas[i].Remesa.Factura);
                                            }

                                        }
                                        $scope.TotalFleteCliente = $scope.MaskValoresGrid($scope.TotalFleteCliente);
                                    }

                                }
                            }, function (response) {
                                ShowError(response.statusText);
                                document.location.href = '#!ConsultarLiquidaciones';
                            });
                    }
                    else {
                        ShowError('No se logro consultar la liquidación número ' + $scope.ModeloNumeroDocumento + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarLiquidaciones';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarLiquidaciones';
                });

            blockUI.stop();
        };

        $scope.ValidarPlanilla = function (Planilla, opt) {
            if (Planilla !== undefined && Planilla !== '' && Planilla !== null && Planilla > 0) {
                TotalBaseImpuestos = 0;
                TotalValorImpuestos = 0;
                PlanillaValidada = false;
                $scope.TotalFleteCliente = 0;
                if (opt > 0) {//Buscar por manifiesto
                    var filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO },
                        NumeroManifiesto: Planilla,
                        Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                        CodigoUsuario: $scope.Sesion.UsuarioAutenticado.Codigo,
                        HaciaTemporal: 1,
                        Obtener: 1
                    }
                } else {
                    var filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO },
                        NumeroDocumento: Planilla,
                        Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                        CodigoUsuario: $scope.Sesion.UsuarioAutenticado.Codigo,
                        HaciaTemporal: 1,
                        Obtener: 1
                    }
                }
                $scope.ListadoGuiaGuardadas = []
                LiquidacionesFactory.ObtenerPlanilla(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true && response.data.Datos.Numero > 0) {
                            if (response.data.Datos.Anulado == 1 && response.data.Datos.Estado.Codigo == ESTADO_BORRADOR) {
                                ShowError('La planilla número ' + Planilla + ' se encuentra anulada o en estado borrador');
                                PlanillaValidada = false;
                            } else if (response.data.Datos.Cumplido.Numero === 0) {
                                ShowError('La planilla número ' + Planilla + ' aún no se encuentra cumplida');
                                PlanillaValidada = false;
                            } else if (response.data.Datos.Liquidacion.Numero !== 0) {
                                ShowError('La planilla ya se encuentra liquidada');
                                PlanillaValidada = false;
                            } else {
                                $scope.NumeroInternoPlanilla = response.data.Datos.Numero;
                                $scope.ModeloNumeroManifiesto = response.data.Datos.Manifiesto.NumeroDocumento;
                                $scope.ModeloNumeroPlanilla = response.data.Datos.NumeroDocumento;
                                $scope.ModeloNumeroCumplido = response.data.Datos.Cumplido.NumeroDocumento;
                                $scope.ModeloPlacaVehiculo = response.data.Datos.Vehiculo.Placa;
                                $scope.ModeloSemirremolque = response.data.Datos.Semirremolque.Placa;
                                $scope.ModeloTenedor = response.data.Datos.NombreTenedor;
                                TerceroLiquidar = response.data.Datos.Tenedor.Codigo;
                                $scope.ModeloConductor = response.data.Datos.NombreConductor;
                                $scope.ModeloListadoConceptos = response.data.Datos.ListaConceptosLiquidacion;
                                $scope.ModeloListadoImpuestostemp = JSON.parse(JSON.stringify(response.data.Datos.ListaImpuestosConceptosLiquidacion))
                                $scope.ListadoGuiaGuardadas = response.data.Datos.Detalles;
                                $scope.ListadoFacturacion = [];
                                for (var i = 0; i < $scope.ListadoGuiaGuardadas.length; i++) {
                                    if ($scope.ListadoGuiaGuardadas[i].Remesa.Factura.Estado == 1) {
                                        $scope.ListadoGuiaGuardadas[i].Remesa.Factura.NumeroRemesa = $scope.ListadoGuiaGuardadas[i].Remesa.NumeroDocumento;
                                        $scope.ListadoGuiaGuardadas[i].Remesa.Factura.CodigoRemesa = $scope.ListadoGuiaGuardadas[i].Remesa.Numero;
                                        $scope.ListadoGuiaGuardadas[i].Remesa.Factura.PesoFacturado = $scope.ListadoGuiaGuardadas[i].Remesa.PesoCliente;
                                        $scope.ListadoGuiaGuardadas[i].Remesa.Factura.FleteFacturado = $scope.ListadoGuiaGuardadas[i].Remesa.ValorFleteCliente;
                                        $scope.ListadoFacturacion.push($scope.ListadoGuiaGuardadas[i].Remesa.Factura);
                                    } else {
                                        $scope.ListadoGuiaGuardadas[i].Remesa.Factura.NumeroRemesa = $scope.ListadoGuiaGuardadas[i].Remesa.NumeroDocumento;
                                        $scope.ListadoGuiaGuardadas[i].Remesa.Factura.CodigoRemesa = $scope.ListadoGuiaGuardadas[i].Remesa.Numero;
                                        $scope.ListadoGuiaGuardadas[i].Remesa.Factura.PesoFacturado = '';
                                        $scope.ListadoGuiaGuardadas[i].Remesa.Factura.FleteFacturado = '';
                                        $scope.ListadoGuiaGuardadas[i].Remesa.Factura.NumeroDocumento = '';
                                        $scope.ListadoFacturacion.push($scope.ListadoGuiaGuardadas[i].Remesa.Factura);
                                    }

                                }


                                $scope.ModeloTipoDespacho = response.data.Datos.TipoDespacho;
                                $scope.ModeloCodDespacho = response.data.Datos.NumeroTarifario;
                                $scope.ModeloUsuarioCumplido = response.data.Datos.UsuarioCumplido;
                                $scope.ModeloFechaCumplido = response.data.Datos.FechaCumplido;
                                $scope.ModeloCiudadOrigen = response.data.Datos.CiudadOrigen;
                                $scope.ModeloCiudadDestino = response.data.Datos.CiudadDestino;
                                $scope.ModeloRuta = response.data.Datos.Ruta.Nombre;
                                $scope.ModeloPesoCargue = MascaraValores(response.data.Datos.PesoCargue);
                                $scope.ModeloCantidad = response.data.Datos.Cantidad
                                $scope.ModeloPesoCumplido = MascaraValores(response.data.Datos.PesoCumplido);
                                $scope.PesoFaltante = MascaraValores(response.data.Datos.PesoFaltante);

                                for (i = 0; i < $scope.ModeloListadoConceptos.length; i++) {
                                    if ($scope.ModeloListadoConceptos[i].Operacion == 2) { //Resta
                                        TotalConceptosResta += $scope.ModeloListadoConceptos[i].Valor;
                                    } else if ($scope.ModeloListadoConceptos[i].Operacion == 1) { //Suma
                                        TotalConceptosSuma += $scope.ModeloListadoConceptos[i].Valor;
                                    }

                                    if ($scope.ModeloListadoConceptos[i].AplicaValorBase == 1) {
                                        //if ($scope.ModeloListadoConceptos[i].AplicaValorBase == 1) {//PAIS
                                        //}
                                        //if ($scope.ModeloListadoConceptos[i].AplicaValorBase == 1) {//DEPARTAMENTO
                                        //}
                                        //if ($scope.ModeloListadoConceptos[i].AplicaValorBase == 1) {//CIUDAD
                                        //}
                                        TotalBaseImpuestos += $scope.ModeloListadoConceptos[i].Valor;
                                    }

                                    //if ($scope.ModeloListadoConceptos[i].ConceptoSistema == 1) {
                                    //    $scope.DeshabilitarFila = true;
                                    //} else {
                                    //    $scope.DeshabilitarFila = true;
                                    //}
                                    $scope.ModeloListadoConceptos[i].Valor = $scope.MaskValoresGrid($scope.ModeloListadoConceptos[i].Valor);
                                }

                                for (i = 0; i < $scope.ModeloListadoImpuestostemp.length; i++) {
                                    $scope.ModeloListadoImpuestostemp[i].ValorImpuesto = parseFloat($scope.ModeloListadoImpuestostemp[i].Valor_base * $scope.ModeloListadoImpuestostemp[i].Valor_tarifa);
                                    TotalValorImpuestos += $scope.ModeloListadoImpuestostemp[i].ValorImpuesto;
                                    $scope.ModeloListadoImpuestostemp[i].Valor_base = $scope.MaskValoresGrid($scope.ModeloListadoImpuestostemp[i].Valor_base);
                                    $scope.ModeloListadoImpuestostemp[i].ValorImpuesto = $scope.MaskValoresGrid($scope.ModeloListadoImpuestostemp[i].ValorImpuesto);
                                }

                                if ($scope.Sesion.UsuarioAutenticado.ManejoRecalculoFleteLiquidacionPesoCumplido && response.data.Datos.PesoCumplido > 0) {
                                    //.data.Datos.Peso
                                    $scope.ModeloValorFlete = Math.round(response.data.Datos.FleteUnitario)
                                } else {
                                    $scope.ModeloValorFlete = response.data.Datos.ValorFleteTransportador;
                                }
                                $scope.ModeloValorConceptos = TotalConceptosResta - TotalConceptosSuma;
                                // Modificacion SC 07/01/2020
                                //$scope.ModeloValorBase = response.data.Datos.ValorFleteTransportador + TotalBaseImpuestos;
                                $scope.ModeloValorImpuestos = TotalValorImpuestos;
                                if ($scope.ModeloCodDespacho == 301 && $scope.Sesion.UsuarioAutenticado.ManejoRecalculoFleteLiquidacionPesoCumplido) {
                                    $scope.ModeloValorBase = Math.round((($scope.ModeloValorFlete * response.data.Datos.PesoCumplido) / 1000));
                                    $scope.ModeloValorPagar = Math.round((($scope.ModeloValorFlete * response.data.Datos.PesoCumplido) / 1000) - $scope.ModeloValorConceptos - $scope.ModeloValorImpuestos);
                                } else {
                                    $scope.ModeloValorBase = $scope.ModeloValorFlete;
                                    $scope.ModeloValorPagar = $scope.ModeloValorFlete - $scope.ModeloValorConceptos - $scope.ModeloValorImpuestos;
                                }
                                PlanillaValidada = true;

                                //Formatear campos
                                $scope.ModeloValorFlete = $scope.MaskValoresGrid($scope.ModeloValorFlete);
                                $scope.ModeloValorConceptos = $scope.MaskValoresGrid($scope.ModeloValorConceptos);
                                //$scope.ModeloPesoCumplido = $scope.MaskValoresGrid($scope.ModeloPesoCumplido);
                                $scope.ModeloValorBase = $scope.MaskValoresGrid($scope.ModeloValorBase);
                                $scope.ModeloValorImpuestos = $scope.MaskValoresGrid($scope.ModeloValorImpuestos);
                                $scope.ModeloValorPagar = $scope.MaskValoresGrid($scope.ModeloValorPagar);
                                var filtroNovedades = {
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    NumeroPlanilla: $scope.NumeroInternoPlanilla
                                }
                                DetalleNovedadesDespachosFactory.Consultar(filtroNovedades).
                                    then(function (response) {
                                        if (response.data.ProcesoExitoso === true) {
                                            if (response.data.Datos.length > 0) {
                                                $scope.ListaNovedades = []
                                                $scope.ModeloListadoImpuestos = []
                                                for (var i = 0; i < response.data.Datos.length; i++) {
                                                    if (response.data.Datos[i].Anulado == 0 && response.data.Datos[i].Novedad.Conceptos.ConceptoLiquidacion.Codigo > 0) {
                                                        $scope.ListaNovedades.push(response.data.Datos[i])
                                                    }
                                                }
                                                for (var i = 0; i < $scope.ListaNovedades.length; i++) {
                                                    for (var j = 0; j < $scope.ModeloListadoConceptos.length; j++) {
                                                        if ($scope.ListaNovedades[i].Novedad.Conceptos.ConceptoLiquidacion.Codigo == $scope.ModeloListadoConceptos[j].ConceptoLiquidacion.Codigo) {
                                                            $scope.ModeloListadoConceptos[j].Valor = $scope.MaskValoresGrid(parseInt(MascaraNumero($scope.ListaNovedades[i].ValorCompra)) + parseInt(MascaraNumero($scope.ModeloListadoConceptos[j].Valor)))
                                                        }
                                                    }
                                                }
                                                for (var i = 0; i < $scope.ModeloListadoConceptos.length; i++) {
                                                    for (var j = 0; j < $scope.ModeloListadoImpuestostemp.length; j++) {
                                                        if (parseInt(MascaraNumero($scope.ModeloListadoConceptos[i].Valor)) > 0) {
                                                            var itemtmp = JSON.parse(JSON.stringify($scope.ModeloListadoImpuestostemp[j]))
                                                            if (itemtmp.ConceptoLiquidacion.Codigo == $scope.ModeloListadoConceptos[i].ConceptoLiquidacion.Codigo) {
                                                                if ($scope.ModeloListadoImpuestos.length == 0) {
                                                                    if ($scope.ModeloListadoConceptos[i].Valor > itemtmp.Valor_base) {
                                                                        itemtmp.Valor_base = parseInt(MascaraNumero($scope.ModeloListadoConceptos[i].Valor))
                                                                    }
                                                                    $scope.ModeloListadoImpuestos.push(itemtmp)
                                                                } else {
                                                                    if ($scope.ModeloListadoConceptos[i].Valor > itemtmp.Valor_base) {
                                                                        itemtmp.Valor_base = parseInt(MascaraNumero($scope.ModeloListadoConceptos[i].Valor))
                                                                    }
                                                                    for (var k = 0; k < $scope.ModeloListadoImpuestos.length; k++) {
                                                                        if ($scope.ModeloListadoImpuestos[k].Codigo == itemtmp.Codigo) {
                                                                            $scope.ModeloListadoImpuestos[k].Valor_base = parseInt(MascaraNumero($scope.ModeloListadoImpuestos[k].Valor_base)) + parseInt(MascaraNumero(itemtmp.Valor_base))
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                for (var i = 0; i < $scope.ModeloListadoImpuestostemp.length; i++) {
                                                    if ($scope.ModeloListadoImpuestostemp[i].ConceptoLiquidacion.Codigo == 0) {
                                                        var itemtmp = JSON.parse(JSON.stringify($scope.ModeloListadoImpuestostemp[i]))
                                                        if (parseInt(MascaraNumero($scope.ModeloValorFlete)) > parseInt(MascaraNumero(itemtmp.Valor_base))) {
                                                            itemtmp.Valor_base = parseInt(MascaraNumero($scope.ModeloValorFlete))
                                                        }
                                                        if ($scope.ModeloListadoImpuestos.length == 0) {
                                                            $scope.ModeloListadoImpuestos.push(itemtmp)
                                                        } else {

                                                            for (var k = 0; k < $scope.ModeloListadoImpuestos.length; k++) {
                                                                if ($scope.ModeloListadoImpuestos[k].Codigo == itemtmp.Codigo) {
                                                                    $scope.ModeloListadoImpuestos[k].Valor_base = parseInt(MascaraNumero($scope.ModeloListadoImpuestos[k].Valor_base)) + parseInt(MascaraNumero(itemtmp.Valor_base))
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            $scope.Calcular()
                                        }
                                    }, function (response) {
                                    });

                                if ($scope.ListadoGuiaGuardadas.length > 0) {
                                    $scope.CondicionesPeso = []
                                    for (var i = 0; i < $scope.ListadoGuiaGuardadas.length; i++) {
                                        filtros = {
                                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                            Codigo: $scope.ListadoGuiaGuardadas[i].Remesa.Cliente.Codigo,
                                            Sync: true,
                                            SoloCondicionesPesoCumplido: 1
                                        };
                                        //--TotalFletecliente
                                        $scope.TotalFleteCliente += $scope.ListadoGuiaGuardadas[i].Remesa.TotalFleteCliente;
                                        //--TotalFletecliente

                                        var Condicion = TercerosFactory.Obtener(filtros)
                                        if (Condicion.ProcesoExitoso === true) {
                                            for (var j = 0; j < Condicion.Datos.CondicionesPeso.length; j++) {
                                                if (Condicion.Datos.CondicionesPeso[j].Producto.Codigo == $scope.ListadoGuiaGuardadas[i].Remesa.ProductoTransportado.Codigo) {
                                                    if ($scope.CondicionesPeso.length > 0) {
                                                        var cont = 0
                                                        for (var k = 0; k < $scope.CondicionesPeso.length; k++) {
                                                            if ($scope.CondicionesPeso[k].Producto.Codigo == $scope.ListadoGuiaGuardadas[i].Remesa.ProductoTransportado.Codigo) {
                                                                cont++
                                                            }
                                                        }
                                                        if (cont == 0) {
                                                            $scope.CondicionesPeso.push(Condicion.Datos.CondicionesPeso[j])
                                                        }
                                                    } else {
                                                        $scope.CondicionesPeso.push(Condicion.Datos.CondicionesPeso[j])
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    $scope.TotalFleteCliente = $scope.MaskValoresGrid($scope.TotalFleteCliente);
                                }
                            }
                        }
                        else {
                            ShowError('La planilla número ' + Planilla + ' no se encuentra en el sistema');
                            PlanillaValidada = false;
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                        document.location.href = '#!ConsultarLiquidaciones';
                    });
            }
        }

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar')
        }

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            window.scrollTo(top, top);
            if (DatosRequeridos()) {

                var Modelo = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHO },
                    Fecha: $scope.ModeloFecha,
                    Numero: $scope.ModeloNumero,
                    NumeroDocumento: $scope.ModeloNumeroDocumento,
                    NumeroPlanilla: $scope.NumeroInternoPlanilla,
                    NumeroManifiesto: $scope.ModeloNumeroManifiesto,
                    FechaEntrega: $scope.ModeloFecha,
                    Observaciones: $scope.ModeloObservaciones,
                    ValorFleteTranportador: $scope.ModeloValorFlete,
                    ValorConceptosLiquidacion: $scope.ModeloValorConceptos,
                    ValorBaseImpuestos: $scope.ModeloValorBase,
                    ValorImpuestos: $scope.ModeloValorImpuestos,
                    ValorPagar: $scope.ModeloValorPagar,
                    Anulado: 0,
                    Estado: $scope.ModeloEstado.Codigo,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Oficina: { Codigo: OficinaUsuario.Codigo },
                    Numeracion: '',
                    ListaDetalleLiquidacion: $scope.ModeloListadoConceptos,
                    ListaImpuestosConceptosLiquidacion: $scope.ModeloListadoImpuestos,
                    HaciaTemporal: 0,
                    PesoCumplido: $scope.ModeloPesoCumplido,
                    PesoCargue: $scope.ModeloPesoCargue,
                    Cantidad: $scope.ModeloCantidad,
                    PesoFaltante: $scope.PesoFaltante
                    //CuentaPorPagar: $scope.ModeloCxP
                }

                //$scope.ModeloCxP = {};

                if ($scope.ModeloEstado.Codigo === ESTADO_DEFINITIVO /* && $scope.MaskNumeroGrid($scope.ModeloValorPagar) > CERO */ && $scope.Sesion.Empresa.AprobarLiquidacion == 0) {
                    if ($scope.Sesion.UsuarioAutenticado.ManejoCuentasPorCobrarPagarLiquidacionesPlanilla) {
                        Modelo.CuentaPorPagar = {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            TipoDocumento: parseInt($scope.ModeloValorPagar) > 0 ? CODIGO_TIPO_DOCUMENTO_CUENTA_POR_PAGAR : CODIGO_TIPO_DOCUMENTO_CUENTA_POR_COBRAR,
                            CodigoAlterno: '',
                            Fecha: $scope.ModeloFecha,
                            Tercero: { Codigo: TerceroLiquidar },
                            DocumentoOrigen: { Codigo: CODIGO_DOCUMENTO_ORIGEN_LIQUIDACION },
                            CodigoDocumentoOrigen: CODIGO_DOCUMENTO_ORIGEN_LIQUIDACION,
                            Numero: $scope.ModeloNumero,
                            Fecha: $scope.ModeloFecha,
                            Numeracion: '',
                            CuentaPuc: { Codigo: 0 },
                            ValorTotal: $scope.ModeloValorPagar,
                            Abono: 0,
                            Saldo: $scope.ModeloValorPagar,
                            FechaCancelacionPago: $scope.ModeloFecha,
                            Aprobado: 1,
                            UsuarioAprobo: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                            FechaAprobo: $scope.ModeloFecha,
                            Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                            Vehiculo: { Codigo: 0 },
                            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        }
                    }
                }



                LiquidacionesFactory.Guardar(Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos === -1) {
                                ShowError('La planilla ya se encuentra liquidada');
                            } else {
                                if (response.data.Datos > 0) {
                                    if ($scope.ModeloNumero === 0) {
                                        ShowSuccess('Se guardó la liquidación ' + response.data.Datos);
                                        document.location.href = '#!ConsultarLiquidaciones/' + response.data.Datos;

                                    }
                                    else {
                                        ShowSuccess('Se modificó la liquidación ' + response.data.Datos);
                                        document.location.href = '#!ConsultarLiquidaciones/' + response.data.Datos;
                                    }
                                } else {
                                    ShowError(response.data.MensajeOperacion);
                                }
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var Continuar = true;

            if ($scope.ModeloFecha === undefined || $scope.ModeloFecha === '' || $scope.ModeloFecha == null) {
                $scope.MensajesError.push('Debe ingresar la fecha');
                Continuar = false
            }
            if ($scope.ModeloNumeroPlanilla === undefined || $scope.ModeloNumeroPlanilla === '' || $scope.ModeloNumeroPlanilla == null || PlanillaValidada == false) {
                $scope.MensajesError.push('Debe ingresar y validar el número de la planilla');
                Continuar = false
            }
            if ($scope.ModeloObservaciones == undefined || $scope.ModeloObservaciones == '' || $scope.ModeloObservaciones == null) {
                $scope.ModeloObservaciones = '';
            }
            if ($scope.ModeloOficina.Codigo == -1) { //TODAS
                Continuar = false;
                $scope.MensajesError.push('Debe seleccionar la oficina');
            }
            if ($scope.ModeloEstado.Codigo == -1) { //TODOS
                Continuar = false;
                $scope.MensajesError.push('Debe seleccionar el estado');
            }
            if ($scope.ModeloValorFlete == undefined || $scope.ModeloValorFlete == '' || $scope.ModeloValorFlete == null || $scope.ModeloValorFlete == '0' || $scope.ModeloValorFlete == 0) { //TODOS
                Continuar = false;
                $scope.MensajesError.push('Debe ingresar el valor flete');
            }
            return Continuar;
        }

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarLiquidaciones/' + $scope.ModeloNumeroDocumento;
        };
        console.clear();
        $scope.Calcular = function () {
            var BaseActual;
            TotalConceptosSuma = 0;
            TotalConceptosResta = 0;

            BaseActual = $scope.ModeloValorBase;
            ValorConceptos = 0;
            TotalBaseImpuestos = 0;

            //Total conceptos
            for (i = 0; i < $scope.ModeloListadoConceptos.length; i++) {
                if ($scope.MaskNumeroGrid($scope.ModeloListadoConceptos[i].Valor) === '' || $scope.MaskNumeroGrid($scope.ModeloListadoConceptos[i].Valor) === null || $scope.MaskNumeroGrid($scope.ModeloListadoConceptos[i].Valor) === undefined || isNaN($scope.MaskNumeroGrid($scope.ModeloListadoConceptos[i].Valor))) {
                    ValorConceptos += 0;
                } else {
                    if ($scope.ModeloListadoConceptos[i].Operacion == 2) { //Resta
                        TotalConceptosResta += $scope.MaskNumeroGrid($scope.ModeloListadoConceptos[i].Valor);
                    } else if ($scope.ModeloListadoConceptos[i].Operacion == 1) { //Suma
                        TotalConceptosSuma += $scope.MaskNumeroGrid($scope.ModeloListadoConceptos[i].Valor);
                    }
                }

                if ($scope.MaskNumeroGrid($scope.ModeloListadoConceptos[i].Valor > 0)) {
                    if ($scope.ModeloListadoConceptos[i].AplicaValorBase === 1) {
                        TotalBaseImpuestos += parseFloat($scope.ModeloListadoConceptos[i].Valor);
                    }
                }
            }
            //ValorConceptos += $scope.MaskNumeroGrid($scope.ModeloListadoConceptos[i].Valor);

            //------------------------------
            $scope.ModeloListadoImpuestos = []
            for (var i = 0; i < $scope.ModeloListadoConceptos.length; i++) {
                for (var j = 0; j < $scope.ModeloListadoImpuestostemp.length; j++) {
                    if (parseInt(MascaraNumero($scope.ModeloListadoConceptos[i].Valor)) > 0) {
                        var itemtmp = JSON.parse(JSON.stringify($scope.ModeloListadoImpuestostemp[j]))
                        if (itemtmp.ConceptoLiquidacion.Codigo == $scope.ModeloListadoConceptos[i].ConceptoLiquidacion.Codigo) {
                            if ($scope.ModeloListadoImpuestos.length == 0) {
                                if (parseInt(MascaraNumero($scope.ModeloListadoConceptos[i].Valor)) > parseInt(MascaraNumero(itemtmp.Valor_base))) {
                                    itemtmp.Valor_base = parseInt(MascaraNumero($scope.ModeloListadoConceptos[i].Valor))
                                }
                                $scope.ModeloListadoImpuestos.push(itemtmp)
                                TotalBaseImpuestos = parseInt(MascaraNumero(itemtmp.Valor_base))
                            } else {
                                if (parseInt(MascaraNumero($scope.ModeloListadoConceptos[i].Valor)) > parseInt(MascaraNumero(itemtmp.Valor_base))) {
                                    itemtmp.Valor_base = parseInt(MascaraNumero($scope.ModeloListadoConceptos[i].Valor))
                                }
                                var cont = 0
                                for (var k = 0; k < $scope.ModeloListadoImpuestos.length; k++) {
                                    if ($scope.ModeloListadoImpuestos[k].Codigo == itemtmp.Codigo) {
                                        $scope.ModeloListadoImpuestos[k].Valor_base = parseInt(MascaraNumero($scope.ModeloListadoImpuestos[k].Valor_base)) + parseInt(MascaraNumero(itemtmp.Valor_base))
                                        TotalBaseImpuestos += parseInt(MascaraNumero($scope.ModeloListadoImpuestos[k].Valor_base));
                                        cont++
                                    }
                                }
                                if (cont == 0) {
                                    $scope.ModeloListadoImpuestos.push(itemtmp)
                                    TotalBaseImpuestos = parseInt(MascaraNumero(itemtmp.Valor_base))
                                }
                            }
                        }
                    }
                }
            }
            ValorConceptos = TotalConceptosResta - TotalConceptosSuma;
            $scope.ModeloValorConceptos = ValorConceptos;
            for (var i = 0; i < $scope.ModeloListadoImpuestostemp.length; i++) {
                if ($scope.ModeloListadoImpuestostemp[i].ConceptoLiquidacion.Codigo == 0) {
                    var itemtmp = JSON.parse(JSON.stringify($scope.ModeloListadoImpuestostemp[i]))
                    if (parseInt(MascaraNumero($scope.ModeloValorFlete)) > parseInt(MascaraNumero(itemtmp.Valor_base))) {
                        itemtmp.Valor_base = parseInt(MascaraNumero($scope.ModeloValorFlete))
                    }
                    if ($scope.ModeloListadoImpuestos.length == 0) {
                        $scope.ModeloListadoImpuestos.push(itemtmp)
                    } else {

                        var cont = 0
                        for (var k = 0; k < $scope.ModeloListadoImpuestos.length; k++) {
                            if ($scope.ModeloListadoImpuestos[k].Codigo == itemtmp.Codigo) {
                                $scope.ModeloListadoImpuestos[k].Valor_base = parseInt(MascaraNumero($scope.ModeloListadoImpuestos[k].Valor_base)) + parseInt(MascaraNumero(itemtmp.Valor_base))
                                cont++
                            }
                        }
                        if (cont == 0) {
                            $scope.ModeloListadoImpuestos.push(itemtmp)
                        }
                    }
                }
            }
            TotalValorImpuestos = 0
            for (i = 0; i < $scope.ModeloListadoImpuestos.length; i++) {
                $scope.ModeloListadoImpuestos[i].ValorImpuesto = Math.round(parseInt(MascaraNumero($scope.ModeloListadoImpuestos[i].Valor_base)) * parseFloat($scope.ModeloListadoImpuestos[i].Valor_tarifa))
                TotalValorImpuestos += $scope.ModeloListadoImpuestos[i].ValorImpuesto;
                $scope.ModeloListadoImpuestos[i].Valor_base = $scope.MaskValoresGrid($scope.ModeloListadoImpuestos[i].Valor_base);
                $scope.ModeloListadoImpuestos[i].ValorImpuesto = $scope.MaskValoresGrid($scope.ModeloListadoImpuestos[i].ValorImpuesto);
            }

            $scope.ModeloValorConceptos = (TotalConceptosResta - TotalConceptosSuma) * -1;
            $scope.ModeloValorImpuestos = Math.round(TotalValorImpuestos);
            PlanillaValidada = true;

            //Formatear campos
            $scope.ModeloValorFlete = $scope.MaskValoresGrid($scope.ModeloValorFlete);
            $scope.ModeloValorImpuestos = $scope.MaskValoresGrid($scope.ModeloValorImpuestos);

            //------------------------------

            // Modificacion SC - 07/01/2020
            //$scope.ModeloValorBase = parseInt(MascaraNumero($scope.ModeloValorFlete)) + parseInt(MascaraNumero(TotalBaseImpuestos));
            if ($scope.ModeloCodDespacho == 301 && $scope.Sesion.UsuarioAutenticado.ManejoRecalculoFleteLiquidacionPesoCumplido) {
                $scope.ModeloValorBase = Math.round(((parseInt($scope.MaskNumeroGrid($scope.ModeloValorFlete)) * parseInt($scope.MaskNumeroGrid($scope.ModeloPesoCumplido))) / 1000));
                $scope.ModeloValorPagar = Math.round(((parseInt($scope.MaskNumeroGrid($scope.ModeloValorFlete)) * parseInt($scope.MaskNumeroGrid($scope.ModeloPesoCumplido))) / 1000) - ValorConceptos - $scope.MaskNumeroGrid($scope.ModeloValorImpuestos));
            } else {
                $scope.ModeloValorBase = parseInt($scope.MaskNumeroGrid($scope.ModeloValorFlete));
                $scope.ModeloValorPagar = parseInt($scope.MaskNumeroGrid($scope.ModeloValorFlete)) - ValorConceptos - $scope.MaskNumeroGrid($scope.ModeloValorImpuestos);
            }

            //Formatear Campos
            $scope.ModeloValorConceptos = $scope.MaskValoresGrid($scope.ModeloValorConceptos);
            $scope.ModeloValorBase = $scope.MaskValoresGrid($scope.ModeloValorBase);
            $scope.ModeloValorPagar = $scope.MaskValoresGrid($scope.ModeloValorPagar);
        }

        $scope.ListadoConceptosCxC = []

        $scope.GuardarValorSaldoOriginal = function (item, index) {
            var Concidencias = 0
            if ($scope.ListadoConceptosCxC.length > 0) {
                $scope.ListadoConceptosCxC.forEach(itemConcepto => {
                    if (itemConcepto.index == index) {
                        itemConcepto.Valor = MascaraNumero(itemConcepto.Valor) < MascaraNumero(item.Valor) ? item.Valor : itemConcepto.Valor
                        Coincidencias++;
                    }
                });
                if (Coincidencias == 0) {
                    $scope.ListadoConceptosCxC.push({
                        index: index,
                        Valor: item.Valor
                    });
                }
            } else {
                $scope.ListadoConceptosCxC.push({
                    index: index,
                    Valor: item.Valor
                });
            }
        }


        //Validar Saldo CxC:
        $scope.ValidarSaldoConcepto = function (item, index) {
            $scope.ListadoConceptosCxC.forEach(itemConcepto => {

                if (itemConcepto.index == index) {
                    if (itemConcepto.Codigo == 10000) {
                        if (MascaraNumero(item.Valor) > MascaraNumero(itemConcepto.Valor)) {
                            ShowError('El valor del abono(' + item.Valor + '), no puede superar el valor del saldo actual(' + itemConcepto.Valor + ')')
                            item.Valor = itemConcepto.Valor
                        }
                    }
                }
            });

        }



        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskMayusGrid = function (item) {
            return MascaraMayus(item)
        };
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };
        $scope.MaskMoneda = function (option) {
            MascaraMonedaGeneral($scope)
        };
        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        $scope.MaskHoraGrid = function (item) {
            return MascaraHora(item)
        };

        /* Obtener parametros*/
        try {
            var Parametros = $routeParams.Codigo.split(',')
            if (Parametros.length > 1) {
                $scope.ModeloNumero = Parametros[0];
                $scope.aplicabase = true
                Obtener();
            } else {
                if ($routeParams.Codigo > 0) {
                    $scope.ModeloNumero = $routeParams.Codigo;
                    Obtener();
                }
                else {
                    $scope.ModeloNumero = 0;
                }

            }
        } catch (e) {
            if ($routeParams.Codigo > 0) {
                $scope.ModeloNumero = $routeParams.Codigo;
                Obtener();
            }
            else {
                $scope.ModeloNumero = 0;
            }

        }
    }]);