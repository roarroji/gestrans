﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.General
Imports System.Transactions

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="RepositorioCiudades"/>
    ''' </summary>
    Public NotInheritable Class RepositorioCiudades
        Inherits RepositorioBase(Of Ciudades)

        Public Overrides Function Consultar(filtro As Ciudades) As IEnumerable(Of Ciudades)
            Dim lista As New List(Of Ciudades)
            Dim item As Ciudades

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If Not IsNothing(filtro.ValorAutocomplete) Or filtro.Sync = True Then
                    conexion.AgregarParametroSQL("@par_Filtro", filtro.ValorAutocomplete)
                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If
                    If filtro.CiudadOficinas > 0 Then
                        conexion.AgregarParametroSQL("@par_Ciudad_Oficinas", 1)
                    End If
                    If filtro.CiudadRegiones > 0 Then
                        conexion.AgregarParametroSQL("@par_Ciudad_Regiones", 1)
                    End If
                    'If filtro.Estado >= Cero Then
                    '    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    'End If
                    If Not IsNothing(filtro.Region) Then
                        If filtro.Region.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_REPA_Codigo", filtro.Region.Codigo)
                        End If
                    End If
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_ciudades_Autocomplete")
                    While resultado.Read
                        item = New Ciudades(resultado)
                        lista.Add(item)
                    End While

                Else
                    If Not IsNothing(filtro.CodigoAlterno) Then
                        conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                    End If

                    If Not IsNothing(filtro.Nombre) Then
                        If Len(filtro.Nombre) > Cero Then
                            conexion.AgregarParametroSQL("@par_Ciudad", filtro.Nombre)
                        End If
                    End If

                    If Not IsNothing(filtro.Paises) Then
                        If filtro.Paises.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_PAIS_Codigo", filtro.Paises.Codigo)
                        End If
                    End If

                    If Not IsNothing(filtro.Departamento) Then
                        If Not IsNothing(filtro.Departamento.Nombre) Then
                            conexion.AgregarParametroSQL("@par_Departamento", filtro.Departamento.Nombre)
                        End If
                    End If
                    If filtro.Estado >= Cero Then
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    End If
                    If filtro.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If

                    If Not IsNothing(filtro.Pagina) Then
                        If filtro.Pagina > Cero Then
                            conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                        End If
                    End If
                    If Not IsNothing(filtro.RegistrosPagina) Then
                        If filtro.RegistrosPagina > Cero Then
                            conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                        End If
                    End If
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_ciudades]")

                    While resultado.Read
                        item = New Ciudades(resultado)
                        lista.Add(item)
                    End While
                End If

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As Ciudades) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True
            Dim insertoConcepto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_DEPA_Codigo", entidad.Departamento.Codigo)
                    conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                    conexion.AgregarParametroSQL("@par_Codigo_Postal", entidad.CodigoPostal)
                    conexion.AgregarParametroSQL("@par_Calcular_ICA_Oficina", entidad.CalcularICAOficina)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea)


                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_ciudades")
                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While


                    resultado.Close()

                    If entidad.Codigo.Equals(Cero) Then
                        Return Cero
                    End If

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Function ConsultarCodigoDane(entidad As CodigoDaneCiudades) As IEnumerable(Of CodigoDaneCiudades)
            Dim lista As New List(Of CodigoDaneCiudades)
            Dim item As CodigoDaneCiudades

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_codigo_dane_ciudades]")

                While resultado.Read
                    item = New CodigoDaneCiudades(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function


        Public Overrides Function Modificar(entidad As Ciudades) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.CodigoCiudad)
                conexion.AgregarParametroSQL("@par_DEPA_Codigo", entidad.Departamento.Codigo)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_Codigo_Postal", entidad.CodigoPostal)
                conexion.AgregarParametroSQL("@par_Calcular_ICA_Oficina", entidad.CalcularICAOficina)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo", entidad.UsuarioModifica)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_ciudades")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As Ciudades) As Ciudades
            Dim item As New Ciudades

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > Cero Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_ciudades]")

                While resultado.Read
                    item = New Ciudades(resultado)
                End While

            End Using

            Return item
        End Function
        Public Function Anular(entidad As Ciudades) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_ciudades]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > Cero, True, False)
                End While

            End Using

            Return anulo
        End Function
        Public Function ConsultarOficinas(filtro As Ciudades) As IEnumerable(Of Oficinas)
            Dim lista As New List(Of Oficinas)
            Dim item As Oficinas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_CIUD_Codigo", filtro.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_ciudades_oficinas")

                While resultado.Read
                    item = New Oficinas(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Function GuardarOficinas(Entidad As Ciudades) As Long
            Dim lonResultado As Long = 0
            If Entidad.Oficinas.Count <> 0 Then
                Call EliminarOficinas(Entidad.CodigoEmpresa, Entidad.Codigo)
            End If

            For Each Oficina In Entidad.Oficinas

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", Oficina.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_CIUD_Codigo", Entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", Oficina.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_ciudades_oficinas")
                    While resultado.Read
                        Oficina.Codigo = Convert.ToInt64(resultado.Item("OFIC_Codigo").ToString())
                    End While

                    resultado.Close()

                    If Oficina.Codigo.Equals(Cero) Then
                        lonResultado = Cero
                        Exit For
                    End If

                    lonResultado = Oficina.Codigo
                End Using
            Next

            Return lonResultado
        End Function

        Public Sub EliminarOficinas(codigoEmpresa As Short, CodigoCiudad As Short)
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                    conexion.AgregarParametroSQL("@par_CIUD_Codigo", CodigoCiudad)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_ciudad_oficinas")
                End Using
                transaccion.Complete()
            End Using
        End Sub


        Public Function ConsultarRegiones(filtro As Ciudades) As IEnumerable(Of RegionesPaises)
            Dim lista As New List(Of RegionesPaises)
            Dim item As RegionesPaises

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_CIUD_Codigo", filtro.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_ciudades_regiones")

                While resultado.Read
                    item = New RegionesPaises(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Function GuardarRegiones(Entidad As Ciudades) As Long
            Dim lonResultado As Long = 0
            If Entidad.Regiones.Count <> 0 Then
                Call EliminarRegiones(Entidad.CodigoEmpresa, Entidad.Codigo)
            End If

            For Each Regiones In Entidad.Regiones

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", Regiones.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_CIUD_Codigo", Entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_REPA_Codigo", Regiones.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_ciudades_regiones")
                    While resultado.Read
                        Regiones.Codigo = Convert.ToInt64(resultado.Item("REPA_Codigo").ToString())
                    End While

                    resultado.Close()

                    If Regiones.Codigo.Equals(Cero) Then
                        lonResultado = Cero
                        Exit For
                    End If

                    lonResultado = Regiones.Codigo
                End Using
            Next

            Return lonResultado
        End Function

        Public Sub EliminarRegiones(codigoEmpresa As Short, CodigoCiudad As Short)
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                    conexion.AgregarParametroSQL("@par_CIUD_Codigo", CodigoCiudad)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_ciudad_regiones")
                End Using
                transaccion.Complete()
            End Using
        End Sub

    End Class

End Namespace