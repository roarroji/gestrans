﻿Imports System.Web.Http

Imports Softtools.GesCarga.IFachada
Imports System.Runtime.Caching
Imports Softtools.GesCarga.Entidades.Basico
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria
Imports Softtools.GesCarga.Entidades.Basico.ServicioCliente
Imports Softtools.GesCarga.Entidades.Basico.Almacen
Imports Softtools.GesCarga.Entidades.Almacen
Imports Softtools.GesCarga.Entidades.Mantenimiento
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports Softtools.GesCarga.Entidades.Gesphone
Imports Softtools.GesCarga.Entidades.Despachos
Imports Softtools.GesCarga.Entidades.Despachos.Remesa
Imports Softtools.GesCarga.Entidades.Tesoreria
Imports Softtools.GesCarga.Entidades.Utilitarios
Imports Softtools.GesCarga.Entidades.ServicioCliente
Imports Softtools.GesCarga.Entidades.Despachos.Paqueteria
Imports Softtools.GesCarga.Entidades.Despachos.Masivo
Imports Softtools.GesCarga.Entidades.Despachos.OrdenCargue
Imports Softtools.GesCarga.Entidades.Despachos.Procesos
Imports Softtools.GesCarga.Entidades.FlotaPropia
Imports Softtools.GesCarga.Entidades.Proveedores
Imports Softtools.GesCarga.Entidades.Fidelizacion.Documentos


Imports Softtools.GesCarga.Entidades.Basico.Facturacion

Imports Softtools.GesCarga.Fachada.Basico.General
Imports Softtools.GesCarga.Fachada.Basico.Despachos
Imports Softtools.GesCarga.Fachada.Basico.ServicioCliente
Imports Softtools.GesCarga.Fachada.Basico.Tesoreria
Imports Softtools.GesCarga.Fachada.Basico.Almacen
Imports Softtools.GesCarga.Fachada.Almacen
Imports Softtools.GesCarga.Fachada.Mantenimiento
Imports Softtools.GesCarga.Fachada.Basico.Facturacion
Imports Softtools.GesCarga.Fachada.SeguridadUsuarios
Imports Softtools.GesCarga.Fachada.Gesphone
Imports Softtools.GesCarga.Fachada.FlotaPropia
Imports Softtools.GesCarga.Fachada.Proveedores
Imports Softtools.GesCarga.Fachada.Despachos
Imports Softtools.GesCarga.Fachada.Tesoreria
Imports Softtools.GesCarga.Entidades.Comercial.Documentos
Imports Softtools.GesCarga.Fachada.Comercial.Documentos
Imports Softtools.GesCarga.Fachada.Paqueteria
Imports Softtools.GesCarga.Fachada.Utilitarios
Imports Softtools.GesCarga.Entidades.Facturacion
Imports Softtools.GesCarga.Fachada.Facturacion
Imports Softtools.GesCarga.Fachada.ServicioCliente
Imports Softtools.GesCarga.Fachada.Despachos.Masivo
Imports Softtools.GesCarga.Entidades.Seguridad
Imports Softtools.GesCarga.Fachada.Seguridad
Imports Softtools.GesCarga.Entidades.ControlViajes
Imports Softtools.GesCarga.Fachada.ControlViajes
Imports Softtools.GesCarga.Entidades.Contabilidad.Documentos
Imports Softtools.GesCarga.Fachada.Contabilidad
Imports Softtools.GesCarga.Fachada.Despachos.Remesa
Imports Softtools.GesCarga.Entidades.Paqueteria
Imports Softtools.GesCarga.Fachada.Despachos.Paqueteria
Imports Softtools.GesCarga.Entidades.Basico.Operacion
Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Repositorio
Imports Softtools.GesCarga.Entidades.Basico.FlotaPropia
Imports Softtools.GesCarga.Fachada.Basico.FlotaPropia
Imports Softtools.GesCarga.Entidades.Contabilidad.Procesos
Imports Softtools.GesCarga.Fachada.Basico
Imports Softtools.GesCarga.Fachada.Fidelizacion.Documentos


''' <summary>
''' Clase <see cref="Base"/>
''' </summary>
Public Class Base
    Inherits ApiController

#Region "Atributos Basico"

    ''' <summary>
    ''' Variable de entorno para el manejo de las politicas de cache
    ''' </summary>
    Dim politicaCache As CacheItemPolicy

    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para empresas.
    ''' </summary>
    ''' 
    Dim persistenciaSolicitudAnticipo As IPersistenciaBase(Of EncabezadoSolicitudAnticipo)
    Dim persistenciaempresas As IPersistenciaBase(Of Empresas)
    Dim persistenciaSistemas As IPersistenciaBase(Of Sistemas)
    Dim persistenciaTipoSubsistemas As IPersistenciaBase(Of TipoSubsistemas)
    Dim persistenciaSubSistemas As IPersistenciaBase(Of SubSistemas)
    Dim persistenciaUnidadReferenciaMantenimiento As IPersistenciaBase(Of UnidadReferenciaMantenimiento)
    Dim PersistenciaEstadoTipoMantenimiento As IPersistenciaBase(Of TipoEquipoMantenimiento)
    Dim PersistenciaEstadoEquipoMantenimiento As IPersistenciaBase(Of EstadoEquipoMantenimiento)
    Dim PersistenciaMarcaEquipoMantenimiento As IPersistenciaBase(Of MarcaEquipoMantenimiento)
    Dim PersistenciaDocumentoEquipoMantenimiento As IPersistenciaBase(Of EquipoMantenimientoDocumento)
    Dim persistenciaEquipoMantenimiento As IPersistenciaBase(Of EquipoMantenimiento)

    Dim persistenciaPlanEquipoMantenimiento As IPersistenciaBase(Of PlanEquipoMantenimiento)
    Dim persistenciaDetallePlanEquipoMantenimiento As IPersistenciaBase(Of DetallePlanEquipoMantenimiento)
    Dim persistenciaTareaMantenimiento As IPersistenciaBase(Of TareaMantenimiento)
    Dim persistenciaDetalleTareaMantenimiento As IPersistenciaBase(Of DetalleTareaMantenimiento)
    Dim persistenciaOrdenTrabajoMantenimiento As IPersistenciaBase(Of OrdenTrabajoMantenimiento)
    Dim persistenciaDetalleOrdenTrabajoMantenimiento As IPersistenciaBase(Of DetalleOrdenTrabajoMantenimiento)
    Dim persistenciaValorCombustible As IPersistenciaBase(Of ValorCombustible)
    Dim persistenciaLegalizacionGastosRuta As IPersistenciaBase(Of LegalizacionGastosRuta)




    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Notificaciones.
    ''' </summary>
    Dim persistencianotificaciones As IPersistenciaBase(Of Notificaciones)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para empresas.
    ''' </summary>
    Dim persistenciaeventocorreos As IPersistenciaBase(Of EventoCorreos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para empresas.
    ''' </summary>
    Dim persistencialistadribucioncorreos As IPersistenciaBase(Of ListaDistribucionCorreos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Usuarios.
    ''' </summary>
    Dim persistenciausuarios As IPersistenciaBase(Of Usuarios)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Usuarios.
    ''' </summary>
    Dim persistenciausuariogrupousuarios As IPersistenciaBase(Of UsuarioGrupoUsuarios)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Usuarios.
    ''' </summary>
    Dim persistenciapermisogrupousuarios As IPersistenciaBase(Of PermisoGrupoUsuarios)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Usuarios.
    ''' </summary>
    Dim persistenciagrupousuarios As IPersistenciaBase(Of GrupoUsuarios)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Usuarios.
    ''' </summary>
    Dim persistenciaestudioseguridad As IPersistenciaBase(Of EstudioSeguridad)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Usuarios.
    ''' </summary>
    Dim persistenciaestudioseguridaddocumentos As IPersistenciaBase(Of EstudioSeguridadDocumentos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Usuarios.
    ''' </summary>
    Dim persistenciagestiondocumentaldocumentos As IPersistenciaBase(Of GestionDocumentalDocumentos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Usuarios.
    ''' </summary>
    Dim persistenciatipodocumentoestudioseguridad As IPersistenciaBase(Of TipoDocumentoEstudioSeguridad)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Usuarios.
    ''' </summary>
    Dim persistenciaentidadorigenestudioseguridad As IPersistenciaBase(Of EntidadOrigenEstudioSeguridad)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Bacos.
    ''' </summary>
    Dim persistenciabancos As IPersistenciaBase(Of Bancos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para PlanUnicoCuentas.
    ''' </summary>
    Dim persistenciaplanunicocuentas As IPersistenciaBase(Of PlanUnicoCuentas)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para PlanUnicoCuentas.
    ''' </summary>
    Dim persistenciacuentabancarias As IPersistenciaBase(Of CuentaBancarias)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para CuentaBancariaOficinas.
    ''' </summary>
    Dim persistenciaCuentaBancariaOficinas As IPersistenciaBase(Of CuentaBancariaOficinas)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Oficinas.
    ''' </summary>
    Dim persistenciazonas As IPersistenciaBase(Of Zonas)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Oficinas.
    ''' </summary>
    Dim persistenciasitiostercerocliente As IPersistenciaBase(Of SitiosTerceroCliente)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Oficinas.
    ''' </summary>
    Dim persistenciaoficinas As IPersistenciaBase(Of Oficinas)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Oficinas.
    ''' </summary>
    Dim PersistenciaTerceros As IPersistenciaBase(Of Terceros)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Catalogos.
    ''' </summary>
    Dim persistenciacatalogos As IPersistenciaBase(Of Catalogos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para ValorCatalogos.
    ''' </summary>
    Dim persistenciaunidadmedida As IPersistenciaBase(Of UnidadMedida)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para ValorCatalogos.
    ''' </summary>
    Dim persistenciaunidadempaque As IPersistenciaBase(Of UnidadEmpaque)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para ValorCatalogos.
    ''' </summary>
    Dim persistenciaproductosministeriotransporte As IPersistenciaBase(Of ProductosMinisterioTransporte)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para PuertosPaises.
    ''' </summary>
    Dim persistenciapuertospaises As IPersistenciaBase(Of PuertosPaises)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para ValorCatalogos.
    ''' </summary>
    Dim persistenciavalorcatalogos As IPersistenciaBase(Of ValorCatalogos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Ciudades.
    ''' </summary>
    Dim persistenciaciudades As IPersistenciaBase(Of Ciudades)
    Dim persistenciacodigosdaneciudades As IPersistenciaBase(Of CodigoDaneCiudades)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Ciudades.
    ''' </summary>
    Dim persistenciaimpuestos As IPersistenciaBase(Of Impuestos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Ciudades.
    ''' </summary>
    Dim persistenciatipodocumentos As IPersistenciaBase(Of TipoDocumentos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Ciudades.
    ''' </summary>
    Dim persistenciaproductotransportados As IPersistenciaBase(Of ProductoTransportados)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Impuesto tipo documentos.
    ''' </summary>
    Dim persistenciaimpuestotipodocumentos As IPersistenciaBase(Of ImpuestoTipoDocumentos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Impuesto tipo documentos.
    ''' </summary>
    Dim persistenciaformularioinspecciones As IPersistenciaBase(Of FormularioInspecciones)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Departamentos.
    ''' </summary>
    Dim persistenciadepartamentos As IPersistenciaBase(Of Departamentos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Departamentos.
    ''' </summary>
    Dim persistenciaconfiguracioncatalogos As IPersistenciaBase(Of ConfiguracionCatalogos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Marca Vehiculos.
    ''' </summary>
    Dim persistenciamarcavehiculos As IPersistenciaBase(Of MarcaVehiculos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Color Vehiculos.
    ''' </summary>
    Dim persistenciacolorvehiculos As IPersistenciaBase(Of ColorVehiculos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para rendimiento galon combustible
    ''' </summary>
    Dim persistenciaRendimientoGalonCombustible As IPersistenciaBase(Of RendimientoGalonCombustible)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Linea Vehiculos.
    ''' </summary>
    Dim persistencialineavehiculos As IPersistenciaBase(Of LineaVehiculos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Linea Vehiculos.
    ''' </summary>
    Dim persistenciasitioscarguedescargue As IPersistenciaBase(Of SitiosCargueDescargue)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Rutas.
    ''' </summary>
    Dim persistenciarutas As IPersistenciaBase(Of Rutas)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Rutas.
    ''' </summary>
    Dim PersistenciaPrecintos As IPersistenciaBase(Of Precintos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Rutas.
    ''' </summary>
    Dim persistenciapuestocontroles As IPersistenciaBase(Of PuestoControles)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para peajes.
    ''' </summary>
    Dim persistenciapeajes As IPersistenciaBase(Of Peajes)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Almacenes.
    ''' </summary>
    Dim persistenciaalmacenes As IPersistenciaBase(Of Almacenes)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Detalle Ubicacion Almacenes.
    ''' </summary>
    Dim persistenciadetalleubicacionalmacenes As IPersistenciaBase(Of DetalleUbicacionAlmacenes)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Encabezado Ubicacion Almacenes.
    ''' </summary>
    Dim persistenciaencabezadoubicacionalmacenes As IPersistenciaBase(Of EncabezadoUbicacionAlmacenes)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para GrupoReferencias.
    ''' </summary>
    Dim persistenciagruporeferencias As IPersistenciaBase(Of GrupoReferencias)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para ReferenciaAlmacenes.
    ''' </summary>
    Dim persistenciareferenciaalmacenes As IPersistenciaBase(Of ReferenciaAlmacenes)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para ReferenciaProveedores.
    ''' </summary>
    Dim persistenciareferenciaproveedores As IPersistenciaBase(Of ReferenciaProveedores)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Referencias.
    ''' </summary>
    Dim persistenciareferencias As IPersistenciaBase(Of Referencias)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para UnidadEmpaqueReferencias.
    ''' </summary>
    Dim persistenciaunidadempaquereferencias As IPersistenciaBase(Of UnidadEmpaqueReferencias)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para UnidadMedidaReferencias.
    ''' </summary>
    Dim persistenciaunidadmedidareferencias As IPersistenciaBase(Of UnidadMedidaReferencias)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para PuestoControles.
    ''' </summary>
    Dim persistenciafotosvehiculos As IPersistenciaBase(Of FotosVehiculos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para PuestoControles.
    ''' </summary>
    Dim persistenciavehiculos As IPersistenciaBase(Of Vehiculos)
    Dim persistenciaVehiculosListaNegra As IPersistenciaBase(Of Vehiculos)

    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Rutas.
    ''' </summary>
    Dim persistenciamarcasemirremolsques As IPersistenciaBase(Of MarcaSemirremolques)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Rutas.
    ''' </summary>
    Dim Persistenciasemirremolques As IPersistenciaBase(Of Semirremolques)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Cajas.
    ''' </summary>
    Dim persistenciacajas As IPersistenciaBase(Of Cajas)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Cajas.
    ''' </summary>
    Dim PersistenciaConceptoLiquidacion As IPersistenciaBase(Of ConceptoLiquidacionPlanillaDespacho)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Cajas.
    ''' </summary>
    Dim PersistenciaConceptoFacturacion As IPersistenciaBase(Of ConceptoFacturacion)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Cajas.
    ''' </summary>
    Dim PersistenciaConceptoGastos As IPersistenciaBase(Of ConceptoGastos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos Impuesto Facturas
    ''' </summary>
    Dim PersistenciaImpuestoFacturas As IPersistenciaBase(Of ImpuestoFacturas)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos Notas Facturas
    ''' </summary>
    Dim PersistenciaNotasFacturas As IPersistenciaBase(Of NotasFacturas)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de Conceptos Notas Facturas.
    ''' </summary>
    Dim PersistenciaConceptoNotasFacturas As IPersistenciaBase(Of ConceptoNotasFacturas)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Cajas.
    ''' </summary>
    Dim persistenciachequeracuentabancarias As IPersistenciaBase(Of ChequeraCuentaBancarias)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para EncabezadoConceptoContables.
    ''' </summary>
    Dim persistenciaencabezadoconceptocontables As IPersistenciaBase(Of EncabezadoConceptoContables)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para DetalleConceptoContables.
    ''' </summary>
    Dim persistenciadetalleconceptocontables As IPersistenciaBase(Of DetalleConceptoContables)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para EncabezadoParametrizacionContables.
    ''' </summary>
    Dim persistenciaencabezadoparametrizacioncontables As IPersistenciaBase(Of EncabezadoParametrizacionContables)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para DetalleParametrizacionContables.
    ''' </summary>
    Dim persistenciadetalleparametrizacioncontables As IPersistenciaBase(Of DetalleParametrizacionContables)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para DetalleParametrizacionContables.
    ''' </summary>
    Dim persistenciadocumentos As IPersistenciaBase(Of Documentos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para DetalleParametrizacionContables.
    ''' </summary>
    Dim persistenciaconfiguraciongestiondocumentos As IPersistenciaBase(Of ConfiguracionGestionDocumentos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Manifiesto.
    ''' </summary>
    Dim persistenciaManifiesto As IPersistenciaBase(Of Manifiesto)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Manifiesto.
    ''' </summary>
    Dim persistenciaconceptoventas As IPersistenciaBase(Of ConceptoVentas)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Manifiesto.
    ''' </summary>
    Dim persistencialineanegociotransportecarga As IPersistenciaBase(Of LineaNegocioTransporteCarga)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Manifiesto.
    ''' </summary>
    Dim persistenciatarifatransportecarga As IPersistenciaBase(Of TarifaTransporteCarga)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Manifiesto.
    ''' </summary>
    Dim persistenciatipolineanegociocarga As IPersistenciaBase(Of TipoLineaNegocioCarga)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Manifiesto.
    ''' </summary>
    Dim persistenciatipotarifatransportecarga As IPersistenciaBase(Of TipoTarifaTransporteCarga)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para reporte ministerio.
    ''' </summary>
    Dim persistenciareporteministerio As IPersistenciaBase(Of ReporteMinisterio)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para reporte ministerio.
    ''' </summary>
    Dim persistencianovedadesdespachos As IPersistenciaBase(Of NovedadesDespachos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para reporte ministerio.
    ''' </summary>
    Dim persistencianovedadesconceptos As IPersistenciaBase(Of NovedadesConceptos)

    Dim persistenciaLineaMantenimiento As IPersistenciaBase(Of LineaMantenimiento)
    Dim persistenciaPeajesRutas As IPersistenciaBase(Of PeajesRutas)

    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para reporte ministerio.
    ''' </summary>
    Dim persistenciaCombinacionContratoVinculacion As IPersistenciaBase(Of CombinacionContratoVinculacion)
#End Region
#Region "Atributos Gesphone"
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Detalle Seguimiento Vehiculos.
    ''' </summary>
    Dim persistenciaencabezadocomprobantescontables As IPersistenciaBase(Of EncabezadoComprobantesContables)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Detalle Seguimiento Vehiculos.
    ''' </summary>
    Dim persistenciadetalleseguimientovehiculos As IPersistenciaBase(Of DetalleSeguimientoVehiculos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Autorizaciones.
    ''' </summary>
    Dim persistenciaAutorizaciones As IPersistenciaBase(Of Autorizaciones)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Inspeccion Preoperacional.
    ''' </summary>
    Dim persistenciainspeccionpreoperacional As IPersistenciaBase(Of InspeccionPreoperacional)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Inspeccion Preoperacional.
    ''' </summary>
    Dim persistenciadocumentoinspeccionpreoperacional As IPersistenciaBase(Of DocumentoInspeccionPreoperacional)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Detalle Seguimiento Carga Vehiculos.
    ''' </summary>
    Dim persistenciadetalleseguimientocargavehiculos As IPersistenciaBase(Of DetalleSeguimientoCargaVehiculos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para DetalleCheckListCargueDescargues.
    ''' </summary>
    Dim persistenciadetallechecklistcarguedescargues As IPersistenciaBase(Of DetalleCheckListCargueDescargues)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos paraDetalleCheckListVehiculos.
    ''' </summary>
    Dim persistenciadetallechecklistvehiculos As IPersistenciaBase(Of DetalleCheckListVehiculos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para EncabezadoCheckListCargueDescargues.
    ''' </summary>
    Dim persistenciaencabezadochecklistcarguedescargues As IPersistenciaBase(Of EncabezadoCheckListCargueDescargues)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para EncabezadoCheckListVehiculos.
    ''' </summary>
    Dim persistenciaencabezadochecklistvehiculos As IPersistenciaBase(Of EncabezadoCheckListVehiculos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para ItemCheckListVehiculos.
    ''' </summary>
    Dim persistenciaitemchecklistvehiculos As IPersistenciaBase(Of ItemCheckListVehiculos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para ItemListCargueDescargues.
    ''' </summary>
    Dim persistenciaitemlistcarguedescargues As IPersistenciaBase(Of ItemListCargueDescargues)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para ItemListCargueDescargues.
    ''' </summary>
    Dim PersistenciaNovedadTransito As IPersistenciaBase(Of NovedadTransito)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para SyncDetalleTarifarioVenta.
    ''' </summary>
   ' Dim PersistenciaSincronizacionApp As IPersistenciaSincronizacionApp
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Monedas.
    ''' </summary>
    Dim persistenciamonedas As IPersistenciaBase(Of Monedas)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Paises.
    ''' </summary>
    Dim persistenciatrm As IPersistenciaBase(Of TRM)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Paises.
    ''' </summary>
    Dim persistenciapaises As IPersistenciaBase(Of Paises)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Detalle Seguimiento Vehiculos.
    ''' </summary>
    Dim persistenciacierrecontabledocumentos As IPersistenciaBase(Of CierreContableDocumentos)
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para interfaz contable PSL
    ''' </summary>
    Dim persistenciainterfazcontablepsl As IPersistenciaBase(Of InterfazContablePSL)
#End Region
#Region "Atributos Despachos"
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para ControlEntregas.
    ''' </summary>
    Dim persistenciaencabeadoseguimientovehiculos As IPersistenciaBase(Of EncabezadoSeguimientoVehiculos)
    Dim persistenciaRemesaPaqueteria As IPersistenciaBase(Of RemesaPaqueteria)
    Dim persistenciaplanillaguias As IPersistenciaBase(Of PlanillaGuias)
    Dim persistenciaplanillapaqueteria As IPersistenciaBase(Of PlanillaPaqueteria)
    Dim persistencialegalizarecaudoremesas As IPersistenciaBase(Of LegalizarRecaudoRemesas)
    Dim persistenciaplanilladespachos As IPersistenciaBase(Of PlanillaDespachos)
    Dim PersistenciaPlanillaEntregadas As IPersistenciaBase(Of PlanillaEntregadas)
    Dim Persistenciadetallenovedadesdespachos As IPersistenciaBase(Of DetalleNovedadesDespachos)
    Dim Persistenciadetalledistribucionremesas As IPersistenciaBase(Of DetalleDistribucionRemesas)
    Dim Persistenciadetalleplanilladespachos As IPersistenciaBase(Of DetallePlanillaDespachos)
    Dim persistenciarecolecciones As IPersistenciaBase(Of Recolecciones)
    Dim persistenciareplanillacolecciones As IPersistenciaBase(Of PlanillaRecolecciones)
    Dim persistenciacumplidos As IPersistenciaBase(Of Cumplido)
    Dim persistencialiquidaciones As IPersistenciaBase(Of Liquidacion)
    Dim persistenciaLegalizacionGastos As IPersistenciaBase(Of LegalizacionGastos)
    Dim persistenciaPlanillaDespachosOtrasEmpresas As IPersistenciaBase(Of PlanillaDespachosOtrasEmpresas)
    Dim persistenciaPlanillaDespachosProveedores As IPersistenciaBase(Of PlanillasDespachosProveedores)
    Dim persistenciaLiquidacionPlanillaDespachosProveedores As IPersistenciaBase(Of LiquidacionPlanillasProveedores)
    Dim persistenciaremesas As IPersistenciaBase(Of Remesas)
    Dim persistenciaOrdenCargue As IPersistenciaBase(Of OrdenCargue)
    Dim persistenciaEnturnamiento As IPersistenciaBase(Of Enturnamiento)
    Dim persistenciaValidacionPlanillaPaqueteria As IPersistenciaBase(Of EncabezadoValidacionPlanillaPaqueteria)
    Dim persistenciaGuiasPreimpresas As IPersistenciaBase(Of GuiaPreimpresa)
    Dim persistenciaEtiquetasPreimpresas As IPersistenciaBase(Of EtiquetaPreimpresa)
    Dim persistenciaRecepcionGuias As IPersistenciaBase(Of RemesaPaqueteria)
    Dim persistenciaCargueMasivoGuias As IPersistenciaBase(Of RemesaPaqueteria)


#End Region
#Region "Atributos Utilitarios"
    Dim persistenciabandejasalidacorreos As IPersistenciaBase(Of BandejaSalidaCorreos)
    Dim persistenciaconfiguracionservidorcorreos As IPersistenciaBase(Of ConfiguracionServidorCorreos)
    Dim persistencialistadosauditoriadocumentos As IPersistenciaBase(Of ListadosAuditoriaDocumentos)


#End Region
#Region "Atributos Básico"

    Public ReadOnly Property CapaPersistenciaPeajesRutas() As IPersistenciaBase(Of PeajesRutas)
        Get
            If persistenciaPeajesRutas Is Nothing Then
                persistenciaPeajesRutas = New PersistenciaPeajesRutas()
            End If
            Return persistenciaPeajesRutas
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaValorCombustible() As IPersistenciaBase(Of ValorCombustible)
        Get
            If persistenciaValorCombustible Is Nothing Then
                persistenciaValorCombustible = New PersistenciaValorCombustible()
            End If
            Return persistenciaValorCombustible
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaLegalizacionGastosRuta() As IPersistenciaBase(Of LegalizacionGastosRuta)
        Get
            If persistenciaLegalizacionGastosRuta Is Nothing Then
                persistenciaLegalizacionGastosRuta = New PersistenciaLegalizacionGastosRuta()
            End If
            Return persistenciaLegalizacionGastosRuta
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaValidacionPlanillaPaqueteria() As IPersistenciaBase(Of EncabezadoValidacionPlanillaPaqueteria)
        Get
            If persistenciaValidacionPlanillaPaqueteria Is Nothing Then
                persistenciaValidacionPlanillaPaqueteria = New PersistenciaValidacionPlanillaPaqueteria()
            End If
            Return persistenciaValidacionPlanillaPaqueteria
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaOrdenTrabajoMantenimiento() As IPersistenciaBase(Of OrdenTrabajoMantenimiento)
        Get
            If persistenciaOrdenTrabajoMantenimiento Is Nothing Then
                persistenciaOrdenTrabajoMantenimiento = New PersistenciaOrdenTrabajoMantenimiento()
            End If
            Return persistenciaOrdenTrabajoMantenimiento
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaDetalleOrdenTrabajoMantenimiento() As IPersistenciaBase(Of DetalleOrdenTrabajoMantenimiento)
        Get
            If persistenciaDetalleOrdenTrabajoMantenimiento Is Nothing Then
                persistenciaDetalleOrdenTrabajoMantenimiento = New PersistenciaDetalleOrdenTrabajoMantenimiento()
            End If
            Return persistenciaDetalleOrdenTrabajoMantenimiento
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaTareaMantenimiento() As IPersistenciaBase(Of TareaMantenimiento)
        Get
            If persistenciaTareaMantenimiento Is Nothing Then
                persistenciaTareaMantenimiento = New PersistenciaTareaMantenimiento()
            End If
            Return persistenciaTareaMantenimiento
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaDetalleTareaMantenimiento() As IPersistenciaBase(Of DetalleTareaMantenimiento)
        Get
            If persistenciaDetalleTareaMantenimiento Is Nothing Then
                persistenciaDetalleTareaMantenimiento = New PersistenciaDetalleTareaMantenimiento()
            End If
            Return persistenciaDetalleTareaMantenimiento
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaDetallePlanEquipoMantenimiento() As IPersistenciaBase(Of DetallePlanEquipoMantenimiento)
        Get
            If persistenciaDetallePlanEquipoMantenimiento Is Nothing Then
                persistenciaDetallePlanEquipoMantenimiento = New PersistenciaDetallePlanEquipoMantenimiento()
            End If
            Return persistenciaDetallePlanEquipoMantenimiento
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaPlanEquipoMantenimiento() As IPersistenciaBase(Of PlanEquipoMantenimiento)
        Get
            If persistenciaPlanEquipoMantenimiento Is Nothing Then
                persistenciaPlanEquipoMantenimiento = New PersistenciaPlanEquipoMantenimiento()
            End If
            Return persistenciaPlanEquipoMantenimiento
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaEquipoMantenimiento() As IPersistenciaBase(Of EquipoMantenimiento)
        Get
            If persistenciaEquipoMantenimiento Is Nothing Then
                persistenciaEquipoMantenimiento = New PersistenciaEquipoMantenimiento()
            End If
            Return persistenciaEquipoMantenimiento
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaDocumentoEquipoMantenimiento() As IPersistenciaBase(Of EquipoMantenimientoDocumento)
        Get
            If PersistenciaDocumentoEquipoMantenimiento Is Nothing Then
                PersistenciaDocumentoEquipoMantenimiento = New PersistenciaDocumentoEquipoMantenimiento()
            End If
            Return PersistenciaDocumentoEquipoMantenimiento
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaMarcaEquipoMantenimiento() As IPersistenciaBase(Of MarcaEquipoMantenimiento)
        Get
            If PersistenciaMarcaEquipoMantenimiento Is Nothing Then
                PersistenciaMarcaEquipoMantenimiento = New PersistenciaMarcaEquipoMantenimiento()
            End If
            Return PersistenciaMarcaEquipoMantenimiento
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaEstadoEquipoMantenimiento() As IPersistenciaBase(Of EstadoEquipoMantenimiento)
        Get
            If PersistenciaEstadoEquipoMantenimiento Is Nothing Then
                PersistenciaEstadoEquipoMantenimiento = New PersistenciaEstadoEquipoMantenimiento()
            End If
            Return PersistenciaEstadoEquipoMantenimiento
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaTipoEquipoMantenimiento() As IPersistenciaBase(Of TipoEquipoMantenimiento)
        Get
            If PersistenciaEstadoTipoMantenimiento Is Nothing Then
                PersistenciaEstadoTipoMantenimiento = New PersistenciaTipoEquipoMantenimiento()
            End If
            Return PersistenciaEstadoTipoMantenimiento
        End Get
    End Property


    Public ReadOnly Property CapaPersistenciaUnidadReferenciaMantenimiento() As IPersistenciaBase(Of UnidadReferenciaMantenimiento)
        Get
            If persistenciaUnidadReferenciaMantenimiento Is Nothing Then
                persistenciaUnidadReferenciaMantenimiento = New PersistenciaUnidadReferenciaMantenimiento()
            End If
            Return persistenciaUnidadReferenciaMantenimiento
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaSubSistemas() As IPersistenciaBase(Of SubSistemas)
        Get
            If persistenciaSubSistemas Is Nothing Then
                persistenciaSubSistemas = New PersistenciaSubsistemas()
            End If
            Return persistenciaSubSistemas
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaTipoSubsistemas() As IPersistenciaBase(Of TipoSubsistemas)
        Get
            If persistenciaTipoSubsistemas Is Nothing Then
                persistenciaTipoSubsistemas = New PersistenciaTipoSubsistemas()
            End If
            Return persistenciaTipoSubsistemas
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaSistemas() As IPersistenciaBase(Of Sistemas)
        Get
            If persistenciaSistemas Is Nothing Then
                persistenciaSistemas = New PersistenciaSistemas()
            End If
            Return persistenciaSistemas
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaLineaMantenimiento() As IPersistenciaBase(Of LineaMantenimiento)
        Get
            If persistenciaLineaMantenimiento Is Nothing Then
                persistenciaLineaMantenimiento = New PersistenciaLineaMantenimiento()
            End If
            Return persistenciaLineaMantenimiento
        End Get
    End Property

    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para empresas
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaEmpresas() As IPersistenciaBase(Of Empresas)
        Get
            If persistenciaempresas Is Nothing Then
                persistenciaempresas = New PersistenciaEmpresas()
            End If
            Return persistenciaempresas
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaSolicitudAnticipo() As IPersistenciaBase(Of EncabezadoSolicitudAnticipo)
        Get
            If persistenciaSolicitudAnticipo Is Nothing Then
                persistenciaSolicitudAnticipo = New PersistenciaSolicitudesAnticipo()
            End If
            Return persistenciaSolicitudAnticipo
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Usuarios
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaOficinas() As IPersistenciaBase(Of Oficinas)
        Get
            If persistenciaoficinas Is Nothing Then
                persistenciaoficinas = New PersistenciaOficinas()
            End If
            Return persistenciaoficinas
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Usuarios
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaZonas() As IPersistenciaBase(Of Zonas)
        Get
            If persistenciazonas Is Nothing Then
                persistenciazonas = New PersistenciaZonas()
            End If
            Return persistenciazonas
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Usuarios
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaSitiosTerceroCliente() As IPersistenciaBase(Of SitiosTerceroCliente)
        Get
            If persistenciasitiostercerocliente Is Nothing Then
                persistenciasitiostercerocliente = New PersistenciaSitiosTerceroCliente()
            End If
            Return persistenciasitiostercerocliente
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Monedas
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaMonedas() As IPersistenciaBase(Of Monedas)
        Get
            If persistenciamonedas Is Nothing Then
                persistenciamonedas = New PersistenciaMonedas()
            End If
            Return persistenciamonedas
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaTRM() As IPersistenciaBase(Of TRM)
        Get
            If persistenciatrm Is Nothing Then
                persistenciatrm = New PersistenciaTRM()
            End If
            Return persistenciatrm
        End Get
    End Property

    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Paises
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaPaises() As IPersistenciaBase(Of Paises)
        Get
            If persistenciapaises Is Nothing Then
                persistenciapaises = New PersistenciaPaises()
            End If
            Return persistenciapaises
        End Get
    End Property

    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para terceros
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaTerceros() As IPersistenciaBase(Of Terceros)
        Get
            If PersistenciaTerceros Is Nothing Then
                PersistenciaTerceros = New PersistenciaTerceros()
            End If
            Return PersistenciaTerceros
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Catalogos
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaCatalogos() As IPersistenciaBase(Of Catalogos)
        Get
            If persistenciacatalogos Is Nothing Then
                persistenciacatalogos = New PersistenciaCatalogos()
            End If
            Return persistenciacatalogos
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Notificaciones
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaNotificaciones() As IPersistenciaBase(Of Notificaciones)
        Get
            If persistencianotificaciones Is Nothing Then
                persistencianotificaciones = New PersistenciaNotificaciones()
            End If
            Return persistencianotificaciones
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para ValorCatalogos
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaUnidadMedida() As IPersistenciaBase(Of UnidadMedida)
        Get
            If persistenciaunidadmedida Is Nothing Then
                persistenciaunidadmedida = New PersistenciaUnidadMedida()
            End If
            Return persistenciaunidadmedida
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para ValorCatalogos
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaUnidadEmpaque() As IPersistenciaBase(Of UnidadEmpaque)
        Get
            If persistenciaunidadempaque Is Nothing Then
                persistenciaunidadempaque = New PersistenciaUnidadEmpaque()
            End If
            Return persistenciaunidadempaque
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para ValorCatalogos
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaProductosMinisterioTransporte() As IPersistenciaBase(Of ProductosMinisterioTransporte)
        Get
            If persistenciaproductosministeriotransporte Is Nothing Then
                persistenciaproductosministeriotransporte = New PersistenciaProductosMinisterioTransporte()
            End If
            Return persistenciaproductosministeriotransporte
        End Get
    End Property

    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para ValorCatalogos
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaPuertosPaises() As IPersistenciaBase(Of PuertosPaises)
        Get
            If persistenciapuertospaises Is Nothing Then
                persistenciapuertospaises = New PersistenciaPuertosPaises()
            End If
            Return persistenciapuertospaises
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para ValorCatalogos
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaValorCatalogos() As IPersistenciaBase(Of ValorCatalogos)
        Get
            If persistenciavalorcatalogos Is Nothing Then
                persistenciavalorcatalogos = New PersistenciaValorCatalogos()
            End If
            Return persistenciavalorcatalogos
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Departamentos
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaDepartamentos() As IPersistenciaBase(Of Departamentos)
        Get
            If persistenciadepartamentos Is Nothing Then
                persistenciadepartamentos = New PersistenciaDepartamentos()
            End If
            Return persistenciadepartamentos
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Ciudades
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaImpuestos() As IPersistenciaBase(Of Impuestos)
        Get
            If persistenciaimpuestos Is Nothing Then
                persistenciaimpuestos = New PersistenciaImpuestos()
            End If
            Return persistenciaimpuestos
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Ciudades
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaCiudades() As IPersistenciaBase(Of Ciudades)
        Get
            If persistenciaciudades Is Nothing Then
                persistenciaciudades = New PersistenciaCiudades()
            End If
            Return persistenciaciudades
        End Get
    End Property



    Public ReadOnly Property CapaPersistenciaCodigosDaneCiudades() As IPersistenciaBase(Of CodigoDaneCiudades)
        Get
            If persistenciacodigosdaneciudades Is Nothing Then
                persistenciacodigosdaneciudades = New PersistenciaCodigosDaneCiudades()
            End If
            Return persistenciacodigosdaneciudades
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Ciudades
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaProductoTransportados() As IPersistenciaBase(Of ProductoTransportados)
        Get
            If persistenciaproductotransportados Is Nothing Then
                persistenciaproductotransportados = New PersistenciaProductoTransportados()
            End If
            Return persistenciaproductotransportados
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Ciudades
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaTipoDocumentos() As IPersistenciaBase(Of TipoDocumentos)
        Get
            If persistenciatipodocumentos Is Nothing Then
                persistenciatipodocumentos = New PersistenciaTipoDocumentos()
            End If
            Return persistenciatipodocumentos
        End Get
    End Property

    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Impuesto tipo Documentos
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaFormularioInspecciones() As IPersistenciaBase(Of FormularioInspecciones)
        Get
            If persistenciaformularioinspecciones Is Nothing Then
                persistenciaformularioinspecciones = New PersistenciaFormularioInspecciones()
            End If
            Return persistenciaformularioinspecciones
        End Get
    End Property

    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Impuesto tipo Documentos
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaImpuestoTipoDocumentos() As IPersistenciaBase(Of ImpuestoTipoDocumentos)
        Get
            If persistenciaimpuestotipodocumentos Is Nothing Then
                persistenciaimpuestotipodocumentos = New PersistenciaImpuestoTipoDocumentos()
            End If
            Return persistenciaimpuestotipodocumentos
        End Get
    End Property

    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para ConfiguracionCatalogos
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaConfiguracionCatalogos() As IPersistenciaBase(Of ConfiguracionCatalogos)
        Get
            If persistenciaconfiguracioncatalogos Is Nothing Then
                persistenciaconfiguracioncatalogos = New PersistenciaConfiguracionCatalogos()
            End If
            Return persistenciaconfiguracioncatalogos
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para ConfiguracionCatalogos
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaMarcaVehiculos() As IPersistenciaBase(Of MarcaVehiculos)
        Get
            If persistenciamarcavehiculos Is Nothing Then
                persistenciamarcavehiculos = New PersistenciaMarcaVehiculos()
            End If
            Return persistenciamarcavehiculos
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para ConfiguracionCatalogos
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaLineaVehiculos() As IPersistenciaBase(Of LineaVehiculos)
        Get
            If persistencialineavehiculos Is Nothing Then
                persistencialineavehiculos = New PersistenciaLineaVehiculos()
            End If
            Return persistencialineavehiculos
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para ConfiguracionCatalogos
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaSitiosCargueDescargue() As IPersistenciaBase(Of SitiosCargueDescargue)
        Get
            If persistenciasitioscarguedescargue Is Nothing Then
                persistenciasitioscarguedescargue = New PersistenciaSitiosCargueDescargue()
            End If
            Return persistenciasitioscarguedescargue
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para ConfiguracionCatalogos
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaColorVehiculos() As IPersistenciaBase(Of ColorVehiculos)
        Get
            If persistenciacolorvehiculos Is Nothing Then
                persistenciacolorvehiculos = New PersistenciaColorVehiculos()
            End If
            Return persistenciacolorvehiculos
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaRendimientoGalonCombustible() As IPersistenciaBase(Of RendimientoGalonCombustible)
        Get
            If persistenciaRendimientoGalonCombustible Is Nothing Then
                persistenciaRendimientoGalonCombustible = New PersistenciaRendimientoGalonCombustible()
            End If
            Return persistenciaRendimientoGalonCombustible
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para ConfiguracionCatalogos
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaRutas() As IPersistenciaBase(Of Rutas)
        Get
            If persistenciarutas Is Nothing Then
                persistenciarutas = New PersistenciaRutas()
            End If
            Return persistenciarutas
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para ConfiguracionCatalogos
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaPrecintos() As IPersistenciaBase(Of Precintos)
        Get
            If PersistenciaPrecintos Is Nothing Then
                PersistenciaPrecintos = New PersistenciaPrecintos()
            End If
            Return PersistenciaPrecintos
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaPuestoControles() As IPersistenciaBase(Of PuestoControles)
        Get
            If persistenciapuestocontroles Is Nothing Then
                persistenciapuestocontroles = New PersistenciaPuestoControles()
            End If
            Return persistenciapuestocontroles
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Peajes
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaPeajes() As IPersistenciaBase(Of Peajes)
        Get
            If persistenciapeajes Is Nothing Then
                persistenciapeajes = New PersistenciaPeajes()
            End If
            Return persistenciapeajes
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Cajas
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaCajas() As IPersistenciaBase(Of Cajas)
        Get
            If persistenciacajas Is Nothing Then
                persistenciacajas = New PersistenciaCajas()
            End If
            Return persistenciacajas
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Cajas
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaConceptosLiquidacion() As IPersistenciaBase(Of ConceptoLiquidacionPlanillaDespacho)
        Get
            If PersistenciaConceptoLiquidacion Is Nothing Then
                PersistenciaConceptoLiquidacion = New PersistenciaConceptoLiquidacion()
            End If
            Return PersistenciaConceptoLiquidacion
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Cajas
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaConceptosFacturacion() As IPersistenciaBase(Of ConceptoFacturacion)
        Get
            If PersistenciaConceptoFacturacion Is Nothing Then
                PersistenciaConceptoFacturacion = New PersistenciaConceptoFacturacion()
            End If
            Return PersistenciaConceptoFacturacion
        End Get
    End Property

    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Cajas
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaConceptoGastos() As IPersistenciaBase(Of ConceptoGastos)
        Get
            If PersistenciaConceptoGastos Is Nothing Then
                PersistenciaConceptoGastos = New PersistenciaConceptoGastos()
            End If
            Return PersistenciaConceptoGastos
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Impuestos Facturas
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaImpuestoFacturas() As IPersistenciaBase(Of ImpuestoFacturas)
        Get
            If PersistenciaImpuestoFacturas Is Nothing Then
                PersistenciaImpuestoFacturas = New PersistenciaImpuestoFacturas()
            End If
            Return PersistenciaImpuestoFacturas
        End Get
    End Property

    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Notas Facturas
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaNotasFacturas() As IPersistenciaBase(Of NotasFacturas)
        Get
            If PersistenciaNotasFacturas Is Nothing Then
                PersistenciaNotasFacturas = New PersistenciaNotasFacturas()
            End If
            Return PersistenciaNotasFacturas
        End Get
    End Property

    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Cajas
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaConceptosNotasFacturas() As IPersistenciaBase(Of ConceptoNotasFacturas)
        Get
            If PersistenciaConceptoNotasFacturas Is Nothing Then
                PersistenciaConceptoNotasFacturas = New PersistenciaConceptoNotasFacturas()
            End If
            Return PersistenciaConceptoNotasFacturas
        End Get
    End Property

    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para EncabezadoConceptoContables
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaEncabezadoConceptoContables() As IPersistenciaBase(Of EncabezadoConceptoContables)
        Get
            If persistenciaencabezadoconceptocontables Is Nothing Then
                persistenciaencabezadoconceptocontables = New PersistenciaEncabezadoConceptoContables()
            End If
            Return persistenciaencabezadoconceptocontables
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para DetalleConceptoContables
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaDetalleConceptoContables() As IPersistenciaBase(Of DetalleConceptoContables)
        Get
            If persistenciadetalleconceptocontables Is Nothing Then
                persistenciadetalleconceptocontables = New PersistenciaDetalleConceptoContables()
            End If
            Return persistenciadetalleconceptocontables
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para EncabezadoParametrizacionContables
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaEncabezadoParametrizacionContables() As IPersistenciaBase(Of EncabezadoParametrizacionContables)
        Get
            If persistenciaencabezadoparametrizacioncontables Is Nothing Then
                persistenciaencabezadoparametrizacioncontables = New PersistenciaEncabezadoParametrizacionContables()
            End If
            Return persistenciaencabezadoparametrizacioncontables
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para DetalleParametrizacionContables
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaDetalleParametrizacionContables() As IPersistenciaBase(Of DetalleParametrizacionContables)
        Get
            If persistenciadetalleparametrizacioncontables Is Nothing Then
                persistenciadetalleparametrizacioncontables = New PersistenciaDetalleParametrizacionContables()
            End If
            Return persistenciadetalleparametrizacioncontables
        End Get
    End Property

    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para ChequeraCuentaBancarias
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaChequeraCuentaBancarias() As IPersistenciaBase(Of ChequeraCuentaBancarias)
        Get
            If persistenciachequeracuentabancarias Is Nothing Then
                persistenciachequeracuentabancarias = New PersistenciaChequeraCuentaBancarias()
            End If
            Return persistenciachequeracuentabancarias
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaFotosVehiculos() As IPersistenciaBase(Of FotosVehiculos)
        Get
            If persistenciafotosvehiculos Is Nothing Then
                persistenciafotosvehiculos = New PersistenciaFotosVehiculos()
            End If
            Return persistenciafotosvehiculos
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaVehiculos() As IPersistenciaBase(Of Vehiculos)
        Get
            If persistenciavehiculos Is Nothing Then
                persistenciavehiculos = New PersistenciaVehiculos()
            End If
            Return persistenciavehiculos
        End Get
    End Property



    Public ReadOnly Property CapaPersistenciaVehiculosListaNegra() As IPersistenciaBase(Of Vehiculos)
        Get
            If persistenciaVehiculosListaNegra Is Nothing Then
                persistenciaVehiculosListaNegra = New PersistenciaVehiculosListaNegra()
            End If
            Return persistenciaVehiculosListaNegra
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaMarcaSemirremolques() As IPersistenciaBase(Of MarcaSemirremolques)
        Get
            If persistenciamarcasemirremolsques Is Nothing Then
                persistenciamarcasemirremolsques = New PersistenciaMarcaSemirremolques()
            End If
            Return persistenciamarcasemirremolsques
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaSemirremolques() As IPersistenciaBase(Of Semirremolques)
        Get
            If Persistenciasemirremolques Is Nothing Then
                Persistenciasemirremolques = New PersistenciaSemirremolques()
            End If
            Return Persistenciasemirremolques
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaDocumentos() As IPersistenciaBase(Of Documentos)
        Get
            If persistenciadocumentos Is Nothing Then
                persistenciadocumentos = New PersistenciaDocumentos()
            End If
            Return persistenciadocumentos
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaConfiguracionGestionDocumentos() As IPersistenciaBase(Of ConfiguracionGestionDocumentos)
        Get
            If persistenciaconfiguraciongestiondocumentos Is Nothing Then
                persistenciaconfiguraciongestiondocumentos = New PersistenciaGestionDocumentos()
            End If
            Return persistenciaconfiguraciongestiondocumentos
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaAlmacenes() As IPersistenciaBase(Of Almacenes)
        Get
            If persistenciaalmacenes Is Nothing Then
                persistenciaalmacenes = New PersistenciaAlmacenes()
            End If
            Return persistenciaalmacenes
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaDetalleUbicacionAlmacenes() As IPersistenciaBase(Of DetalleUbicacionAlmacenes)
        Get
            If persistenciadetalleubicacionalmacenes Is Nothing Then
                persistenciadetalleubicacionalmacenes = New PersistenciaDetalleUbicacionAlmacenes()
            End If
            Return persistenciadetalleubicacionalmacenes
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaEncabezadoUbicacionAlmacenes() As IPersistenciaBase(Of EncabezadoUbicacionAlmacenes)
        Get
            If persistenciaencabezadoubicacionalmacenes Is Nothing Then
                persistenciaencabezadoubicacionalmacenes = New PersistenciaEncabezadoUbicacionAlmacenes()
            End If
            Return persistenciaencabezadoubicacionalmacenes
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaGrupoReferencias() As IPersistenciaBase(Of GrupoReferencias)
        Get
            If persistenciagruporeferencias Is Nothing Then
                persistenciagruporeferencias = New PersistenciaGrupoReferencias()
            End If
            Return persistenciagruporeferencias
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaReferenciaAlmacenes() As IPersistenciaBase(Of ReferenciaAlmacenes)
        Get
            If persistenciareferenciaalmacenes Is Nothing Then
                persistenciareferenciaalmacenes = New PersistenciaReferenciaAlmacenes()
            End If
            Return persistenciareferenciaalmacenes
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaReferenciaProveedores() As IPersistenciaBase(Of ReferenciaProveedores)
        Get
            If persistenciareferenciaproveedores Is Nothing Then
                persistenciareferenciaproveedores = New PersistenciaReferenciaProveedores()
            End If
            Return persistenciareferenciaproveedores
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaReferencias() As IPersistenciaBase(Of Referencias)
        Get
            If persistenciareferencias Is Nothing Then
                persistenciareferencias = New PersistenciaReferencias()
            End If
            Return persistenciareferencias
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaUnidadMedidaReferencias() As IPersistenciaBase(Of UnidadMedidaReferencias)
        Get
            If persistenciaunidadmedidareferencias Is Nothing Then
                persistenciaunidadmedidareferencias = New PersistenciaUnidadMedidaReferencias()
            End If
            Return persistenciaunidadmedidareferencias
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaUnidadEmpaqueReferencias() As IPersistenciaBase(Of UnidadEmpaqueReferencias)
        Get
            If persistenciaunidadempaquereferencias Is Nothing Then
                persistenciaunidadempaquereferencias = New PersistenciaUnidadEmpaqueReferencias()
            End If
            Return persistenciaunidadempaquereferencias
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaManifiestos() As IPersistenciaBase(Of Manifiesto)
        Get
            If persistenciaManifiesto Is Nothing Then
                persistenciaManifiesto = New PersistenciaManifiestos()
            End If
            Return persistenciaManifiesto
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaConceptoVentas() As IPersistenciaBase(Of ConceptoVentas)
        Get
            If persistenciaconceptoventas Is Nothing Then
                persistenciaconceptoventas = New PersistenciaConceptoVentas()
            End If
            Return persistenciaconceptoventas
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaLineaNegocioTransporteCarga() As IPersistenciaBase(Of LineaNegocioTransporteCarga)
        Get
            If persistencialineanegociotransportecarga Is Nothing Then
                persistencialineanegociotransportecarga = New PersistenciaLineaNegocioTransporteCarga()
            End If
            Return persistencialineanegociotransportecarga
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaTarifaTransporteCarga() As IPersistenciaBase(Of TarifaTransporteCarga)
        Get
            If persistenciatarifatransportecarga Is Nothing Then
                persistenciatarifatransportecarga = New PersistenciaTarifaTransporteCarga()
            End If
            Return persistenciatarifatransportecarga
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaTipoLineaNegocioCarga() As IPersistenciaBase(Of TipoLineaNegocioCarga)
        Get
            If persistenciatipolineanegociocarga Is Nothing Then
                persistenciatipolineanegociocarga = New PersistenciaTipoLineaNegocioCarga()
            End If
            Return persistenciatipolineanegociocarga
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaTipoTarifaTransporteCarga() As IPersistenciaBase(Of TipoTarifaTransporteCarga)
        Get
            If persistenciatipotarifatransportecarga Is Nothing Then
                persistenciatipotarifatransportecarga = New PersistenciaTipoTarifaTransporteCarga()
            End If
            Return persistenciatipotarifatransportecarga
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaReporteMinisterio() As IPersistenciaBase(Of ReporteMinisterio)
        Get
            If persistenciareporteministerio Is Nothing Then
                persistenciareporteministerio = New PersistenciaReporteMinisterio()
            End If
            Return persistenciareporteministerio
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaNovedadesDespachos() As IPersistenciaBase(Of NovedadesDespachos)
        Get
            If persistencianovedadesdespachos Is Nothing Then
                persistencianovedadesdespachos = New PersistenciaNovedadesDespachos()
            End If
            Return persistencianovedadesdespachos
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaNovedadesConceptos() As IPersistenciaBase(Of NovedadesConceptos)
        Get
            If persistencianovedadesconceptos Is Nothing Then
                persistencianovedadesconceptos = New PersistenciaNovedadesConceptos()
            End If
            Return persistencianovedadesconceptos
        End Get
    End Property




#End Region

#Region "Atributos Gesphone"
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Detalle Seguimiento Vehiculos
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaEncabezadoComprobantesContables() As IPersistenciaBase(Of EncabezadoComprobantesContables)
        Get
            If persistenciaencabezadocomprobantescontables Is Nothing Then
                persistenciaencabezadocomprobantescontables = New PersistenciaEncabezadoComprobantesContables()
            End If
            Return persistenciaencabezadocomprobantescontables
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Detalle Seguimiento Vehiculos
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaDetalleSeguimientoVehiculos() As IPersistenciaBase(Of DetalleSeguimientoVehiculos)
        Get
            If persistenciadetalleseguimientovehiculos Is Nothing Then
                persistenciadetalleseguimientovehiculos = New PersistenciaDetalleSeguimientoVehiculos()
            End If
            Return persistenciadetalleseguimientovehiculos
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaAutorizaciones() As IPersistenciaBase(Of Autorizaciones)
        Get
            If persistenciaAutorizaciones Is Nothing Then
                persistenciaAutorizaciones = New PersistenciaAutorizaciones()
            End If
            Return persistenciaAutorizaciones
        End Get
    End Property


    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Inspeccion Preoperacional
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaInspeccionPreoperacional() As IPersistenciaBase(Of InspeccionPreoperacional)
        Get
            If persistenciainspeccionpreoperacional Is Nothing Then
                persistenciainspeccionpreoperacional = New PersistenciaInspeccionPreoperacional()
            End If
            Return persistenciainspeccionpreoperacional
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Inspeccion Preoperacional
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaDocumentoInspeccionPreoperacional() As IPersistenciaBase(Of DocumentoInspeccionPreoperacional)
        Get
            If persistenciadocumentoinspeccionpreoperacional Is Nothing Then
                persistenciadocumentoinspeccionpreoperacional = New PersistenciaDocumentoInspeccionPreoperacional()
            End If
            Return persistenciadocumentoinspeccionpreoperacional
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Detalle Seguimiento Vehiculos
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaDetalleSeguimientoCargaVehiculos() As IPersistenciaBase(Of DetalleSeguimientoCargaVehiculos)
        Get
            If persistenciadetalleseguimientocargavehiculos Is Nothing Then
                persistenciadetalleseguimientocargavehiculos = New PersistenciaDetalleSeguimientoCargaVehiculos()
            End If
            Return persistenciadetalleseguimientocargavehiculos
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos paraDetalleCheckListCargueDescargues
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaDetalleCheckListCargueDescargues() As IPersistenciaBase(Of DetalleCheckListCargueDescargues)
        Get
            If persistenciadetallechecklistcarguedescargues Is Nothing Then
                persistenciadetallechecklistcarguedescargues = New PersistenciaDetalleCheckListCargueDescargues()
            End If
            Return persistenciadetallechecklistcarguedescargues
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para DetalleCheckListVehiculos
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaDetalleCheckListVehiculos() As IPersistenciaBase(Of DetalleCheckListVehiculos)
        Get
            If persistenciadetallechecklistvehiculos Is Nothing Then
                persistenciadetallechecklistvehiculos = New PersistenciaDetalleCheckListVehiculos()
            End If
            Return persistenciadetallechecklistvehiculos
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para EncabezadoCheckListCargueDescargues
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaEncabezadoCheckListCargueDescargues() As IPersistenciaBase(Of EncabezadoCheckListCargueDescargues)
        Get
            If persistenciaencabezadochecklistcarguedescargues Is Nothing Then
                persistenciaencabezadochecklistcarguedescargues = New PersistenciaEncabezadoCheckListCargueDescargues()
            End If
            Return persistenciaencabezadochecklistcarguedescargues
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para EncabezadoCheckListVehiculos
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaEncabezadoCheckListVehiculos() As IPersistenciaBase(Of EncabezadoCheckListVehiculos)
        Get
            If persistenciaencabezadochecklistvehiculos Is Nothing Then
                persistenciaencabezadochecklistvehiculos = New PersistenciaEncabezadoCheckListVehiculos()
            End If
            Return persistenciaencabezadochecklistvehiculos
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para ItemCheckListVehiculos
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaItemCheckListVehiculos() As IPersistenciaBase(Of ItemCheckListVehiculos)
        Get
            If persistenciaitemchecklistvehiculos Is Nothing Then
                persistenciaitemchecklistvehiculos = New PersistenciaItemCheckListVehiculos()
            End If
            Return persistenciaitemchecklistvehiculos
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para ItemListCargueDescargues
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaItemListCargueDescargues() As IPersistenciaBase(Of ItemListCargueDescargues)
        Get
            If persistenciaitemlistcarguedescargues Is Nothing Then
                persistenciaitemlistcarguedescargues = New PersistenciaItemListCargueDescargues()
            End If
            Return persistenciaitemlistcarguedescargues
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para ItemListCargueDescargues
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaNovedadTransito() As IPersistenciaBase(Of NovedadTransito)
        Get
            If PersistenciaNovedadTransito Is Nothing Then
                PersistenciaNovedadTransito = New PersistenciaNovedadTransito()
            End If
            Return PersistenciaNovedadTransito
        End Get
    End Property

    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Detalle Seguimiento Vehiculos
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaInterfazContablePSL() As IPersistenciaBase(Of InterfazContablePSL)
        Get
            If persistenciainterfazcontablepsl Is Nothing Then
                persistenciainterfazcontablepsl = New PersistenciaInterfazContablePSL()
            End If
            Return persistenciainterfazcontablepsl
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaCierreContableDocumentos() As IPersistenciaBase(Of CierreContableDocumentos)
        Get
            If persistenciacierrecontabledocumentos Is Nothing Then
                persistenciacierrecontabledocumentos = New PersistenciaCierreContableDocumentos()
            End If
            Return persistenciacierrecontabledocumentos
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaCombinacionContratoVinculacion() As IPersistenciaBase(Of CombinacionContratoVinculacion)
        Get
            If persistenciaCombinacionContratoVinculacion Is Nothing Then
                persistenciaCombinacionContratoVinculacion = New PersistenciaCombinacionContratoVinculacion()
            End If
            Return persistenciaCombinacionContratoVinculacion
        End Get
    End Property

    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para SyncDetalleTarifarioVenta
    ''' </summary>
    'Public ReadOnly Property CapaPersistenciaSincronizacionApp() As IPersistenciaSincronizacionApp
    '    Get
    '        If PersistenciaSincronizacionApp Is Nothing Then
    '            PersistenciaSincronizacionApp = New PersistenciaSincronizacionApp()
    '        End If
    '        Return PersistenciaSincronizacionApp
    '    End Get
    'End Property


#End Region
#Region "Atributos Facturacion"

#End Region

#Region "Atributos Fidelizacion Tranportadores"
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Fidelizacion.
    ''' </summary>
    Dim persistenciaPuntosVehiculos As IPersistenciaBase(Of PuntosVehiculos)
    Public ReadOnly Property CapaPersistenciaPuntosVehiculos() As IPersistenciaBase(Of PuntosVehiculos)
        Get
            If persistenciaPuntosVehiculos Is Nothing Then
                persistenciaPuntosVehiculos = New PersistenciaPuntosVehiculos()
            End If
            Return persistenciaPuntosVehiculos
        End Get
    End Property

    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Fidelizacion - Causaciones.
    ''' </summary>
    Dim persistenciaDetalleCausalesPuntosVehiculos As IPersistenciaBase(Of DetalleCausalesPuntosVehiculos)
    Public ReadOnly Property CapaPersistenciaDetalleCausalesPuntosVehiculos() As IPersistenciaBase(Of DetalleCausalesPuntosVehiculos)
        Get
            If persistenciaDetalleCausalesPuntosVehiculos Is Nothing Then
                persistenciaDetalleCausalesPuntosVehiculos = New PersistenciaDetalleCausalesPuntosVehiculos()
            End If
            Return persistenciaDetalleCausalesPuntosVehiculos
        End Get
    End Property

    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Fidelizacion - Despachos.
    ''' </summary>
    Dim persistenciaDetalleDespachosPuntosVehiculos As IPersistenciaBase(Of DetalleDespachosPuntosVehiculos)
    Public ReadOnly Property CapaPersistenciaDetalleDespachosPuntosVehiculos() As IPersistenciaBase(Of DetalleDespachosPuntosVehiculos)
        Get
            If persistenciaDetalleDespachosPuntosVehiculos Is Nothing Then
                persistenciaDetalleDespachosPuntosVehiculos = New PersistenciaDetalleDespachosPuntosVehiculos()
            End If
            Return persistenciaDetalleDespachosPuntosVehiculos
        End Get
    End Property

#End Region
#Region "Atributos Despachos"
    Public ReadOnly Property CapaPersistenciaCargueMasivoGuias() As IPersistenciaBase(Of RemesaPaqueteria)
        Get
            If persistenciaCargueMasivoGuias Is Nothing Then
                persistenciaCargueMasivoGuias = New PersistenciaCargueMasivoGuias()
            End If
            Return persistenciaCargueMasivoGuias
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaGuiasPreimpresas() As IPersistenciaBase(Of GuiaPreimpresa)
        Get
            If persistenciaGuiasPreimpresas Is Nothing Then
                persistenciaGuiasPreimpresas = New PersistenciaGuiasPreimpresas()
            End If
            Return persistenciaGuiasPreimpresas
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaEtiquetasPreimpresas() As IPersistenciaBase(Of EtiquetaPreimpresa)
        Get
            If persistenciaEtiquetasPreimpresas Is Nothing Then
                persistenciaEtiquetasPreimpresas = New PersistenciaEtiquetasPreimpresas()
            End If
            Return persistenciaEtiquetasPreimpresas
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para ControlEntregas
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaEncabezadoSeguimientoVehiculos() As IPersistenciaBase(Of EncabezadoSeguimientoVehiculos)
        Get
            If persistenciaencabeadoseguimientovehiculos Is Nothing Then
                persistenciaencabeadoseguimientovehiculos = New PersistenciaEncabezadoSeguimientoVehiculos()
            End If
            Return persistenciaencabeadoseguimientovehiculos
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaRemesaPaqueteria() As IPersistenciaBase(Of RemesaPaqueteria)
        Get
            If persistenciaRemesaPaqueteria Is Nothing Then
                persistenciaRemesaPaqueteria = New PersistenciaRemesaPaqueteria()
            End If
            Return persistenciaRemesaPaqueteria
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaRecepcionGuias() As IPersistenciaBase(Of RemesaPaqueteria)
        Get
            If persistenciaRecepcionGuias Is Nothing Then
                persistenciaRecepcionGuias = New PersistenciaRecepcionGuiasOficina()
            End If
            Return persistenciaRecepcionGuias
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaPlanillaGuias() As IPersistenciaBase(Of PlanillaGuias)
        Get
            If persistenciaplanillaguias Is Nothing Then
                persistenciaplanillaguias = New PersistenciaPlanillaGuias()
            End If
            Return persistenciaplanillaguias
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaPlanillaPaqueteria() As IPersistenciaBase(Of PlanillaPaqueteria)
        Get
            If persistenciaplanillapaqueteria Is Nothing Then
                persistenciaplanillapaqueteria = New PersistenciaPlanillaPaqueteria()
            End If
            Return persistenciaplanillapaqueteria
        End Get
    End Property


    Public ReadOnly Property CapaPersistenciaLegalizarRecaudoRemesas() As IPersistenciaBase(Of LegalizarRecaudoRemesas)
        Get
            If persistencialegalizarecaudoremesas Is Nothing Then
                persistencialegalizarecaudoremesas = New PersistenciaLegalizarRecaudoRemesas()
            End If
            Return persistencialegalizarecaudoremesas
        End Get
    End Property


    Public ReadOnly Property CapaPersistenciaPlanillaDespachos() As IPersistenciaBase(Of PlanillaDespachos)
        Get
            If persistenciaplanilladespachos Is Nothing Then
                persistenciaplanilladespachos = New PersistenciaPlanillaDespachos()
            End If
            Return persistenciaplanilladespachos
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaPlanillaEntregadas() As IPersistenciaBase(Of PlanillaEntregadas)
        Get
            If PersistenciaPlanillaEntregadas Is Nothing Then
                PersistenciaPlanillaEntregadas = New PersistenciaPlanillaEntregadas()
            End If
            Return PersistenciaPlanillaEntregadas
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaDetalleNovedadesDespachos() As IPersistenciaBase(Of DetalleNovedadesDespachos)
        Get
            If Persistenciadetallenovedadesdespachos Is Nothing Then
                Persistenciadetallenovedadesdespachos = New PersistenciaDetalleNovedadesDespachos()
            End If
            Return Persistenciadetallenovedadesdespachos
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaDetalleDistribucionRemesas() As IPersistenciaBase(Of DetalleDistribucionRemesas)
        Get
            If Persistenciadetalledistribucionremesas Is Nothing Then
                Persistenciadetalledistribucionremesas = New PersistenciaDetalleDistribucionRemesas()
            End If
            Return Persistenciadetalledistribucionremesas
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaDetallePlanillaDespachos() As IPersistenciaBase(Of DetallePlanillaDespachos)
        Get
            If Persistenciadetalleplanilladespachos Is Nothing Then
                Persistenciadetalleplanilladespachos = New PersistenciaDetallePlanillaDespachos()
            End If
            Return Persistenciadetalleplanilladespachos
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaRecolecciones() As IPersistenciaBase(Of Recolecciones)
        Get
            If persistenciarecolecciones Is Nothing Then
                persistenciarecolecciones = New PersistenciaRecolecciones()
            End If
            Return persistenciarecolecciones
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaPlanillaRecolecciones() As IPersistenciaBase(Of PlanillaRecolecciones)
        Get
            If persistenciareplanillacolecciones Is Nothing Then
                persistenciareplanillacolecciones = New PersistenciaPlanillaRecolecciones()
            End If
            Return persistenciareplanillacolecciones
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaRemesas() As IPersistenciaBase(Of Remesas)
        Get
            If persistenciaremesas Is Nothing Then
                persistenciaremesas = New PersistenciaRemesas()
            End If
            Return persistenciaremesas
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaOrdenCargue() As IPersistenciaBase(Of OrdenCargue)
        Get
            If persistenciaOrdenCargue Is Nothing Then
                persistenciaOrdenCargue = New PersistenciaOrdenCargue()
            End If
            Return persistenciaOrdenCargue
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaEnturnamiento() As IPersistenciaBase(Of Enturnamiento)
        Get
            If persistenciaEnturnamiento Is Nothing Then
                persistenciaEnturnamiento = New PersistenciaEnturnamiento()
            End If
            Return persistenciaEnturnamiento
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaCumplidos() As IPersistenciaBase(Of Cumplido)
        Get
            If persistenciacumplidos Is Nothing Then
                persistenciacumplidos = New PersistenciaCumplidos()
            End If
            Return persistenciacumplidos
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaLiquidaciones() As IPersistenciaBase(Of Liquidacion)
        Get
            If persistencialiquidaciones Is Nothing Then
                persistencialiquidaciones = New PersistenciaLiquidaciones()
            End If
            Return persistencialiquidaciones
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaLegalizacionGastos() As IPersistenciaBase(Of LegalizacionGastos)
        Get
            If persistenciaLegalizacionGastos Is Nothing Then
                persistenciaLegalizacionGastos = New PersistenciaLegalizacionGastos()
            End If
            Return persistenciaLegalizacionGastos
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaPlanillasDespachosOtrasEmpresas() As IPersistenciaBase(Of PlanillaDespachosOtrasEmpresas)
        Get
            If persistenciaPlanillaDespachosOtrasEmpresas Is Nothing Then
                persistenciaPlanillaDespachosOtrasEmpresas = New PersistenciaPlanillasDespachosOtrasEmpresas()
            End If
            Return persistenciaPlanillaDespachosOtrasEmpresas
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaPlanillasDespachosProveedores() As IPersistenciaBase(Of PlanillasDespachosProveedores)
        Get
            If persistenciaPlanillaDespachosProveedores Is Nothing Then
                persistenciaPlanillaDespachosProveedores = New PersistenciaPlanillasDespachosProveedores()
            End If
            Return persistenciaPlanillaDespachosProveedores
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaLiquidacionPlanillasDespachosProveedores() As IPersistenciaBase(Of LiquidacionPlanillasProveedores)
        Get
            If persistenciaLiquidacionPlanillaDespachosProveedores Is Nothing Then
                persistenciaLiquidacionPlanillaDespachosProveedores = New PersistenciaLiquidacionPlanillasProveedores()
            End If
            Return persistenciaLiquidacionPlanillaDespachosProveedores
        End Get
    End Property

#End Region

#Region "Atributos Seguridad Usuarios"
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Usuarios
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaUsuarios() As IPersistenciaBase(Of Usuarios)
        Get
            If persistenciausuarios Is Nothing Then
                persistenciausuarios = New PersistenciaUsuarios()
            End If
            Return persistenciausuarios
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Usuarios
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaUsuarioGrupoUsuarios() As IPersistenciaBase(Of UsuarioGrupoUsuarios)
        Get
            If persistenciausuariogrupousuarios Is Nothing Then
                persistenciausuariogrupousuarios = New PersistenciaUsuarioGrupoUsuarios()
            End If
            Return persistenciausuariogrupousuarios
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Usuarios
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaPermisoGrupoUsuarios() As IPersistenciaBase(Of PermisoGrupoUsuarios)
        Get
            If persistenciapermisogrupousuarios Is Nothing Then
                persistenciapermisogrupousuarios = New PersistenciaPermisoGrupoUsuarios()
            End If
            Return persistenciapermisogrupousuarios
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Usuarios
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaEstudioSeguridad() As IPersistenciaBase(Of EstudioSeguridad)
        Get
            If persistenciaestudioseguridad Is Nothing Then
                persistenciaestudioseguridad = New PersistenciaEstudioSeguridad()
            End If
            Return persistenciaestudioseguridad
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Usuarios
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaEstudioSeguridadDocumentos() As IPersistenciaBase(Of EstudioSeguridadDocumentos)
        Get
            If persistenciaestudioseguridaddocumentos Is Nothing Then
                persistenciaestudioseguridaddocumentos = New PersistenciaEstudioSeguridadDocumentos()
            End If
            Return persistenciaestudioseguridaddocumentos
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Usuarios
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaGestionDocumentalDocumentos() As IPersistenciaBase(Of GestionDocumentalDocumentos)
        Get
            If persistenciagestiondocumentaldocumentos Is Nothing Then
                persistenciagestiondocumentaldocumentos = New PersistenciaGestionDocumentalDocumentos()
            End If
            Return persistenciagestiondocumentaldocumentos
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Usuarios
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaTipoDocumentoEstudioSeguridad() As IPersistenciaBase(Of TipoDocumentoEstudioSeguridad)
        Get
            If persistenciatipodocumentoestudioseguridad Is Nothing Then
                persistenciatipodocumentoestudioseguridad = New PersistenciaTipoDocumentoEstudioSeguridad()
            End If
            Return persistenciatipodocumentoestudioseguridad
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Usuarios
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaEntidadOrigenEstudioSeguridad() As IPersistenciaBase(Of EntidadOrigenEstudioSeguridad)
        Get
            If persistenciaentidadorigenestudioseguridad Is Nothing Then
                persistenciaentidadorigenestudioseguridad = New PersistenciaEntidadOrigenEstudioSeguridad()
            End If
            Return persistenciaentidadorigenestudioseguridad
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para Usuarios
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaGrupoUsuarios() As IPersistenciaBase(Of GrupoUsuarios)
        Get
            If persistenciagrupousuarios Is Nothing Then
                persistenciagrupousuarios = New PersistenciaGrupoUsuarios()
            End If
            Return persistenciagrupousuarios
        End Get
    End Property

#End Region

#Region "Atributos Tesoreria"
    Dim persistenciaencabezadodocumentocuentas As IPersistenciaBase(Of EncabezadoDocumentoCuentas)
    Dim persistenciadetalledocumentoComprobantes As IPersistenciaBase(Of EncabezadoDocumentoComprobantes)
    Dim persistenciapodetallecomprobantes As IPersistenciaBase(Of DetalleDocumentoComprobantes)
    Dim persistenciaencabezadoCausaciones As IPersistenciaBase(Of EncabezadoDocumentoCausaciones)
    Dim persistenciadetallecausaciones As IPersistenciaBase(Of DetalleDocumentoCausaciones)
    Dim persistenciadetalledocumentocuentacomprobantes As IPersistenciaBase(Of DetalleDocumentoCuentaComprobantes)


    Public ReadOnly Property CapaPersistenciaEncabezadoDocumentoCuentas() As IPersistenciaBase(Of EncabezadoDocumentoCuentas)
        Get
            If persistenciaencabezadodocumentocuentas Is Nothing Then
                persistenciaencabezadodocumentocuentas = New PersistenciaEncabezadoDocumentoCuentas()
            End If
            Return persistenciaencabezadodocumentocuentas
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaEncabezadoDocumentoComprobantes() As IPersistenciaBase(Of EncabezadoDocumentoComprobantes)
        Get
            If persistenciadetalledocumentoComprobantes Is Nothing Then
                persistenciadetalledocumentoComprobantes = New PersistenciaEncabezadoDocumentoComprobantes()
            End If
            Return persistenciadetalledocumentoComprobantes
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaDetalleDocumentoComprobantes() As IPersistenciaBase(Of DetalleDocumentoComprobantes)
        Get
            If persistenciapodetallecomprobantes Is Nothing Then
                persistenciapodetallecomprobantes = New PersistenciaDetalleDocumentoComprobantes()
            End If
            Return persistenciapodetallecomprobantes
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaEncabezadoDocumentoCausaciones() As IPersistenciaBase(Of EncabezadoDocumentoCausaciones)
        Get
            If persistenciaencabezadoCausaciones Is Nothing Then
                persistenciaencabezadoCausaciones = New PersistenciaEncabezadoDocumentoCausaciones()
            End If
            Return persistenciaencabezadoCausaciones
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaDetalleDocumentoCausaciones() As IPersistenciaBase(Of DetalleDocumentoCausaciones)
        Get
            If persistenciadetallecausaciones Is Nothing Then
                persistenciadetallecausaciones = New PersistenciaDetalleDocumentoCausaciones()
            End If
            Return persistenciadetallecausaciones
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaDetalleDocumentoCuentaComprobantes() As IPersistenciaBase(Of DetalleDocumentoCuentaComprobantes)
        Get
            If persistenciadetalledocumentocuentacomprobantes Is Nothing Then
                persistenciadetalledocumentocuentacomprobantes = New PersistenciaDetalleDocumentoCuentaComprobantes()
            End If
            Return persistenciadetalledocumentocuentacomprobantes
        End Get
    End Property


#End Region

#Region "Atributos Servicio Cliente"
    Dim persistenciadetalleconceptosolicitudordenservicios As IPersistenciaBase(Of DetalleConceptoSolicitudOrdenServicios)
    Dim persistenciadetallepolizassolicitudordenservicios As IPersistenciaBase(Of DetallePolizasSolicitudOrdenServicios)
    Dim persistenciadetallesolicitudordenservicios As IPersistenciaBase(Of DetalleSolicitudOrdenServicios)
    Dim persistenciadetalledespachosolicitudordenservicios As IPersistenciaBase(Of DetalleDespachoSolicitudOrdenServicios)
    Dim persistenciaencabezadosolicitudordenservicios As IPersistenciaBase(Of EncabezadoSolicitudOrdenServicios)
    Dim persistenciaencabezadoprogramacionordenservicios As IPersistenciaBase(Of EncabezadoProgramacionOrdenServicios)
    Dim persistenciaimportacionexportacionsolicitudordenservicios As IPersistenciaBase(Of ImportacionExportacionSolicitudOrdenServicios)
    Dim persistenciacierreordenservicio As IPersistenciaBase(Of CerrarOrdenServicio)



    Public ReadOnly Property CapaPersistenciaDetalleConceptoSolicitudOrdenServicios() As IPersistenciaBase(Of DetalleConceptoSolicitudOrdenServicios)
        Get
            If persistenciadetalleconceptosolicitudordenservicios Is Nothing Then
                persistenciadetalleconceptosolicitudordenservicios = New PersistenciaDetalleConceptoSolicitudOrdenSericios()
            End If
            Return persistenciadetalleconceptosolicitudordenservicios
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaDetallePolizasSolicitudOrdenServicios() As IPersistenciaBase(Of DetallePolizasSolicitudOrdenServicios)
        Get
            If persistenciadetallepolizassolicitudordenservicios Is Nothing Then
                persistenciadetallepolizassolicitudordenservicios = New PersistenciaDetallePolizasSolicitudOrdenServicios()
            End If
            Return persistenciadetallepolizassolicitudordenservicios
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaDetalleSolicitudOrdenServicios() As IPersistenciaBase(Of DetalleSolicitudOrdenServicios)
        Get
            If persistenciadetallesolicitudordenservicios Is Nothing Then
                persistenciadetallesolicitudordenservicios = New PersistenciaDetalleSolicitudOrdenServicios()
            End If
            Return persistenciadetallesolicitudordenservicios
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaDetalleDespachoSolicitudOrdenServicios() As IPersistenciaBase(Of DetalleDespachoSolicitudOrdenServicios)
        Get
            If persistenciadetalledespachosolicitudordenservicios Is Nothing Then
                persistenciadetalledespachosolicitudordenservicios = New PersistenciaDetalleDespachoSolicitudOrdenServicios()
            End If
            Return persistenciadetalledespachosolicitudordenservicios
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaEncabezadoSolicitudOrdenServicios() As IPersistenciaBase(Of EncabezadoSolicitudOrdenServicios)
        Get
            If persistenciaencabezadosolicitudordenservicios Is Nothing Then
                persistenciaencabezadosolicitudordenservicios = New PersistenciaEncabezadoSolicitudOrdenServicios()
            End If
            Return persistenciaencabezadosolicitudordenservicios
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaEncabezadoProgramacionOrdenServicios() As IPersistenciaBase(Of EncabezadoProgramacionOrdenServicios)
        Get
            If persistenciaencabezadoprogramacionordenservicios Is Nothing Then
                persistenciaencabezadoprogramacionordenservicios = New PersistenciaEncabezadoProgramacionOrdenServicios()
            End If
            Return persistenciaencabezadoprogramacionordenservicios
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaImportacionExportacionSolicitudOrdenServicios() As IPersistenciaBase(Of ImportacionExportacionSolicitudOrdenServicios)
        Get
            If persistenciaimportacionexportacionsolicitudordenservicios Is Nothing Then
                persistenciaimportacionexportacionsolicitudordenservicios = New PersistenciaImportacionExportacionSolicitudOrdenServicios()
            End If
            Return persistenciaimportacionexportacionsolicitudordenservicios
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaCerrarOrdenServicio() As IPersistenciaBase(Of CerrarOrdenServicio)
        Get
            If persistenciacierreordenservicio Is Nothing Then
                persistenciacierreordenservicio = New PersistenciaCerrarOrdenServicio()
            End If
            Return persistenciacierreordenservicio
        End Get
    End Property



#End Region

#Region "Atributos Utilitarios"
    Public ReadOnly Property CapaPersistenciaListaDistribucionCorreos() As IPersistenciaBase(Of ListaDistribucionCorreos)
        Get
            If persistencialistadribucioncorreos Is Nothing Then
                persistencialistadribucioncorreos = New PersistenciaListaDistribucionCorreos()
            End If
            Return persistencialistadribucioncorreos
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaListadosAuditoriaDocumentos() As IPersistenciaBase(Of ListadosAuditoriaDocumentos)
        Get
            If persistencialistadosauditoriadocumentos Is Nothing Then
                persistencialistadosauditoriadocumentos = New PersistenciaListadosAuditoriaDocumentos()
            End If
            Return persistencialistadosauditoriadocumentos
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaEventoCorreos() As IPersistenciaBase(Of EventoCorreos)
        Get
            If persistenciaeventocorreos Is Nothing Then
                persistenciaeventocorreos = New PersistenciaEventoCorreos()
            End If
            Return persistenciaeventocorreos
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaBandejaSalidaCorreos() As IPersistenciaBase(Of BandejaSalidaCorreos)
        Get
            If persistenciabandejasalidacorreos Is Nothing Then
                persistenciabandejasalidacorreos = New PersistenciaBandejaSalidaCorreos()
            End If
            Return persistenciabandejasalidacorreos
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaConfiguracionServidorCorreos() As IPersistenciaBase(Of ConfiguracionServidorCorreos)
        Get
            If persistenciaconfiguracionservidorcorreos Is Nothing Then
                persistenciaconfiguracionservidorcorreos = New PersistenciaConfiguracionServidorCorreos()
            End If
            Return persistenciaconfiguracionservidorcorreos
        End Get
    End Property
#End Region

#Region "Atributos Comercial"

    Dim persistencialineanegociotransportes As IPersistenciaBase(Of LineaNegocioTransportes)
    Dim persistenciatarifatransportes As IPersistenciaBase(Of TarifaTransportes)
    Dim persistenciatipolineanegociotransportes As IPersistenciaBase(Of TipoLineaNegocioTransportes)
    Dim persistenciatipotarifatransportes As IPersistenciaBase(Of TipoTarifaTransportes)
    Dim persistenciatarifarioventas As IPersistenciaBase(Of TarifarioVentas)
    Dim persistenciatarifariocompras As IPersistenciaBase(Of TarifarioCompras)
    Dim persistenciaUnidadEmpaques As IPersistenciaBase(Of UnidadEmpaqueProductosTransportados)

    Public ReadOnly Property CapaPersistenciaLineaNegocioTransportes() As IPersistenciaBase(Of LineaNegocioTransportes)
        Get
            If persistencialineanegociotransportes Is Nothing Then
                persistencialineanegociotransportes = New PersistenciaLineaNegocioTransportes()
            End If
            Return persistencialineanegociotransportes
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaTarifaTransportes() As IPersistenciaBase(Of TarifaTransportes)
        Get
            If persistenciatarifatransportes Is Nothing Then
                persistenciatarifatransportes = New PersistenciaTarifaTransportes()
            End If
            Return persistenciatarifatransportes
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaTipoLineaNegocioTransportes() As IPersistenciaBase(Of TipoLineaNegocioTransportes)
        Get
            If persistenciatipolineanegociotransportes Is Nothing Then
                persistenciatipolineanegociotransportes = New PersistenciaTipoLineaNegocioTransportes()
            End If
            Return persistenciatipolineanegociotransportes
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaTipoTarifaTransportes() As IPersistenciaBase(Of TipoTarifaTransportes)
        Get
            If persistenciatipotarifatransportes Is Nothing Then
                persistenciatipotarifatransportes = New PersistenciaTipoTarifaTransportes()
            End If
            Return persistenciatipotarifatransportes
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaTarifarioVentas() As IPersistenciaBase(Of TarifarioVentas)
        Get
            If persistenciatarifarioventas Is Nothing Then
                persistenciatarifarioventas = New PersistenciaTarifarioVentas()
            End If
            Return persistenciatarifarioventas
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaTarifarioCompras() As IPersistenciaBase(Of TarifarioCompras)
        Get
            If persistenciatarifariocompras Is Nothing Then
                persistenciatarifariocompras = New PersistenciaTarifarioCompras()
            End If
            Return persistenciatarifariocompras
        End Get
    End Property
    Public ReadOnly Property CapaPersistenciaUnidadEmpaqueProductos() As IPersistenciaBase(Of UnidadEmpaqueProductosTransportados)
        Get
            If persistenciaUnidadEmpaques Is Nothing Then
                persistenciaUnidadEmpaques = New PersistenciaUnidadEmpaqueProductosTransportados()
            End If
            Return persistenciaUnidadEmpaques
        End Get
    End Property
#End Region

#Region "Atributos Almacen"

    Dim persistenciaOrdenCompra As IPersistenciaBase(Of OrdenCompra)
    Public ReadOnly Property CapaPersistenciaOrdenCompra() As IPersistenciaBase(Of OrdenCompra)
        Get
            If persistenciaOrdenCompra Is Nothing Then
                persistenciaOrdenCompra = New PersistenciaOrdenCompra()
            End If
            Return persistenciaOrdenCompra
        End Get
    End Property
    ''' <summary>
    ''' Variable de entorno para manejo de la instancia a la capa de persistencia de datos para Detalle Orden Compra
    ''' </summary>
    Dim persistenciaDetalleOrdenCompra As IPersistenciaBase(Of DetalleOrdenCompra)

    Public ReadOnly Property CapaPersistenciaDetalleOrdenCompra() As IPersistenciaBase(Of DetalleOrdenCompra)
        Get
            If persistenciaDetalleOrdenCompra Is Nothing Then
                persistenciaDetalleOrdenCompra = New PersistenciaDetalleOrdenCompra()
            End If
            Return persistenciaDetalleOrdenCompra
        End Get
    End Property

    Dim persistenciaDetalleDocumentoAlmacenes As IPersistenciaBase(Of DetalleDocumentoAlmacenes)
    Public ReadOnly Property CapaPersistenciaDetalleDocumentoAlmacenes() As IPersistenciaBase(Of DetalleDocumentoAlmacenes)
        Get
            If persistenciaDetalleDocumentoAlmacenes Is Nothing Then
                persistenciaDetalleDocumentoAlmacenes = New PersistenciaDetalleDocumentoAlmacenes()
            End If
            Return persistenciaDetalleDocumentoAlmacenes
        End Get
    End Property


    Dim persistenciaDocumentoAlmacenes As IPersistenciaBase(Of DocumentoAlmacenes)

    Public ReadOnly Property CapaPersistenciaDocumentoAlmacenes() As IPersistenciaBase(Of DocumentoAlmacenes)
        Get
            If persistenciaDocumentoAlmacenes Is Nothing Then
                persistenciaDocumentoAlmacenes = New PersistenciaDocumentoAlmacenes()
            End If
            Return persistenciaDocumentoAlmacenes
        End Get
    End Property

#End Region
#Region "Facturacion"
    Dim persistenciafacturas As IPersistenciaBase(Of Facturas)
    Dim persistenciafacturasotrosconceptos As IPersistenciaBase(Of Facturas)
    Dim persistenciadetallefacturas As IPersistenciaBase(Of DetalleFacturas)
    Dim persistenciaDetalleConceptosFacturas As IPersistenciaBase(Of DetalleConceptosFacturas)
    Dim persistenciaDetalleImpuestosFacturas As IPersistenciaBase(Of DetalleImpuestosFacturas)
    Dim persistenciaReporteFacturaElectronica As IPersistenciaBase(Of ReporteFacturaElectronica)

    Public ReadOnly Property CapaPersistenciaFacturas() As IPersistenciaBase(Of Facturas)
        Get
            If persistenciafacturas Is Nothing Then
                persistenciafacturas = New PersistenciaFacturas()
            End If
            Return persistenciafacturas
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaFacturasOtrosConceptos() As IPersistenciaBase(Of Facturas)
        Get
            If persistenciafacturasotrosconceptos Is Nothing Then
                persistenciafacturasotrosconceptos = New PersistenciaFacturasOtrosConceptos()
            End If
            Return persistenciafacturasotrosconceptos
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaDetalleFacturas() As IPersistenciaBase(Of DetalleFacturas)
        Get
            If persistenciadetallefacturas Is Nothing Then
                persistenciadetallefacturas = New PersistenciaDetalleFacturas()
            End If
            Return persistenciadetallefacturas
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaDetalleConceptosFacturas() As IPersistenciaBase(Of DetalleConceptosFacturas)
        Get
            If persistenciaDetalleConceptosFacturas Is Nothing Then
                persistenciaDetalleConceptosFacturas = New PersistenciaDetalleConceptosFacturas()
            End If
            Return persistenciaDetalleConceptosFacturas
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaDetalleImpuestosFacturas() As IPersistenciaBase(Of DetalleImpuestosFacturas)
        Get
            If persistenciaDetalleImpuestosFacturas Is Nothing Then
                persistenciaDetalleImpuestosFacturas = New PersistenciaDetalleImpuestosFacturas()
            End If
            Return persistenciaDetalleImpuestosFacturas
        End Get
    End Property

    Public ReadOnly Property CapaPersistenciaReporteFacturaElectronica() As IPersistenciaBase(Of ReporteFacturaElectronica)
        Get
            If persistenciaReporteFacturaElectronica Is Nothing Then
                persistenciaReporteFacturaElectronica = New PersistenciaReporteFacturaElectronica()
            End If
            Return persistenciaReporteFacturaElectronica
        End Get
    End Property

#End Region

#Region "Paqueteria"
    Dim persistenciaPlanificacionDespachos As IPersistenciaBase(Of PlanificacionDespachos)
    Public ReadOnly Property CapaPersistenciaPlanificacionDespachos() As IPersistenciaBase(Of PlanificacionDespachos)
        Get
            If persistenciaPlanificacionDespachos Is Nothing Then
                persistenciaPlanificacionDespachos = New PersistenciaPlanificacionDespachos()
            End If
            Return persistenciaPlanificacionDespachos
        End Get
    End Property
    Dim persistenciaLegalizarRemesasPaqueteria As IPersistenciaBase(Of LegalizarRemesasPaqueteria)
    Public ReadOnly Property CapaPersistenciaLegalizarRemesasPaqueteria() As IPersistenciaBase(Of LegalizarRemesasPaqueteria)
        Get
            If persistenciaLegalizarRemesasPaqueteria Is Nothing Then
                persistenciaLegalizarRemesasPaqueteria = New PersistenciaLegalizarRemesasPaqueteria()
            End If
            Return persistenciaLegalizarRemesasPaqueteria
        End Get
    End Property
#End Region

#Region "Propiedades solo lectura Basico"

    ''' <summary>
    ''' Obtiene o establece la configuración de las politicas de datos en cache
    ''' </summary>
    Public Property PoliticaDeCache() As CacheItemPolicy
        Get
            Return politicaCache
        End Get
        Set(value As CacheItemPolicy)
            politicaCache = value
        End Set
    End Property

#End Region

#Region "Propiedades solo lectura Básico Tesorería"

    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para bancos
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaBancos() As IPersistenciaBase(Of Bancos)
        Get
            If persistenciabancos Is Nothing Then
                persistenciabancos = New PersistenciaBancos()
            End If
            Return persistenciabancos
        End Get
    End Property

    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para PlanUnicoCuentas
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaPlanUnicoCuentas() As IPersistenciaBase(Of PlanUnicoCuentas)
        Get
            If persistenciaplanunicocuentas Is Nothing Then
                persistenciaplanunicocuentas = New PersistenciaPlanUnicoCuentas()
            End If
            Return persistenciaplanunicocuentas
        End Get
    End Property

    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para CuentaBancarias
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaCuentaBancarias() As IPersistenciaBase(Of CuentaBancarias)
        Get
            If persistenciacuentabancarias Is Nothing Then
                persistenciacuentabancarias = New PersistenciaCuentaBancarias()
            End If
            Return persistenciacuentabancarias
        End Get
    End Property
    ''' <summary>
    ''' Obtiene una instancia a la capa de persistencia de datos para CuentaBancariaOficinas
    ''' </summary>
    Public ReadOnly Property CapaPersistenciaCuentasBancariaOficinas() As IPersistenciaBase(Of CuentaBancariaOficinas)
        Get
            If persistenciaCuentaBancariaOficinas Is Nothing Then
                persistenciaCuentaBancariaOficinas = New PersistenciaCuentaBancariaOficinas()
            End If
            Return persistenciaCuentaBancariaOficinas
        End Get
    End Property

#End Region

#Region "Propiedades solo lectura"

#End Region

#Region "Constructor"
    Sub New()
        politicaCache = New CacheItemPolicy With {.AbsoluteExpiration = DateTimeOffset.Now.AddMinutes(30)}
    End Sub

#End Region
    Public DSRE As New DSRE

    Public Function SrLstObject(Of T)(objeto As Object) As Object
        Return JsonConvert.DeserializeObject(Of Respuesta(Of IEnumerable(Of T)))(JsonConvert.SerializeObject(objeto, Formatting.Indented, New JsonSerializerSettings With {
                                                                                        .NullValueHandling = NullValueHandling.Ignore, .DefaultValueHandling = DefaultValueHandling.Ignore
                                                                                        }))
    End Function
End Class

